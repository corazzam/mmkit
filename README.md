# README #

* Quick summary
This repository contains the code used to developed the MMkit. 
The MMkit is a tool developed to study organic solar cells while recording the environment where the cells are tested in. It is based on the Arduino for controlling the sensors and measuring the solar cells. The graphical user interface has been developed through Processing.
* Version 1.0
* [Michael Corazza](https://corazzam@bitbucket.org/corazzam/mmkit.git)

### How do I get set up? ###

To make this work you need to have an Arduino MEGA board. Possibly you also need a shield to measure an IV curve. Check our publication for the schematic (coming soon). It is however based on the MIT sourcemeter http://pv.mit.edu/home/education/resources-for-educators/build-your-own-sourcemeter/

You can write me for any request or if you want to know more:
mico@dtu.dk