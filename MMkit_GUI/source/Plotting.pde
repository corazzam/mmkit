/*
Update the plots with the new data received from Arduino
 */


/* Setting for IV*/


int posx_plots = 250;
int posy_plots = 300;
float time_start;
float[] panelDim_IV = new float[] {
  370, 380
};



/* Setting for summary*/
float[] firstPlotPos = new float[] {
  25, 25
};
float[] panelDim = new float[] {
  200, 150
};
float[] margins = new float[] {
  0, 30, 300, 0
};



public void update_plots() {
  
   
  //parameters for saving data
  colors_array = generate_colors(Integer.parseInt(max_iv.getText().trim()));
  int max_iv_curve_graph_new = Integer.parseInt(max_iv.getText().trim());
  if (max_iv_curve_graph_new!=max_iv_curve_graph) {
 String[] savedataPartial_tmp = new String[max_iv_curve_graph];
 Integer[] savedataPartial_ind_tmp = new Integer[max_iv_curve_graph]; 
 arrayCopy( savedataPartial,0, savedataPartial_tmp,0,savedataPartial.length);
 arrayCopy( savedataPartial_ind, savedataPartial_ind_tmp);
 savedataPartial=savedataPartial_tmp;
 savedataPartial_ind=savedataPartial_ind_tmp;
  }
  
  // Create a new plot and set its position on the screen
  
  //IV curves
  IV_plot.setPos(posx_plots, posy_plots);
  IV_plot.setDim(panelDim_IV);
  // Set the plot title and the axis labels
  IV_plot.setTitleText("IV plot");
  IV_plot.getXAxis().setAxisLabelText("Voltage [mV]");
  IV_plot.getYAxis().setAxisLabelText("Current [mA]");
  // Preserve initial XLim and YLim for autoscale
  xLimAutoscale = IV_plot.getXLim();
  yLimAutoscale = IV_plot.getYLim();
  // Draw it!
  
    IV_plot.beginDraw();
  IV_plot.drawBox();
  IV_plot.drawXAxis();
  IV_plot.drawYAxis();
  IV_plot.drawTopAxis();
  IV_plot.drawRightAxis();
  IV_plot.drawTitle();
  IV_plot.drawGridLines(GPlot.BOTH);
  IV_plot.activatePanning();
  IV_plot.activateZooming();
  IV_plot.drawPoints();
  IV_plot.drawLines();
  if (l1!=0 || l2!=0) {
    int size = (int)(l2-l1);
    //the +1 is to consider the main layer that cannot be removed
    String[] legend_values = new String[size+1];
    float[] x = new float[size+1];
    float[] y = new float[size+1];
    int j=0;
   for (long i= l1; i <l2+1; i++)
   {
       if (i==l1) legend_values[j]="Legend:"; 
       else legend_values[j]= Long.toString(i);
       
       x[j]= 0.1;
       y[j]=0.95-0.04*j;
      j++; 
   } 
  IV_plot.drawLegend(legend_values,x,y);
  }
  
  
  IV_plot.endDraw();

  // Create four plots to represent the 4 panels

  PCE_plot.setPos(firstPlotPos);
  PCE_plot.setMar(0, margins[1], margins[2], 0);
  PCE_plot.setDim(panelDim);
  PCE_plot.setAxesOffset(0);
  PCE_plot.setTicksLength(-4);
  PCE_plot.getXAxis().setDrawTickLabels(true);
  if(compact_mode==true)
  PCE_plot.setVerticalAxesNTicks(2);
  else
  PCE_plot.setVerticalAxesNTicks(3);

  irr_plot.setPos(firstPlotPos[0] + margins[1] + panelDim[0]+515, firstPlotPos[1]);
  irr_plot.setMar(0, 0, margins[2], margins[3]);
  irr_plot.setDim(panelDim);
  irr_plot.setAxesOffset(0);
  irr_plot.setTicksLength(-4);
  irr_plot.getXAxis().setDrawTickLabels(true);
  irr_plot.activateZooming();
  if(compact_mode==true)
  irr_plot.setVerticalAxesNTicks(2);
  else
  irr_plot.setVerticalAxesNTicks(3);

  temp_plot.setPos(firstPlotPos[0], firstPlotPos[1] + margins[2] + panelDim[1]);
  temp_plot.setMar(margins[0], margins[1], 75, 0);
  temp_plot.setDim(panelDim);
  temp_plot.setAxesOffset(0);
  temp_plot.setTicksLength(-4);
  if(compact_mode==true)
  temp_plot.setVerticalAxesNTicks(2);
  else
  temp_plot.setVerticalAxesNTicks(3);
  

  hum_plot.setPos(firstPlotPos[0] + margins[1] + panelDim[0]+515, firstPlotPos[1] + margins[2] + panelDim[1]);
  hum_plot.setMar(margins[0], 0, 75, margins[3]);
  hum_plot.setDim(panelDim);
  hum_plot.setAxesOffset(0);
  hum_plot.setTicksLength(-4);
  if(compact_mode==true)
  hum_plot.setVerticalAxesNTicks(2);
  else
  hum_plot.setVerticalAxesNTicks(3);

  // Set the points, the title and the axis labels

  PCE_plot.getXAxis().setAxisLabelText("Time [minutes]");
  PCE_plot.getYAxis().setAxisLabelText("PCE [%]");
  PCE_plot.setPointColor(color(100, 100, 255));

  irr_plot.getXAxis().setAxisLabelText("Time [minutes]");
  irr_plot.getYAxis().setAxisLabelText("Irradiance (mW/cm2)");
  irr_plot.setPointColor(color(100, 100, 255));

  temp_plot.getXAxis().setAxisLabelText("Time [minutes]");
  temp_plot.getYAxis().setAxisLabelText("Temperature (oC)");
  temp_plot.setInvertedYScale(false);
  temp_plot.setPointColor(color(100, 100, 255));

  hum_plot.getXAxis().setAxisLabelText("Time [minutes]");
  hum_plot.getYAxis().setAxisLabelText("Humidity [% R.H.]");
  hum_plot.setInvertedYScale(false);
  hum_plot.setPointColor(color(100, 100, 255));

  // Draw the plots
  PCE_plot.beginDraw();
  PCE_plot.drawBox();
  PCE_plot.drawXAxis();
  PCE_plot.drawYAxis();
  PCE_plot.drawTopAxis();
  PCE_plot.drawRightAxis();
  PCE_plot.drawTitle();
  PCE_plot.drawPoints();
  PCE_plot.endDraw();

  irr_plot.beginDraw();
  irr_plot.drawBox();
  irr_plot.drawXAxis();
  irr_plot.drawYAxis();
  irr_plot.drawTopAxis();
  irr_plot.drawRightAxis();
  irr_plot.drawPoints();
  irr_plot.endDraw();

  temp_plot.beginDraw();
  temp_plot.drawBox();
  temp_plot.drawXAxis();
  temp_plot.drawYAxis();
  temp_plot.drawTopAxis();
  temp_plot.drawRightAxis();
  temp_plot.drawPoints();
  temp_plot.endDraw();

  hum_plot.beginDraw();
  hum_plot.drawBox();
  hum_plot.drawXAxis();
  hum_plot.drawYAxis();
  hum_plot.drawTopAxis();
  hum_plot.drawRightAxis();
  hum_plot.drawPoints();
  //hum_plot.drawLines();
  hum_plot.endDraw();
}

// double click on IV graph to autoscale
void mousePressed() {
  if (IV_plot.isOverBox(mouseX, mouseY)) {
    if (mouseEvent.getClickCount()==2) {
      IV_plot.setXLim(xLimAutoscale);
      IV_plot.setYLim(yLimAutoscale);
    }
  }
}
