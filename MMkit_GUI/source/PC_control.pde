
// Group 1 controls
GToggleGroup togG1Options; 
GOption grp1_a, grp1_b, grp1_c; 
GDropList grp1_d; 
GKnob grp1_e; 
GButton btnConnect, btnLdSett, btnSvSett, btnStMeas, btnSvData, btnStopMeas, btnChFolder, btnCalPhoto, btnCalTime, btnCalEnd, btnSvSet, btnDelData;
GCustomSlider grp1_g; 
GTextField startV, stopV, numPoints, numMeasurements, filename, folder, sett_irr, intervalIV, intervalEnv, intervalIrr, area, iv_save, note, max_iv, email, PCE_limit, cal_photo; 
GLabel lblTitle, lblStatus, startV_lbl, stopV_lbl, numPoints_lbl, numMeasurements_lbl, filename_lbl, folder_lbl, sett_irr_lbl, intervalIV_lbl, intervalEnv_lbl, intervalIrr_lbl, area_lbl, iv_save_lbl, note_lbl, max_iv_lbl, static_graph_lbl; 
GPanel panelIV, panelOpt, panelTime, panelCal, panelExSet; 
GCheckbox Debug, DHT22, DS18, Irr, autoSaveOpt, database_MySQL, con_ethernet, refresh_graph; 
GTextArea Serial_text, Serial_text_M; 
GWindow Calibr_window;
int pYStatus = 780;
int pWidth = width_window;
boolean ser_connected = false;
int x_IV_values =250;
int y_IV_values = 30;
int widthx_IV_values =50;
int widthy_IV_values =20;
int distance_IV =40;
int distance_lbl_IV=280; // negative
int widthx_lbl_IV=250;
int widthy_lbl_IV=widthy_IV_values;
String path_save;
// to write to the same file
PrintWriter output;
boolean calibrPh=false;
boolean calibrTi=false;
public static final char TIME_HEADER = 'T';
public static final char TIME_REQUEST = 7;  // ASCII bell character 
boolean DEBUG = false;
int count_data=0;
boolean ready=false;
String so = "mac";
int[] colors_array;
boolean measuring=false;


public void createGUI() {
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(GCScheme.CYAN_SCHEME);
  if (frame != null)
    frame.setTitle("MMkit User Interface");
  createGroup1Controls();
  createTextContainers();
  createOptions();
  createTimeSettings();
  createCalibration();  
  extraSettings();
  extraInternet();

  rects.add(new Rectangle(3, 40, 988, 260));
  rects.add(new Rectangle(3, 300, 988, 478));

  if (compact_mode==false) {
    lblStatus = new GLabel(this, 0, pYStatus, pWidth, 30, "");
  } else {
    lblStatus = new GLabel(this, 0, pYStatus-heigh_reduction*2-20, pWidth, 30, "");
  }

  lblStatus.setOpaque(true);
  // initialize title label
  lblTitle = new GLabel(this, 0, 0, pWidth, 35, "MultiMeasurement kit");
  lblTitle.setOpaque(true);
  GLabel lblMico = new GLabel(this, 450, 0, pWidth, 35, "mico@dtu.dk");
  lblTitle.setOpaque(true);
}


public void createGroup1Controls() {
  Debug = new GCheckbox(this, x_pos_cnt-170, y_pos_cnt, x_width_btn, y_width_btn);
  Debug.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  Debug.setText("Debug");
  Debug.addEventHandler(this, "debug_clicked");
  Debug.setOpaque(false);
  filename = new GTextField(this, x_pos_cnt+390, y_pos_cnt+32, x_width_btn-10, y_width_btn-15, G4P.SCROLLBARS_NONE);
  filename.setOpaque(true);
  //filename.setText(String.valueOf(year())+String.valueOf(month())+String.valueOf(day()));
  filename.setPromptText("Filename");
  //filename_lbl = new GLabel(this, x_pos_cnt+465, y_pos_cnt+5, x_width_btn, y_width_btn-10);
  //filename_lbl.setText("Filename:");
  //filename_lbl.setTextAlign(GAlign.RIGHT, null);
  //filename_lbl.setOpaque(false);
  btnConnect = new GButton(this, x_pos_cnt+75, y_pos_cnt-46, x_width_btn, y_width_btn);
  btnConnect.setText("Connect");
  btnConnect.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  btnStMeas = new GButton(this, x_pos_cnt+700, y_pos_cnt, x_width_btn, y_width_btn);
  btnStMeas.setText("Start");
  btnStMeas.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  btnStopMeas = new GButton(this, x_pos_cnt+800, y_pos_cnt, x_width_btn, y_width_btn);
  btnStopMeas.setText("Stop");
  btnStopMeas.setLocalColorScheme(GCScheme.RED_SCHEME);
  folder = new GTextField(this, x_pos_cnt+300, y_pos_cnt+32, x_width_btn, y_width_btn-15, G4P.SCROLLBARS_NONE);
  folder.setOpaque(true);
  //folder.setPromptText("Folder path");
  folder_lbl = new GLabel(this, x_pos_cnt+20, y_pos_cnt+32, x_width_btn, y_width_btn-15);
  //folder_lbl.setText("Folder:");
  folder.setPromptText("Folder");
  folder_lbl.setTextAlign(GAlign.RIGHT, null);
  folder_lbl.setOpaque(false);
  btnChFolder = new GButton(this, x_pos_cnt+300, y_pos_cnt, x_width_btn, y_width_btn);
  btnChFolder.setText("Choose folder");
  btnDelData = new GButton(this, x_pos_cnt+500, y_pos_cnt, x_width_btn, y_width_btn);
  btnDelData.setText("Clean plots");
  iv_save = new GTextField(this, x_pos_cnt+460, y_pos_cnt+32, x_width_btn-50, y_width_btn-15, G4P.SCROLLBARS_NONE);
  iv_save.setOpaque(true);
  iv_save.setPromptText("n. IV");
  btnSvData = new GButton(this, x_pos_cnt+400, y_pos_cnt, x_width_btn, y_width_btn);
  btnSvData.setText("Save data");
}

// event handler for buttons
public void handleButtonEvents(GButton button, GEvent event) {
  // handle Connect button
  if (btnConnect == button && event == GEvent.CLICKED) {
    portName = cboSerial.getSelectedText();
    updateStatusBar("Connecting..."); // display connecting status 
    myPort = new Serial(this, portName, 115200);
    ser_connected= true;
    try {
      Thread.sleep(1000);                 //1000 milliseconds is one second.
    } 
    catch(InterruptedException ex) {
      Thread.currentThread().interrupt();
    }
    myPort.write("C");
  } else if (btnStMeas == button && event == GEvent.CLICKED ) {
    File f = new File(folder.getText()+fs+filename.getText()+".txt");
    //if (ser_connected == true && (lblStatus.getText().equals("Status: Ready to measure")==true ||  lblStatus.getText().equals("Status: Saved!")==true||  lblStatus.getText().equals("Status: Data erased")==true||  lblStatus.getText().equals("Stop pressed! Finishing current measurement")==true||  lblStatus.getText().equals("Status: Settings saved to SD!")==true||  lblStatus.getText().equals("Status: Settings updated (SD is not connected)")==true)) {
    if ((ser_connected == true && ready == true) || debug==true) {
      if (Integer.parseInt(numPoints.getText().trim())>50 ||Integer.parseInt(numPoints.getText().trim())<2) {
        handleMessageDialog("N. points can vary from 2 to 50");
      } else if (autoSaveOpt.isSelected() && f.exists() && !f.isDirectory()) {
        handleMessageDialog("Autosave mode: file where to save must be empty");
      }
      //start==stop voltage don't run
      else if (Float.parseFloat(startV.getText().trim())== Float.parseFloat(stopV.getText().trim())) {
        handleMessageDialog("Start and stop voltage cannot be the same");
      } else if (abs(Float.parseFloat(startV.getText().trim()))>12 || abs(Float.parseFloat(stopV.getText().trim()))>12 ) {
        handleMessageDialog("Exceeding voltage limit");
      } else if (Irr.isSelected()==true && cal_photo.getText().equals("")==true  ) {
        handleMessageDialog("You must insert the calibration value of the photodiode");
      } else {
        if (((Float.parseFloat(numMeasurements.getText().trim())>1 || Float.parseFloat(numMeasurements.getText().trim())==0)) && (autoSaveOpt.isSelected()==false )) {
          handleMessageDialog("You are performing a time-study without autosave on");
        }
        // clean graphs
        if (refresh_graph.isSelected()==true) {
          PCE_plot = new GPlot(this);
          irr_plot = new GPlot(this);
          temp_plot = new GPlot(this);
          hum_plot = new GPlot(this);
        }

        // autosave is on then delete clean data before measuring 
        // if autosave is selected and then stop and start is pressed
        if (autoSaveOpt.isSelected() ) {
          file= folder.getText()+fs+filename.getText().trim()+note.getText().trim()+".txt";  // for database exportation
          file_sum= folder.getText()+fs+filename.getText().trim()+"_summary.csv";  // summary of iv curve measurements
          file_iv= folder.getText()+fs+filename.getText().trim()+"_iv.csv";  // separate iv curves, used only in autosave mode
          file_sum_sensors= folder.getText()+fs+filename.getText().trim()+"_summarySensors.csv";  // summary of sensors measurements, used only in autosave mode
          println(file);
          cleanData();
          output = createWriter(file);
        }

        //Options measurements
        email_sent = false;
        counter=0;

        if (debug==true) {
          debug_meas=true;
        } else {
          if (Debug.isSelected()) myPort.write("D 1");
          if (!Debug.isSelected()) myPort.write("D 0");
          if (DHT22.isSelected()) myPort.write("H 1");
          if (!DHT22.isSelected()) myPort.write("H 0");
          if (DS18.isSelected()) myPort.write("T 1");
          if (!DS18.isSelected()) myPort.write("T 0");
          if (Irr.isSelected()) { 
            myPort.write("I 1");
            myPort.write("V 5 "+Integer.parseInt(cal_photo.getText().trim()));
          }
          if (!Irr.isSelected()) myPort.write("I 0");
          int DELAY=10;

          // Set up interval if number of measurement > 1
          if ( Integer.parseInt(numMeasurements.getText().trim()) > 1 || Integer.parseInt(numMeasurements.getText().trim())== 0) {
            myPort.write("M 1 "+intervalIV.getText());
            if (DHT22.isSelected()||DS18.isSelected() )
              myPort.write("M 2 "+intervalEnv.getText());
            if (Irr.isSelected())
              myPort.write("M 3 "+intervalIrr.getText());
          }
          updateStatusBar("Measuring..."); // display connecting status 
          String command = "S " + startV.getText().trim()+ " "+ stopV.getText().trim()+ " "+(Integer.parseInt(numPoints.getText().trim())-1)+ " "+Integer.parseInt(numMeasurements.getText().trim());
          myPort.write(command);

          updateStatusBar("Command: " + command);
        }
        updateStatusBar("Measuring...");
        if (refresh_graph.isSelected()==true)  time_start=millis()/1000;

        ready=false; // MMkit is measuring
        measuring=true;
      }
    } else if (ser_connected == false) {
      handleMessageDialog("You must connect first!");
    } else  if (ready ==false) {
      handleMessageDialog("MMkit is not ready to measure");
    } else {
      handleMessageDialog("Error on starting measurement. Try restart");
    }
  } else if (btnStopMeas == button && event == GEvent.CLICKED) {
    if (ser_connected == true && measuring==true) {
      updateStatusBar("Stop! Finishing current measurement"); // display connecting status 
      ready=true; //ready to measure again - this should come
      measuring =false; //not sure
      String command = "O";
      myPort.write(command);
    } else if (ser_connected == false) {
      handleMessageDialog("You must connect first!");
    } else if (measuring==false) {
      handleMessageDialog("You are not measuring!");
    }
  } else if (btnCalPhoto == button && event == GEvent.CLICKED) {
    if (ser_connected == true && ready==true && measuring==false) {
      ready=false;
      updateStatusBar("Calibrating photodiode"); // display connecting status 
      calibrPh=true; //Special mode - no update of graphs. Serial stream goes to new window
      Calibr_window = new GWindow(this, "Calibration", 0, 0, 240, 300, false, JAVA2D);
      Calibr_window.setActionOnClose(GWindow.KEEP_OPEN);   //  GWindow.KEEP_OPEN, GWindow.CLOSE_WINDOW  depending on the action you want to perform when attempting to close
      Calibr_window.addDrawHandler(this, "win_draw1");
      int x = 50;
      int y =200;
      btnCalEnd = new GButton(Calibr_window.papplet, x, y, x_width_btn, y_width_btn);
      btnCalEnd.setText("End calibration");
      btnCalEnd.setLocalColorScheme(GCScheme.RED_SCHEME);
      String command = "A 1";
      Serial_text = new GTextArea(Calibr_window.papplet, 40, 20, 160, 140, G4P.SCROLLBARS_VERTICAL_ONLY);
      Serial_text.setOpaque(true);
      //Serial_text.addEventHandler(this, "Serial_text_change");
      myPort.write(command);
    } else if (ser_connected == false)
      updateStatusBar("You must connect first!");
    else if (ready == false)
      updateStatusBar("MMkit is not correctly connected");
    else if (measuring == true)
      updateStatusBar("You can't calibrate while measuring");
  } else if (btnCalTime == button && event == GEvent.CLICKED) {
    if (ser_connected == true && ready==true && measuring==false) {
      ready =false;
      if (Debug.isSelected()) myPort.write("D 1");
      if (!Debug.isSelected()) myPort.write("D 0");
      updateStatusBar("Calibrating time"); // display connecting status 
      calibrTi=true; //Special mode - no update of graphs. Serial stream goes to new window
      Calibr_window = new GWindow(this, "Calibration", 0, 0, 240, 300, false, JAVA2D);
      Calibr_window.setActionOnClose(GWindow.KEEP_OPEN);   //  GWindow.KEEP_OPEN, GWindow.CLOSE_WINDOW  depending on the action you want to perform when attempting to close
      Calibr_window.addDrawHandler(this, "win_draw1");
      int x = 50;
      int y =200;
      btnCalEnd = new GButton(Calibr_window.papplet, x, y, x_width_btn, y_width_btn);
      btnCalEnd.setText("End calibration");
      btnCalEnd.setLocalColorScheme(GCScheme.RED_SCHEME);
      //Calibr_window.addDrawHandler(this, "Calibr_draw");
      String command = "A 2";
      Serial_text = new GTextArea(Calibr_window.papplet, 40, 20, 160, 140, G4P.SCROLLBARS_VERTICAL_ONLY);
      Serial_text.setOpaque(true);
      myPort.write(command);
      print("command sent");
    } else if (ser_connected == false)
      updateStatusBar("You must connect first!");
    else if (ready == false)
      updateStatusBar("MMkit is not correctly connected");
    else if (measuring == true)
      updateStatusBar("You can't calibrate while measuring");
  } else if (btnCalEnd == button && event == GEvent.CLICKED) {
    ready=true;
    Calibr_window.forceClose();
    calibrTi=false;
    calibrPh=false;
    String command = "1";
    myPort.write(command);
    updateStatusBar("Ready to measure");
  } else if (btnDelData == button && event == GEvent.CLICKED) {
    updateStatusBar("Data erased"); // display connecting status 
    alldata=""; // empty data when saved!
    cleanData();
  } else if (btnSvSet == button && event == GEvent.CLICKED) {
    if (ser_connected == true || DEBUG==true) {

      if (Integer.parseInt(numPoints.getText().trim())>50 ||Integer.parseInt(numPoints.getText().trim())<2) {
        handleMessageDialog("N. points can vary from 2 to 50");
      } else if (abs(Float.parseFloat(startV.getText().trim()))>12 || abs(Float.parseFloat(stopV.getText().trim()))>12 ) {
        handleMessageDialog("Exceeding voltage limit");
      } else {
        String command = "E";
        updateStatusBar("Save settings");
        myPort.write(command);
        if (Debug.isSelected()) myPort.write("D 1");
        if (!Debug.isSelected()) myPort.write("D 0");
        if (DHT22.isSelected()) myPort.write("H 1");
        if (!DHT22.isSelected()) myPort.write("H 0");
        if (DS18.isSelected()) myPort.write("T 1");
        if (!DS18.isSelected()) myPort.write("T 0");
        if (Irr.isSelected()) myPort.write("I 1");
        if (!Irr.isSelected()) myPort.write("I 0");
        myPort.write("M 1 "+intervalIV.getText());
        myPort.write("M 2 "+intervalEnv.getText());
        myPort.write("M 3 "+intervalIrr.getText());
        myPort.write("V 1 "+startV.getText().trim());
        println("here ok");
        int DELAY=500;
        delay(DELAY);
        myPort.write("V 2 "+stopV.getText().trim());
        delay(DELAY);
        myPort.write("V 3 "+(Integer.parseInt(numPoints.getText().trim())-1));
        delay(DELAY);
        myPort.write("V 4 "+numMeasurements.getText().trim());
        delay(DELAY);
        myPort.write("V 5 "+Integer.parseInt(cal_photo.getText().trim()));
        println("Send signal to write to SD");
        myPort.write("E 1");  // write settings to sd card
      }
    } else 
      handleMessageDialog("You are not connected!");
  }
  //////////////// SAVE data /////////////////////////// 
  else if (btnSvData == button && event == GEvent.CLICKED) {


    if ( folder.getText() =="") {
      handleMessageDialog("Chose a folder first!");
    } else if (autoSaveOpt.isSelected()==true) {
      // when autosave is on there is no need to click save
      handleMessageDialog("Auto save is enabled!");
    } else if (savedata=="") handleMessageDialog("Nothing to save");
    else if (filename.getText()=="") handleMessageDialog("Specify a filename");
    else {
      // there is something to save
      //updateStatusBar("Saving..."); // display connecting status 
      file= folder.getText()+fs+filename.getText()+note.getText().trim()+".txt";  // for database exportation
      file_sum= folder.getText()+fs+filename.getText()+"_summary.csv";  // summary of iv curve measurements
      file_iv= folder.getText()+fs+filename.getText()+"_iv.csv";  // separate iv curves, used only in autosave mode
      file_sum_sensors= folder.getText()+fs+filename.getText()+"_summarySensors.csv";  // summary of sensors measurements, used only in autosave mode

      //no option for savings --> standard save: last iv curve is saved
      if (iv_save.getText()=="") {
        //save last iv curve data
        println("Save last iv curve");

        //extract information from data saved in program and export to toSave_sum and toSave_IV
        Table toSave_sum= new Table();
        Table toSave_IV =new Table();
        recreateTableIV(toSave_IV);
        recreateTableSum(toSave_sum);
        //find last curve position:
        // counter_iv is a counter that is reset only by pressing "delete data". However only a number of curve equals to 
        // "IV to display is stored"
        // savedataPartial_ind contains all the ID of the iv curves, acquired from counter_iv
        // find the index in savedataPartial_ind containing counter_iv
        int ind_dataToSave = Arrays.asList(savedataPartial_ind).indexOf(counter_iv);
        String in = String.valueOf(savedataPartial_ind[ind_dataToSave]);
        println("ID curve to save is: "+ in);
        println("IV saved is: "+ savedataPartial[ind_dataToSave]);
        try {
          FileWriter fw = new FileWriter(file, true); //the true will append the new data
          fw.write(savedataPartial[ind_dataToSave]+eol);//appends the string to the file
          fw.close();
        }
        catch(Exception ioe)
        {
          System.err.println("IOException: " + ioe.getMessage());
          handleMessageDialog("Couldn't save" + file);
        }


        // all the row with id=counter_iv (i.e. last curve are saved)
        for (TableRow row_to_save : Meas_summary.findRows ( Integer.toString (counter_iv), "id")) {
          toSave_sum.addRow(row_to_save);
        }
        for (TableRow row_to_save : IV_data.findRows ( Integer.toString (counter_iv), "id")) {
          toSave_IV.addRow(row_to_save);
        }

        try {
          addTable(toSave_sum, file_sum);
        }
        catch(Exception ioe)
        {
          System.err.println("IOException: " + ioe.getMessage());
          handleMessageDialog("Couldn't save" + file_sum);
        }
        try {
          addTable(toSave_IV, file_iv);
        }
        catch(Exception ioe)
        {
          System.err.println("IOException: " + ioe.getMessage());
          handleMessageDialog("Couldn't save" + file_iv);
        }

        updateStatusBar("IV curve " +counter_iv+ " saved"); // display connecting status
      }
      //save all the iv curve  displayed
      else if (iv_save.getText().trim().equals("A")==true) {
        println("Complete saving");

        try {
          saveTable(Meas_summary, file_sum);
        }
        catch(Exception ioe)
        {
          System.err.println("IOException: " + ioe.getMessage());
          handleMessageDialog("Couldn't save" + file_sum);
        }

        try {
          saveTable(IV_data, file_iv);
        }
        catch(Exception ioe)
        {
          System.err.println("IOException: " + ioe.getMessage());
          handleMessageDialog("Couldn't save" + file_iv);
        }

        try {
          FileWriter fw = new FileWriter(file, true); //the true will append the new data
          int limit;
          if (counter_iv>=max_iv_curve_graph) limit = max_iv_curve_graph;
          else limit = counter_iv;
          for (int i=0; i<limit; i++) {
            fw.write(savedataPartial[i]);//appends the string to the file
          }
          fw.close();
        }
        catch(Exception ioe)
        {
          System.err.println("IOException: " + ioe.getMessage());
          handleMessageDialog("Couldn't save" + file);
        }

        updateStatusBar("All IV curve saved"); // display connecting status
      } else if ( isNumeric(iv_save.getText().trim())) {

        println("Partial saving");
        if (Integer.parseInt(iv_save.getText ().trim())> counter_iv || Integer.parseInt(iv_save.getText ().trim())<= counter_iv-max_iv_curve_graph|| Integer.parseInt(iv_save.getText ().trim())<= 0) {
          handleMessageDialog("IV curve " +iv_save.getText ().trim() + " is not in memory" );
        } else {
          // save only selected iv curve and do not remove any data
          // so far I want to add data to existing file until I press delete data
          Table toSave_sum= new Table();
          Table toSave_IV= new Table();
          recreateTableIV(toSave_IV);
          recreateTableSum(toSave_sum);
          int ind_dataToSave = Arrays.asList(savedataPartial_ind).indexOf(Integer.parseInt(iv_save.getText ().trim()));
          String in = String.valueOf(savedataPartial_ind[ind_dataToSave]);
          println("ID curve to save is: "+ in);
          println("IV saved is: "+ savedataPartial[ind_dataToSave]);
          try {
            FileWriter fw = new FileWriter(file, true); //the true will append the new data
            fw.write(savedataPartial[ind_dataToSave]+eol);//appends the string to the file
            fw.close();
          }
          catch(Exception ioe)
          {
            System.err.println("IOException: " + ioe.getMessage());
            handleMessageDialog("Couldn't save" + file);
          }


          for (TableRow row_to_save : Meas_summary.findRows ( (iv_save.getText ().trim()), "id")) {
            toSave_sum.addRow(row_to_save);
          }
          TableRow newRow = Meas_summary.addRow();
          for (TableRow row_to_save : IV_data.findRows ( (iv_save.getText ().trim()), "id")) {
            toSave_IV.addRow(row_to_save);
          }            

          try {
            addTable(toSave_sum, file_sum);
          }
          catch(Exception ioe)
          {
            System.err.println("IOException: " + ioe.getMessage());
            handleMessageDialog("Couldn't save" + file_sum);
          }
          try {
            addTable(toSave_IV, file_iv);
          }
          catch(Exception ioe)
          {
            System.err.println("IOException: " + ioe.getMessage());
            handleMessageDialog("Couldn't save" + file_iv);
          }
          updateStatusBar("IV curve " +(Integer.parseInt(iv_save.getText ().trim()))+ " save!d"); // display connecting status
        }
      } else {
        handleMessageDialog("N. IV can be numberic or \"A\" to save all data stored");
      }
    }
  } else if (btnChFolder == button && event == GEvent.CLICKED) {
    selectFolder("Select a folder to process:", "folderSelected");
  }
}

int posx_option = 10;
public void createOptions() {
  panelOpt = new GPanel(this, 407, 110, 265, widthy_IV_values*9, "IV settings");
  panelOpt.setCollapsible(false);
  panelOpt.setDraggable(false);
  panelOpt.setText("Options");
  panelOpt.setOpaque(true);
  DHT22 = new GCheckbox(this, posx_option, y_IV_values-4, x_width_btn, y_width_btn);
  DHT22.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  DHT22.setText("Humidity");
  DHT22.addEventHandler(this, "DHT22_clicked");
  DHT22.setOpaque(false);
  autoSaveOpt = new GCheckbox(this, posx_option, y_IV_values+distance_IV*3-4, x_width_btn, y_width_btn);
  autoSaveOpt.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  autoSaveOpt.setText("Auto save");
  autoSaveOpt.addEventHandler(this, "autosave_clicked");
  autoSaveOpt.setOpaque(false);
  //autoSaveOpt.setSelected(true);
  DS18 = new GCheckbox(this, posx_option, y_IV_values+distance_IV-4, x_width_btn+15, y_width_btn);
  DS18.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  DS18.setText("Temperature");
  DS18.addEventHandler(this, "DS18_clicked");
  DS18.setOpaque(false);
  Irr = new GCheckbox(this, posx_option, y_IV_values+distance_IV*2-4, x_width_btn+50, y_width_btn);
  Irr.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  Irr.setText("Fixed irradiance\n(set value mW/cm2)");
  Irr.addEventHandler(this, "irr_clicked");
  Irr.setOpaque(false);
  sett_irr = new GTextField(this, posx_option+150, y_IV_values+distance_IV*2+1, x_width_btn-50, y_width_btn-10, G4P.SCROLLBARS_NONE);
  sett_irr.setOpaque(true);
  sett_irr.setText("100");
  area = new GTextField(this, posx_option+200, y_IV_values+1, x_width_btn-40, y_width_btn-10, G4P.SCROLLBARS_NONE);
  area.setOpaque(true);
  area.setText("1");
  area_lbl = new GLabel(this, posx_option+140, y_IV_values-5, x_width_btn-30, y_width_btn);
  area_lbl.setText("Area(cm2)");
  area_lbl.setTextAlign(GAlign.RIGHT, null);
  area_lbl.setOpaque(false);
  note = new GTextField(this, posx_option+170, y_IV_values+distance_IV, x_width_btn-10, y_width_btn-10, G4P.SCROLLBARS_NONE);
  note.setOpaque(true);
  note.setText("");
  note_lbl = new GLabel(this, posx_option+110, y_IV_values-5+distance_IV, x_width_btn-30, y_width_btn);
  note_lbl.setText("Note");
  note_lbl.setTextAlign(GAlign.RIGHT, null);
  note_lbl.setOpaque(false);
  panelOpt.addControl(DHT22);
  panelOpt.addControl(DS18);
  panelOpt.addControl(Irr);
  panelOpt.addControl(autoSaveOpt);
  panelOpt.addControl(sett_irr);
  panelOpt.addControl(area);
  panelOpt.addControl(area_lbl);
  panelOpt.addControl(note);
  panelOpt.addControl(note_lbl);
}


public void createTimeSettings() {
  int x_time = 170;
  int y_IV_values =40;
  panelTime = new GPanel(this, 700, 110, 250, widthy_IV_values*9, "a");
  panelTime.setCollapsible(true);
  panelTime.setCollapsed(true);
  panelTime.setDraggable(false);
  panelTime.setText("Interval between measurements");
  panelTime.setOpaque(true);
  intervalIV = new GTextField(this, x_time, y_IV_values, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  intervalIV.setOpaque(true);
  intervalIV.setText("1");
  intervalIV_lbl = new GLabel(this, x_time-distance_lbl_IV, y_IV_values, widthx_lbl_IV, widthy_lbl_IV);
  intervalIV_lbl.setText("IV curve (m)");
  intervalIV_lbl.setOpaque(false);
  intervalIV_lbl.setTextAlign(GAlign.RIGHT, null);
  intervalEnv = new GTextField(this, x_time, y_IV_values+distance_IV, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  intervalEnv.setOpaque(true);
  intervalEnv.setText("1");
  intervalEnv_lbl = new GLabel(this, x_time-distance_lbl_IV, y_IV_values+distance_IV, widthx_lbl_IV, widthy_lbl_IV);
  intervalEnv_lbl.setText("Temp. and Humidity (m)");
  intervalEnv_lbl.setTextAlign(GAlign.RIGHT, null);
  intervalEnv_lbl.setOpaque(false);
  intervalIrr = new GTextField(this, x_time, y_IV_values+distance_IV*2, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  intervalIrr.setOpaque(true);
  intervalIrr.setText("1");
  intervalIrr_lbl = new GLabel(this, x_time-distance_lbl_IV, y_IV_values+distance_IV*2, widthx_lbl_IV, widthy_lbl_IV);
  intervalIrr_lbl.setText("Irradiance (m)");
  intervalIrr_lbl.setTextAlign(GAlign.RIGHT, null);
  intervalIrr_lbl.setOpaque(false);

  panelTime.addControl(intervalIV);
  panelTime.addControl(intervalIV_lbl);
  panelTime.addControl(intervalEnv);
  panelTime.addControl(intervalEnv_lbl);
  panelTime.addControl(intervalIrr);
  panelTime.addControl(intervalIrr_lbl);
}
public void extraInternet() {
  int x = 20;
  int y =40;
  int y_shift= 60;
  panelExSet = new GPanel(this, 780, 150, x_width_btn*2+20, widthy_IV_values*4, "a");
  panelExSet.setCollapsible(true);
  panelExSet.setCollapsed(true);
  panelExSet.setDraggable(false);
  panelExSet.setText("Email report");
  panelExSet.setOpaque(true);

  email = new GTextField(this, x+15, y-5, x_width_btn+50, y_width_btn-15, G4P.SCROLLBARS_NONE);
  email.setPromptText("email");

  PCE_limit = new GTextField(this, x+95, y+15, x_width_btn-30, y_width_btn-15, G4P.SCROLLBARS_NONE);
  PCE_limit.setPromptText("PCElim");

  panelExSet.addControl(email);
  panelExSet.addControl(PCE_limit);
}

public void extraSettings() {
  int x = 20;
  int y =40;
  int y_shift= 60;
  GLabel expand_tab = new GLabel(this, 670, 92, 300, 20);
  expand_tab.setText("Click the tabs below to expand/compact each menu:");
  panelExSet = new GPanel(this, 700, 130, 250, widthy_IV_values*7, "a");
  panelExSet.setCollapsible(true);
  panelExSet.setCollapsed(true);
  panelExSet.setDraggable(false);
  panelExSet.setText("Update settings and plot options");
  panelExSet.setOpaque(true);
  btnSvSet = new GButton(this, x, y-5, x_width_btn, y_width_btn);
  btnSvSet.setText("Update settings");
  max_iv = new GTextField(this, x+55, y_IV_values+y_shift, x_width_btn-50, 20, G4P.SCROLLBARS_NONE);
  max_iv.setOpaque(true);
  max_iv.setText("5");
  max_iv_lbl = new GLabel(this, x-40, y_IV_values+y_shift, x_width_btn, 30);
  max_iv_lbl.setText("N. IV to display");
  max_iv_lbl.setOpaque(false);
  max_iv_lbl.setTextAlign(GAlign.RIGHT, null);
  if (sql) {
    database_MySQL = new GCheckbox(this, x, y+40, x_width_btn, y_width_btn);
    database_MySQL.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
    database_MySQL.setText("Database");
    database_MySQL.setOpaque(false);
  }
  refresh_graph = new GCheckbox(this, x+100, y_IV_values+y_shift, x_width_btn+50, 20);
  refresh_graph.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  refresh_graph.setText("Refresh graphs");
  refresh_graph.setSelected(true);
  refresh_graph.setOpaque(false);

  if (eth) {
    con_ethernet = new GCheckbox(this, x+100, y+40, x_width_btn, y_width_btn);
    con_ethernet.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
    con_ethernet.setText("Ethernet");
    con_ethernet.setOpaque(false);
  }
  panelExSet.addControl(max_iv);
  panelExSet.addControl(max_iv_lbl);
  panelExSet.addControl(btnSvSet);

  if (sql)panelExSet.addControl(database_MySQL);
  if (eth)panelExSet.addControl(con_ethernet);
  panelExSet.addControl(refresh_graph);
}


public void createCalibration() {
  int x = 120;
  int y =30;
  panelCal = new GPanel(this, 700, 150, x_width_btn*2+20, widthy_IV_values*4, "a");
  panelCal.setCollapsible(true);
  panelCal.setCollapsed(true);
  panelCal.setDraggable(false);
  panelCal.setText("Calibration");
  panelCal.setOpaque(true);
  btnCalPhoto = new GButton(this, x-25, y+20, x_width_btn, y_width_btn-10);
  btnCalPhoto.setText("Photodiode");
  btnCalTime = new GButton(this, x-115, y+20, x_width_btn, y_width_btn-10);
  btnCalTime.setText("Time");
  cal_photo = new GTextField(this, x-25, y, x_width_btn-50, y_width_btn-10, G4P.SCROLLBARS_NONE);
  panelCal.addControl(btnCalPhoto);
  panelCal.addControl(btnCalTime);
  panelCal.addControl(cal_photo);
  cal_photo.setText("");
}


public void createTextContainers() {
  int redude_x= 25;
  x_IV_values=200;
  panelIV = new GPanel(this, 50+redude_x, 110, widthx_IV_values+widthx_lbl_IV+25-redude_x, widthy_IV_values*9, "IV settings");
  panelIV.setCollapsible(false);
  panelIV.setText("IV settings");
  panelIV.setDraggable(false);
  panelIV.setOpaque(true);
  startV = new GTextField(this, x_IV_values, y_IV_values, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  startV.setOpaque(true);
  startV.setText("-1");
  startV_lbl = new GLabel(this, x_IV_values-distance_lbl_IV, y_IV_values, widthx_lbl_IV, widthy_lbl_IV);
  startV_lbl.setText("Start (V)");
  startV_lbl.setOpaque(false);
  startV_lbl.setTextAlign(GAlign.RIGHT, null);
  stopV = new GTextField(this, x_IV_values, y_IV_values+distance_IV, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  stopV.setOpaque(true);
  stopV.setText("1");
  stopV_lbl = new GLabel(this, x_IV_values-distance_lbl_IV, y_IV_values+distance_IV, widthx_lbl_IV, widthy_lbl_IV);
  stopV_lbl.setText("Stop (V)");
  stopV_lbl.setTextAlign(GAlign.RIGHT, null);
  stopV_lbl.setOpaque(false);
  numPoints = new GTextField(this, x_IV_values, y_IV_values+distance_IV*2, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  numPoints.setOpaque(true);
  numPoints.setText("10");
  numPoints_lbl = new GLabel(this, x_IV_values-distance_lbl_IV, y_IV_values+distance_IV*2, widthx_lbl_IV, widthy_lbl_IV);
  numPoints_lbl.setText("N. points");
  numPoints_lbl.setTextAlign(GAlign.RIGHT, null);
  numPoints_lbl.setOpaque(false);
  numMeasurements = new GTextField(this, x_IV_values, y_IV_values+distance_IV*3, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  numMeasurements.setOpaque(true);
  numMeasurements.setText("1");
  numMeasurements_lbl = new GLabel(this, x_IV_values-distance_lbl_IV, y_IV_values+distance_IV*3, widthx_lbl_IV, widthy_lbl_IV+10);
  numMeasurements_lbl.setText("N. of Measurements\n(0 for unlimited)");
  numMeasurements_lbl.setTextAlign(GAlign.RIGHT, null);
  numMeasurements_lbl.setOpaque(false);
  panelIV.addControl(startV); 
  panelIV.addControl(startV_lbl);
  Serial_text_M = new GTextArea(this, 700, 190, 200, 100, G4P.SCROLLBARS_VERTICAL_ONLY);
  Serial_text_M.setOpaque(true);
  ;
  panelIV.addControl(stopV); 
  panelIV.addControl(stopV_lbl);
  panelIV.addControl(numPoints); 
  panelIV.addControl(numPoints_lbl);
  panelIV.addControl(numMeasurements); 
  panelIV.addControl(numMeasurements_lbl);
}
public void updateStatusBar(String s) {
  lblStatus.setText("Status: " + s);
}
public void debug_clicked(GCheckbox source, GEvent event) { //_CODE_:Debug:212813:
} //_CODE_:Debug:212813:

public void autosave_clicked(GCheckbox source, GEvent event) { 
  File f = new File(folder.getText()+fs+filename.getText()+".txt");
  if ( autoSaveOpt.isSelected() && folder.getText() =="") {
    handleMessageDialog("Chose a folder first!");
    autoSaveOpt.setSelected(false);
  } else if  (autoSaveOpt.isSelected() && f.exists() && !f.isDirectory()) {
    // check that the file doesn't exist already

      handleMessageDialog("File already exist! Chose a different filename.");
    autoSaveOpt.setSelected(false);
  } else if  (autoSaveOpt.isSelected() && filename.getText()=="") {
    // check that the file doesn't exist already

    handleMessageDialog("Specify a filename");
    autoSaveOpt.setSelected(false);
  } else if  (measuring==true) {
    // check that the file doesn't exist already

    handleMessageDialog("Cannot change while measuring");
    autoSaveOpt.setSelected(false);
  } else {
  }
}

public void irr_clicked(GCheckbox source, GEvent event) { 
  if (ready==true) {
    if ( Irr.isSelected() == true) {
      Irr.setText("Auto irradiance");
      sett_irr.setText(cal_photo.getText().trim());
      sett_irr.setVisible(false);

      sett_irr.setText(cal_photo.getText().trim());
    } else {
      Irr.setText("Irradiance\n(set value mW/cm2)");
      sett_irr.setText("100");
      sett_irr.setVisible(true);
    }
  } else if (lblStatus.getText().equals("Status: Waiting for connection")==true) {
    if ( Irr.isSelected() == true) {
      Irr.setText("Auto irradiance");
      sett_irr.setText(cal_photo.getText().trim());
      sett_irr.setVisible(false);
    } else {
      Irr.setText("Irradiance\n(set value mW/cm2)");
      sett_irr.setText("100");
      sett_irr.setVisible(true);
    }
  } else {
    handleMessageDialog("Don't change while measuring. Stop and wait for \"Ready to measure\"!");
    if ( Irr.isSelected() == true) {
      Irr.setSelected(false);
    } else {
      Irr.setSelected(true);
    }
  }
}

public void DS18_clicked(GCheckbox source, GEvent event) { 
  if (ready==true) {
  } else if (lblStatus.getText().equals("Status: Waiting for connection")==true) {
  } else {
    handleMessageDialog("Don't change while measuring. Stop and wait for \"Ready to measure\"!");
    if ( DS18.isSelected() == true) {
      DS18.setSelected(false);
    } else {
      DS18.setSelected(true);
    }
  }
}

public void DHT22_clicked(GCheckbox source, GEvent event) { 
  if (ready==true) {
  } else if (lblStatus.getText().equals("Status: Waiting for connection")==true) {
  } else {
    handleMessageDialog("Don't change while measuring. Stop and wait for \"Ready to measure\"!");
    if ( DHT22.isSelected() == true) {
      DHT22.setSelected(false);
    } else {
      DHT22.setSelected(true);
    }
  }
}



void folderSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    path_save = selection.getAbsolutePath();
    folder.setText(path_save);
  }
}

int md_mtype;
// G4P code for message dialogs
public void handleMessageDialog(String message) {
  // Determine message type
  int mtype;
  switch(md_mtype) {
  default:
  case 0: 
    mtype = G4P.PLAIN; 
    break;
  case 1: 
    mtype = G4P.ERROR; 
    break;
  case 2: 
    mtype = G4P.INFO; 
    break;
  case 3: 
    mtype = G4P.WARNING; 
    break;
  case 4: 
    mtype = G4P.QUERY; 
    break;
  }
  String title = "Message";
  G4P.showMessage(this, message, title, mtype);
}


/**
 * Appends text to the end of a text file located in the data directory, 
 * creates the file if it does not exist.
 * Can be used for big files with lots of rows, 
 * existing lines will not be rewritten
 */
void appendTextToFile(String filename, String text) {
  File f = new File(dataPath(filename));
  if (!f.exists()) {
    createFile(f);
  }
  try {
    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(f, true)));
    out.println(text);
    out.close();
  }
  catch (IOException e) {
    e.printStackTrace();
    handleMessageDialog("File "+filename + "cannot be written");
  }
}


/**
 * Creates a new file including all subfolders
 */
void createFile(File f) {
  File parentDir = f.getParentFile();
  try {
    parentDir.mkdirs(); 
    f.createNewFile();
  }
  catch(Exception e) {
    e.printStackTrace();
  }
}    

// Graphic frames used to group controls
ArrayList<Rectangle> rects ;

// Simple graphical frame to group controls
public void showFrame(Rectangle r) {
  noFill();
  strokeWeight(1);
  stroke(color(240, 240, 255));
  rect(r.x, r.y, r.width, r.height);
  stroke(color(0));
  rect(r.x+1, r.y+1, r.width, r.height);
}


synchronized public void win_draw1(GWinApplet appc, GWinData data) { //_CODE_:Calibr_window:917203:
  appc.background(230);
} //_CODE_:Calibr_window:917203:




// Time methods
void sendTimeMessage(char header, long time) {  
  String timeStr = String.valueOf(time);  
  myPort.write(header);  // send header and time to arduino
  myPort.write(timeStr);
}

long getTimeNow() {
  // java time is in ms, we want secs    
  GregorianCalendar cal = new GregorianCalendar();
  cal.setTime(new Date());
  int  tzo = cal.get(Calendar.ZONE_OFFSET);
  int  dst = cal.get(Calendar.DST_OFFSET);
  long now = (cal.getTimeInMillis() / 1000) ; 
  now = now + (tzo/1000) + (dst/1000); 
  return now;
}
