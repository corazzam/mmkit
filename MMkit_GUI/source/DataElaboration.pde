public double[] estrapolate_pce(float[] volt, float[] cur, float irr) {
  double M_power=0;
  double pce=0;

  // find Pmax
  for (int i=0; i< volt.length; i++) {
    double tmp_max;
    tmp_max = volt[i]/1000*cur[i];
    if (tmp_max>M_power) 
      M_power = tmp_max;
  }

  println("PM: "+M_power);

  double area_cell = Double.parseDouble(area.getText());
  pce = M_power/area_cell/irr*100;

  double[] volt_double = new double[volt.length];
  double[] cur_double = new double[volt.length];

  // keep voc positive
  double [] volt_2 =  new double[2];
  double [] cur_2 = new double[2];
  double[][] voc = new double[volt.length][1];  //max voc for every point
  double[] zero = {
    0
  };

  for (int i = 0; i < volt.length; i++)
  {
    volt_double[i] = (double) volt[i]/1000;
    cur_double[i] = (double) -cur[i];

    // calculate interception of isc=0 for all data points
    if (i>=1) {
      volt_2[1] = volt_double[i];
      volt_2[0] = volt_double[i-1];
      cur_2[1] = cur_double[i];
      cur_2[0] = cur_double[i-1];
      voc[i-1] = interpLinear(cur_2, volt_2, zero);
    }
  }

  double[] isc = interpLinear(volt_double, cur_double, zero);
  double voc_pos=voc[0][0]; // start from the initial value of interception
  int i =1;
  // take the voc as the first interception of isc=0 with a positive and monotic growing value
  while (! (voc_pos>=0) && i<volt.length) {
    //println("voc_tmp: "+voc[i][0]);
    voc_pos = voc[i][0];
    i++;
  }
  // if it was not possible to find a positive value
  if (!(voc_pos>=0) && i==volt.length) {
    voc_pos=Double.NaN;
  }


  double FF = -M_power/voc_pos/isc[0]*100;
  //println("voc: "+voc_pos+" FF: "+FF);

  double[] para = {
    pce, isc[0], voc_pos, FF, M_power
  };
  return para;
}



/**
 * <p>Static methods for doing useful math</p><hr>
 *
 * @author  : $Author: brian $
 * @version : $Revision: 1.1 $
 *
 * <hr><p><font size="-1" color="#336699"><a href="http://www.mbari.org">
 * The Monterey Bay Aquarium Research Institute (MBARI)</a> provides this
 * documentation and code &quot;as is&quot;, with no warranty, express or
 * implied, of its quality or consistency. It is provided without support and
 * without obligation on the part of MBARI to assist in its use, correction,
 * modification, or enhancement. This information should not be published or
 * distributed to third parties without specific written permission from
 * MBARI.</font></p><br>
 *
 * <font size="-1" color="#336699">Copyright 2002 MBARI.<br>
 * MBARI Proprietary Information. All rights reserved.</font><br><hr><br>
 *
 */

public   double[] interpLinear(double[] x, double[] y, double[] xi) throws IllegalArgumentException {

  if (x.length != y.length) {
    throw new IllegalArgumentException("X and Y must be the same length");
  }
  if (x.length == 1) {
    throw new IllegalArgumentException("X must contain more than one value");
  }
  double[] dx = new double[x.length - 1];
  double[] dy = new double[x.length - 1];
  double[] slope = new double[x.length - 1];
  double[] intercept = new double[x.length - 1];

  // Calculate the line equation (i.e. slope and intercept) between each point
  for (int i = 0; i < x.length - 1; i++) {
    dx[i] = x[i + 1] - x[i];
    if (dx[i] == 0) {
      println("X must be montotonic. A duplicate " + "x-value was found");
    }
    if (dx[i] < 0) {
      println("X must be sorted");
    }
    dy[i] = y[i + 1] - y[i];
    slope[i] = dy[i] / dx[i];
    intercept[i] = y[i] - x[i] * slope[i];
  }

  // Perform the interpolation here
  double[] yi = new double[xi.length];
  for (int i = 0; i < xi.length; i++) {
    if ((xi[i] > x[x.length - 1]) || (xi[i] < x[0])) {
      yi[i] = Double.NaN;
    } else {
      int loc = Arrays.binarySearch(x, xi[i]);
      if (loc < -1) {
        loc = -loc - 2;
        yi[i] = slope[loc] * xi[i] + intercept[loc];
      } else {
        yi[i] = y[loc];
      }
    }
  }

  return yi;
}

public static final BigDecimal[] interpLinear(BigDecimal[] x, BigDecimal[] y, BigDecimal[] xi) {
  if (x.length != y.length) {
    throw new IllegalArgumentException("X and Y must be the same length");
  }
  if (x.length == 1) {
    throw new IllegalArgumentException("X must contain more than one value");
  }
  BigDecimal[] dx = new BigDecimal[x.length - 1];
  BigDecimal[] dy = new BigDecimal[x.length - 1];
  BigDecimal[] slope = new BigDecimal[x.length - 1];
  BigDecimal[] intercept = new BigDecimal[x.length - 1];

  // Calculate the line equation (i.e. slope and intercept) between each point
  BigInteger zero = new BigInteger("0");
  BigDecimal minusOne = new BigDecimal(-1);

  for (int i = 0; i < x.length - 1; i++) {
    //dx[i] = x[i + 1] - x[i];
    dx[i] = x[i + 1].subtract(x[i]);
    if (dx[i].equals(new BigDecimal(zero, dx[i].scale()))) {
      throw new IllegalArgumentException("X must be montotonic. A duplicate " + "x-value was found");
    }
    if (dx[i].signum() < 0) {
      throw new IllegalArgumentException("X must be sorted");
    }
    //dy[i] = y[i + 1] - y[i];
    dy[i] = y[i + 1].subtract(y[i]);
    //slope[i] = dy[i] / dx[i];
    slope[i] = dy[i].divide(dx[i]);
    //intercept[i] = y[i] - x[i] * slope[i];
    intercept[i] = x[i].multiply(slope[i]).subtract(y[i]).multiply(minusOne);
    //intercept[i] = y[i].subtract(x[i]).multiply(slope[i]);
  }

  // Perform the interpolation here
  BigDecimal[] yi = new BigDecimal[xi.length];
  for (int i = 0; i < xi.length; i++) {
    //if ((xi[i] > x[x.length - 1]) || (xi[i] < x[0])) {
    if (xi[i].compareTo(x[x.length - 1]) > 0 || xi[i].compareTo(x[0]) < 0) {
      yi[i] = null; // same as NaN
    } else {
      int loc = Arrays.binarySearch(x, xi[i]);
      if (loc < -1) {
        loc = -loc - 2;
        //yi[i] = slope[loc] * xi[i] + intercept[loc];
        yi[i] = slope[loc].multiply(xi[i]).add(intercept[loc]);
      } else {
        yi[i] = y[loc];
      }
    }
  }

  return yi;
}

public   double[] interpLinear(long[] x, double[] y, long[] xi) throws IllegalArgumentException {

  double[] xd = new double[x.length];
  for (int i = 0; i < x.length; i++) {
    xd[i] = (double) x[i];
  }

  double[] xid = new double[xi.length];
  for (int i = 0; i < xi.length; i++) {
    xid[i] = (double) xi[i];
  }

  return interpLinear(xd, y, xid);
}



public void addTable(Table table, java.lang.String filename) {
  Table added_table;
  boolean sv_chk;
  File f;
  f = new File(filename);

  if (f.exists() && !f.isDirectory()) { 

    //tmp = loadTable(filename,"header");
    added_table = loadTable(filename);
    for (TableRow row : table.rows ()) {
      added_table.addRow(row);
    }
    sv_chk = saveTable(added_table, filename);
  } else {
    sv_chk = saveTable(table, filename); 
    if (sv_chk == false) {
      handleMessageDialog("Couldn't save! Probably file is open");
    }
  }
}

// Recreate datatable to be saved in csv file
public void recreateTableIV(Table table) {
  table.addColumn("id");
  table.addColumn("Time(Unix)");
  table.addColumn("Time");
  table.addColumn("Voltage(V)");
  table.addColumn("Current(mA)");
  table.addColumn("PCE(%)");
  table.addColumn("Isc(mA)");
  table.addColumn("Voc(V)");
  table.addColumn("FF(%)");
  table.addColumn("Humidity(R.H.)");
  table.addColumn("Temperature(C)");
  table.addColumn("Irradiance(mW/cm2)");
  table.addColumn("Pmax(mW)");
  table.addColumn("Area(cm2)");
  table.addColumn("Note");
}

public void recreateTableSum(Table table) {
  table.addColumn("id");
  table.addColumn("Time(Unix)");
  table.addColumn("Time");
  table.addColumn("PCE(%)");
  table.addColumn("Isc(mA)");
  table.addColumn("Voc(V)");
  table.addColumn("FF(%)");
  table.addColumn("Humidity(R.H.)");
  table.addColumn("Temperature(C)");
  table.addColumn("Irradiance(mW/cm2)");
  table.addColumn("Pmax(mW)");
  table.addColumn("Area(cm2)");
  table.addColumn("Note");
}

public void recreateTableSumS(Table table) {
  table.addColumn("Time(Unix)");
  table.addColumn("Time");
  table.addColumn("Humidity(R.H.)");
  table.addColumn("Temperature(C)");
  table.addColumn("Irradiance(mW/cm2)");
  table.addColumn("Note");
}

public void cleanData() {
  IV_data= new Table(); //Empty data
  Meas_summary= new Table(); //Empty data
  savedata="";
  recreateTableSum(Meas_summary);
  recreateTableIV(IV_data);

  count_data =0;
  counter_iv =0;
  //Erase plot too
  PCE_plot = new GPlot(this);
  irr_plot = new GPlot(this);
  temp_plot = new GPlot(this);
  hum_plot = new GPlot(this);
  IV_plot = new GPlot(this);
  l2=0;
  l1=0;
}

public void autoSave() {
  file= folder.getText()+fs+filename.getText()+"_"+note.getText().trim()+".txt";  // for database exportation
  file_sum= folder.getText()+"\\"+filename.getText()+"_summary.csv";
  file_iv= folder.getText()+"\\"+filename.getText()+"_iv_"+counter_iv +".csv";
  //save text file for DB
  appendTextToFile(file, savedata);
  try {
    addTable(Meas_summary, file_sum);
    addTable(IV_data, file_iv);
    // when the data is added, then we can clean the data array
    Meas_summary= new Table();
    IV_data= new Table();
    recreateTableIV(IV_data);
    recreateTableSum(Meas_summary);
    println("Autosave IV done");
  }
  catch(Exception ioe)
  {
    System.err.println("IOException: " + ioe.getMessage());
    handleMessageDialog("Couldn't save! Probably csv file is open");
  }
}

public void autoSaveSensors() {
  file= folder.getText()+fs+filename.getText()+"_"+note.getText().trim()+".txt";  // for database exportation
  file_sum= folder.getText()+"\\"+filename.getText()+"_summarySensors.csv";

  //save text file for DB
  appendTextToFile(file, savedata);
  try {
    //don't care about these columns for sensors
    addTable(Meas_summarySens, file_sum);
    // when the data is added, then we can clean the data array
    Meas_summarySens= new Table();
    recreateTableSumS(Meas_summarySens);
    println("Autosave sensors done");
  }
  catch(Exception ioe)
  {
    System.err.println("IOException: " + ioe.getMessage());
    handleMessageDialog("Couldn't save! Probably csv file is open");
  }
}


public int[] generate_colors(int size) {
  //change colour last iv curve
    Color color_t; //random color, but can be bright or dull
    
  //to get n=size different colors
  float hue_step=(float)1/size;
  int[] colors_array = new int[size];
  for (int i=0; i< size; i++) {
    float hue = (float) hue_step*(i);
    float saturation = 0.9f;//1.0 for brilliant, 0.0 for dull
    float luminance = 1.0f; //1.0 for brighter, 0.0 for black
    color_t = Color.getHSBColor(hue, saturation, luminance);
    colors_array[i] = color_t.getRGB();
  }
  return colors_array;
}


public static boolean isNumeric(String str)  
{  
  try  
  {  
    double d = Double.parseDouble(str);
  }  
  catch(NumberFormatException nfe)  
  {  
    return false;
  }  
  return true;
}
