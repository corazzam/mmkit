// v. 33 
// added debug mode
// changed if into else if for "IP" data selector in Serial_processing tab

boolean debug=false;

import grafica.*;
import g4p_controls.*;
import processing.serial.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.awt.Rectangle;
import java.util.*; 
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.awt.Color;
import java.security.SecureRandom;
import javax.swing.ImageIcon;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//Email
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.BodyPart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeMultipart;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.DataHandler;

// Settings default size of windows
int width_window = 1000;
int height_window = 810;

//  serial port:
Serial myPort;  
GDropList cboSerial;
String portName;

boolean eth = false;
boolean sql = false;

// Graphs creation
GPlot IV_plot = new GPlot(this);
GPlot env = new GPlot(this);
GPlot PCE_plot = new GPlot(this);
GPlot irr_plot = new GPlot(this);
GPlot temp_plot = new GPlot(this);
GPlot hum_plot = new GPlot(this);

float[] xLimAutoscale;
float[] yLimAutoscale;
int x_pos_cnt=20;
int y_pos_cnt=50;
int y_width_btn=30;
int x_width_btn=80;
int taskbars = 70; // max size taskabr in pixel is 40 plus 20 pixel titlebar plus 10 of tollerance
int logo_x=20;
int logo_y=40;

PImage img, bck;
//final static String ICON  = "MMkit_icon.png";
//final static String TITLE = "MMkit";
boolean compact_mode=false;
int heigh_reduction;

public void createSerialListBox() {
  String[] serialPortList = Serial.list(); // find all serial ports

  if (serialPortList!=null && serialPortList.length>0) {
    //System.out.println(serialPortList.length);
  } else {
    //System.out.println("Array is not initialized or empty");
    serialPortList = new String[] { 
      "No COMs"
    };
  }

  cboSerial = new GDropList(this, x_pos_cnt, y_pos_cnt-45, x_pos_cnt+50, y_pos_cnt+120, 5); // create serial list box
  cboSerial.setItems(serialPortList, 0);
  portName = cboSerial.getSelectedText(); // set global portName
}

boolean debug_meas=false;
int debug_i=0;

void setup() {
  frameRate(20); // limit framerate to limit computer comsuption

  // Make sure the program fit different screen resolution (width<1000)
  float red_factor=2;
  if (displayHeight < 660+taskbars) {  // note that x-resolution must be at least 1000px
    red_factor = 1;  // height window (hw)=570
    compact_mode =true;
  } else if (displayHeight < height_window+taskbars) {
    red_factor=2;   // hw = 660
    compact_mode =true;
  } else if (displayHeight >= height_window+taskbars)
    compact_mode=false;
  else {
    red_factor=2;   // hw = 660
    compact_mode =true;
  }
  int displayHeightMax = 900;

  if (compact_mode==true) {
    heigh_reduction=int(float((displayHeightMax-height_window))/red_factor+20);
    compact_mode=true;
    compact_mode_on();
    height_window=height_window-heigh_reduction*2-20;
    println("heigh_reduction: "+heigh_reduction);
    println("display_height: "+displayHeight);
    println("heigh_window: "+height_window);
  }

  // create window
  size(width_window, height_window);
  //frame.setResizable(true);
  //size(width_window, height_window,OPENGL);
  rects = new ArrayList<Rectangle> ();
  createGUI();

  // create data containers
  recreateTableSum(Meas_summary);
  recreateTableIV(IV_data);
  recreateTableSumS(Meas_summarySens);
  
  max_iv_curve_graph = Integer.parseInt(max_iv.getText().trim());
  savedataPartial = new String[max_iv_curve_graph];
  savedataPartial_ind = new Integer[max_iv_curve_graph]; 
  time_start=millis()/1000; //time when the software started
  updateStatusBar("Waiting for connection");
  //img = loadImage("mmkit-logo.png");
  //bck = loadImage("sun6.png");
  //changeAppIcon( loadImage("mmkit-icon.png") );
  createSerialListBox();  // for automatic update of port, you can move it to draw. But the program will use much more CPU.

  if (debug==true) {
    ready=true;
    ser_connected=true;
  }
}



void draw() {
  background(255, 255, 255);
  //image(img, logo_x, logo_y, 270, 70);

  update_plots();

  for (Rectangle r : rects)
    showFrame(r);
  if (ser_connected==true && debug==false) {
    while (myPort.available () > 0) {
      if (calibrPh==false && calibrTi==false) {
        String inBuffer = myPort.readStringUntil(10);   //10 is the ASCII code for new line
        if (inBuffer != null) {
          println(inBuffer);
          update_data(inBuffer, IV_plot, temp_plot);
        }
      }
      if (calibrPh==true) {
        String inBuffer = myPort.readStringUntil(10);   //10 is the ASCII code for new line
        if (inBuffer != null) {
          Serial_text.appendText(inBuffer);
        }
      }
      if (calibrTi==true) {
        //String inBuffer = myPort.readStringUntil(10);   //10 is the ASCII code for new line
        String val = myPort.readStringUntil(10);
        Serial_text.appendText((val));
        if (val!=null && val.length()>1) {
          val = val.substring (0, 1);
          if (val.equals("T")==true) {
            long t = getTimeNow();
            sendTimeMessage(TIME_HEADER, t); 
            updateStatusBar("Time calibrated!");
          }
        }
      }
    }
  }
}


// if mouse is clicked in the logo go to the MMkit homepage
void mouseClicked() {
  if (mouseX>logo_x+100&& mouseX<logo_x+200  &&mouseY>logo_y&& mouseY<logo_y+70 )
    link("http://plasticphotovoltaics.org/mmkit.html");
}


void changeAppIcon(PImage img) {
  final PGraphics pg = createGraphics(48, 48, JAVA2D);

  pg.beginDraw();
  pg.image(img, 0, 0, 45, 45);
  pg.endDraw();

  frame.setIconImage(pg.image);
}

// resize window in compact mode
void compact_mode_on()
{
  panelDim_IV = new float[] {
    380, (380-heigh_reduction*2-20)
    };
    
    /* Setting for summary*/
  firstPlotPos = new float[] {
    25, 25
  };
  panelDim = new float[] {
    200, 150-heigh_reduction
  };
  margins = new float[] {
    0, 30, 300, 0
  };
}
