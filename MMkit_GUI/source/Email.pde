// set up your own email account to allow sending email with it from MMKit GUI

final String username = "whatever@gmail.com";
final String password = "something";
boolean email_sent = false;


public void send_email(String pce_meas, String email_to) {

  Serial_text_M.appendText("sending email");

  Properties props = new Properties();
  props.put("mail.smtp.auth", "true");
  props.put("mail.smtp.starttls.enable", "true");
  props.put("mail.smtp.host", "smtp.gmail.com");
  props.put("mail.smtp.port", "587");


  Session session = Session.getInstance(props, 
  new javax.mail.Authenticator() {
    protected PasswordAuthentication getPasswordAuthentication() {
      return new PasswordAuthentication(username, password);
    }
  }
  );

  try {
    // Create a default MimeMessage object.
    Message message = new MimeMessage(session);

    // Set From: header field of the header.
    message.setFrom(new InternetAddress(username));

    // Set To: header field of the header.
    message.setRecipients(Message.RecipientType.TO, 
    InternetAddress.parse(email.getText().trim()));
    // Set Subject: header field
    message.setSubject("MMkit stability report");

    // Create the message part
    BodyPart messageBodyPart = new MimeBodyPart();

    // Now set the actual message
    messageBodyPart.setText("Hello,"
      + "\n\n Your device under test has reached a pce of "+pce_meas+".");

    // Create a multipar message
    Multipart multipart = new MimeMultipart();

    // Set text message part
    multipart.addBodyPart(messageBodyPart);

    // attachment iv summary 
    messageBodyPart = new MimeBodyPart();
    DataSource source = new FileDataSource(file_sum);
    messageBodyPart.setDataHandler(new DataHandler(source));
    messageBodyPart.setFileName("summary_iv.csv");
    multipart.addBodyPart(messageBodyPart);        

    // attachment for database
    messageBodyPart = new MimeBodyPart();
    source = new FileDataSource(file);
    messageBodyPart.setDataHandler(new DataHandler(source));
    messageBodyPart.setFileName("database_file.txt");
    multipart.addBodyPart(messageBodyPart);

    // attachment screenshot 
    messageBodyPart = new MimeBodyPart();
    String file_screenshot = folder.getText()+fs+"screenshot"+hour()+"_"+minute()+"_"+second()+"_"+day()+"_"+month()+"_"+year() +".png";
    saveFrame(file_screenshot);
    source = new FileDataSource(file_screenshot);
    messageBodyPart.setDataHandler(new DataHandler(source));
    messageBodyPart.setFileName("screenshot.png");
    multipart.addBodyPart(messageBodyPart);

    // Send the complete message parts
    message.setContent(multipart);

    // Send message
    Transport.send(message);

    System.out.println("Sent message successfully....");

    Serial_text_M.appendText("Email sent");
  } 
  catch (MessagingException e) {
    throw new RuntimeException(e);
  }

  email_sent = true;
}
