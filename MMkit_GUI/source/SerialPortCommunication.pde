/*
This method uses data from serial port. It updates the vectors containing the data
 to plot, distinguishing among the different types of data. 
 The initial part of the data trasmitted must begin with 2 letters which identify a
 specific data type. JSON is not compatible because of the special character"
 */
boolean serialPortCreated = false;
String savedata="";
String alldata="";
String eol = System.getProperty("line.separator");
String fs = System.getProperty("file.separator");
ArrayList<Float> dataV = new ArrayList(); // where the most recent voltage data is stored as floats
ArrayList<Float> dataI = new ArrayList(); // where the most recent current data is stored as floats
ArrayList<Float> dataVAll = new ArrayList(); // where all historical voltage data is stored as floats
ArrayList<Float> dataIAll = new ArrayList(); // where all historical current data is stored as floats
float dataVMin = 1.5; // minimum voltage value in data
float dataVMax = -1.5; // maximum voltage value in data
float dataIMax = -250; // maximum current value in data
float dataIMin = 250; // minimum current value in data
float maxAbsCurrent; // maximum magnitude of current
float maxAbsVoltage; // maximum magnitude of voltage
boolean IVpoints = false;
//table code
Table IV_data = new Table();
Table Meas_summary = new Table();
Table Meas_summarySens = new Table();
int counter;
int counter_iv=0;
boolean start_iv=true;
int max_iv_curve_graph;  //(the value is inserted must be reduced by one to have the correct result);
String file;
String file_sum;
String file_iv;
String file_sum_sensors;
boolean receiving_data;
//legend in IV graph
static long l1;
static long l2;
int part_save = 0;
String[] savedataPartial;
Integer[] savedataPartial_ind;
int max_memory = 100;
//color
Random rand = new Random();


public void update_data(String serial, GPlot plot_iv, GPlot temp_plot) {
  if (serial.length()>=2) {
    receiving_data = true;
    String selector = serial.substring (0, 2); // extract the first 2 character from sting
    String data = serial.substring(2); // extract the data
    // Arduino is sending an IV curve

    //////////////////////////////////////// complete IV ///////////////////////////////////////////
    if (selector.equals("IV")==true) {

      String[] i_v = splitTokens(data, "[] ");
      println(i_v[1]);
      String curr = i_v[1];  //first part of the token sent are the current points
      String volt = i_v[3];  //third part of the data sent are the voltage points
      // IV data to save
      savedata = "IV{\"current\":[" + curr +"],\"voltage\":" + "[" + volt + "]}" +"TI["+ hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year() + "]";
      float[] c_pts = string2float(curr);
      float[] v_pts = string2float(volt);


      // initialize sensor variables
      float hu=0; 
      float te=0; 
      float ir=0;
      // add to save data and to table other sensors information, if connected
      TableRow newRow = Meas_summary.addRow();
      try {
        if (DHT22.isSelected()==true && DS18.isSelected()==true && Irr.isSelected()==true) {

          hu = Float.parseFloat(i_v[5]); 
          float ta = Float.parseFloat(i_v[7]); 
          newRow.setFloat("Humidity(R.H.)", hu); 
          savedata=savedata+"HU["+hu+"]";
          // println("iv9: "+ i_v[9]);
          te = Float.parseFloat(i_v[9]); 
          newRow.setFloat("Temperature(C)", te);
          savedata=savedata+"DS["+te+"]";
          //println("te: "+ te);
          ir = Float.parseFloat(i_v[11]); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        } else if (DHT22.isSelected()==true && DS18.isSelected()==true && Irr.isSelected()==false) {
          hu = Float.parseFloat(i_v[5]); 
          float ta = Float.parseFloat(i_v[7]); 
          newRow.setFloat("Humidity(R.H.)", hu); 
          savedata=savedata+"HU["+hu+"]";
          te = Float.parseFloat(i_v[9]); 
          newRow.setFloat("Temperature(C)", te);
          savedata=savedata+"DS["+te+"]";
          ir = Float.parseFloat(sett_irr.getText().trim()); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        } else if (DHT22.isSelected()==true && DS18.isSelected()==false && Irr.isSelected()==true) {
          hu = Float.parseFloat(i_v[5]); 
          float ta = Float.parseFloat(i_v[7]); 
          newRow.setFloat("Humidity(R.H.)", hu); 
          savedata=savedata+"HU["+hu+"]";
          ir = Float.parseFloat(i_v[9]); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        } else if (DHT22.isSelected()==true && DS18.isSelected()==false && Irr.isSelected()==false) {
          hu = Float.parseFloat(i_v[5]); 
          float ta = Float.parseFloat(i_v[7]); 
          newRow.setFloat("Humidity(R.H.)", hu); 
          savedata=savedata+"HU["+hu+"]";
          ir = Float.parseFloat(sett_irr.getText().trim()); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        } else if (DHT22.isSelected()==false && DS18.isSelected()==true && Irr.isSelected()==true) {
          te = Float.parseFloat(i_v[5]); 
          newRow.setFloat("Temperature(C)", te);
          savedata=savedata+"DS["+te+"]";
          ir = Float.parseFloat(i_v[7]); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        } else if (DHT22.isSelected()==false && DS18.isSelected()==true && Irr.isSelected()==false) {
          te = Float.parseFloat(i_v[5]); 
          newRow.setFloat("Temperature(C)", te);
          savedata=savedata+"DS["+te+"]";
          ir = Float.parseFloat(sett_irr.getText().trim()); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        } else if (DHT22.isSelected()==false && DS18.isSelected()==false && Irr.isSelected()==true) {
          ir = Float.parseFloat(i_v[5]); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        }
        // no sensors are selected
        else { 
          ir = Float.parseFloat(sett_irr.getText().trim()); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        }
      }
      catch (NumberFormatException   a) {
        handleMessageDialog("Error some sensors are probably not connected!");
        updateStatusBar("Stop! Finishing current measurement"); // display connecting status 
        ready=true; //ready to measure again - this should come
        measuring =false; //not sure
        String command = "O";
        myPort.write(command);
      }
      catch (ArrayIndexOutOfBoundsException  a) {
        handleMessageDialog("Do not change sensor option while measuring!");
        updateStatusBar("Stop! Finishing current measurement"); // display connecting status 
        ready=true; //ready to measure again - this should come
        measuring =false; //not sure
        String command = "O";
        myPort.write(command);
      }
      //savedata=savedata+eol;

      double[] para = new double[5];
      float pce;

      // extract photovoltaic parameters
      try {
        para = estrapolate_pce(v_pts, c_pts, ir);
        pce = (float)para[0];
      }
      catch (Exception e)
      {
        pce=0;
        para[0]=0;
        para[1]=0;
        para[2]=0;
      }

      //add pce to plot
      PCE_plot.addPoint((millis()/1000-time_start)/60, pce);
      println(para[0], " ", para[1], " ", para[2], " ", para[3], " ", para[4]);
      NumberFormat formatter = new DecimalFormat("#0.0");   
      // print to GUI result of IV curve
      Serial_text_M.appendText("ID: "+Integer.toString(counter_iv)+" PCE: "+ formatter.format(para[0])+" Isc(mA): " + formatter.format(para[1]) + " Voc(V): " + formatter.format(para[2])+" FF(%): " + formatter.format(para[3]) );


      // add data to "Meas_summary" table
      newRow.setString("id", Integer.toString(counter_iv));
      newRow.setString("Time", hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year());
      newRow.setFloat("PCE(%)", (float)para[0]);
      newRow.setFloat("Isc(mA)", (float)para[1]);
      newRow.setFloat("Voc(V)", (float)para[2]);
      newRow.setFloat("FF(%)", (float)para[3]);
      newRow.setFloat("Pmax(mW)", (float)para[4]);
      newRow.setFloat("Time(Unix)", (float)getTimeNow());
      newRow.setString("Note", note.getText());  //note is only for iv curve
      newRow.setFloat("Area(cm2)", Float.parseFloat(area.getText().trim()));

      // add data to "IV_data" table: this data is inserted after every IV point
      newRow = IV_data.addRow();
      newRow.setFloat("PCE(%)", (float)para[0]);
      newRow.setFloat("Isc(mA)", (float)para[1]);
      newRow.setFloat("Voc(V)", (float)para[2]);
      newRow.setFloat("FF(%)", (float)para[3]);
      newRow.setFloat("Pmax(mW)", (float)para[4]);
      newRow.setString("id", Integer.toString(counter_iv));
      newRow.setFloat("Time(Unix)", (float)System.currentTimeMillis() / 1000L);
      newRow.setString("Time", hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year());
      newRow.setString("Note", note.getText());  //note is only for iv curve
      newRow.setFloat("Area(cm2)", Float.parseFloat(area.getText().trim()));
      try {
        if (DHT22.isSelected()==true) { 
          newRow.setFloat("Humidity(R.H.)", hu);
        }
        if (DS18.isSelected()==true) { 
          newRow.setFloat("Temperature(C)", te);
        }
        // irr is always inserted: from sensor or value specified
        newRow.setFloat("Irradiance(mW/cm2)", ir);
      }
      catch (Exception e) {
        handleMessageDialog("Couldn't save to IV_data table");
      }



      // save iv curve in memory for .txt file
      savedataPartial_ind[part_save] = counter_iv;
      savedataPartial[part_save] = savedata;
      println("part "+part_save);
      // colors
      IV_plot.getLayer(Integer.toString(counter_iv)).setPointColor(colors_array[part_save]);
      IV_plot.getLayer(Integer.toString(counter_iv)).setLineColor(colors_array[part_save]);
      //legend
      l2++;
      part_save++;
      
      if (autoSaveOpt.isSelected()==true) {
        // AUTOSAVE ON
        // read filename everytime. iv curve are saved in separate iv file
        autoSave();
        Serial_text_M.setText("");   // when the data is saved, there is no need to show it on the message area
      }


      if (part_save==max_iv_curve_graph) {
        part_save=0;
      }
      // meas_summary has one line for every iv curve
      // autosave: meas_summary is clean every time a data is saved
      if (Meas_summary.getRowCount()==max_iv_curve_graph+1) {
        Meas_summary.removeRow(0);
        Table toSave_IV =new Table();
        recreateTableIV(toSave_IV);
        for (int i=counter_iv-max_iv_curve_graph+1; i<=counter_iv; i++) {
          println("id= "+i);
          int j=0;
          for (TableRow row_to_keep : IV_data.findRows ( Integer.toString (i), "id")) {
            toSave_IV.addRow(row_to_keep);
            println("id rows of measure to keep= "+toSave_IV.getString(j, "id"));
            j++;
          }
        }
        for (TableRow row : IV_data.rows ()) {
          println(row.getString("id"));
        }
        for (TableRow row : toSave_IV.rows ()) {
          println(row.getString("id"));
        }
        IV_data = toSave_IV;
      }

      // keep only curve shown in the legend in memory:
      // part_save is the index of the array savedataPartial: when the index reaches the savedataPartial_ind.length=(max_iv_curve_graph-1)
      // then overwrite initial values

      // if something was inserted in PCE limit then it is possible to send an email (only in stability studies)
      if ( isNumeric(PCE_limit.getText().trim()) && email.getText().trim().equals("")==false &&  (autoSaveOpt.isSelected()==true) && (Float.parseFloat(numMeasurements.getText().trim())>1 || Float.parseFloat(numMeasurements.getText().trim())==0) ) {
        println("send email");
        // the  email is sent only once
        if (pce<Float.parseFloat(PCE_limit.getText().trim()) && email_sent==false) {
          send_email(String.valueOf(pce), email.getText().trim());
        }
      } else if (isNumeric(PCE_limit.getText().trim()) && email.getText().trim().equals("")==false &&  (autoSaveOpt.isSelected()==false))
        Serial_text_M.appendText("Can't use email function without autosave");
      else if (isNumeric(PCE_limit.getText().trim()) && email.getText().trim().equals("")==false &&  Float.parseFloat(numMeasurements.getText().trim())==1)
        Serial_text_M.appendText("Can't use email function with single iv measurement");
      count_data++;
      start_iv=true; //iv curve done
    }

    // these data doesn't have any id information --> it cannot be saved with single iv saving
    else if (selector.equals("DS")==true) {
      String[] t_e = splitTokens(data, "[] ");
      float te = Float.parseFloat(t_e[0]); 
      temp_plot.addPoint((millis()/1000-time_start)/60, te);
      // data is not saved if the autosave mode is not active
      if (autoSaveOpt.isSelected()==false) {
      } else if (autoSaveOpt.isSelected()==true) {
        savedata = "DS[" + te +"]TI["+ hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year() +"]";
        TableRow newRow = Meas_summarySens.addRow();
        newRow.setFloat("Temperature(C)", te);
        newRow.setFloat("Time(Unix)", (float)System.currentTimeMillis() / 1000L);
        newRow.setString("Time", hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year());
        autoSaveSensors();
      }
    } else if (selector.equals("HU")==true) {
      float hu=0;
      try {
        String[] h_u = splitTokens(data, "[] ");
        hu = Float.parseFloat(h_u[0]);
      }
      catch (NumberFormatException a) {
        handleMessageDialog("DHT sensor is probably not connected!");
      }
      if (autoSaveOpt.isSelected()==false) {
      } else if (autoSaveOpt.isSelected()==true) {
        savedata = "HU[" + hu +"]TI["+ hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year() +"]";
        TableRow newRow = Meas_summarySens.addRow();
        newRow.setFloat("Humidity(R.H.)", hu);
        newRow.setFloat("Time(Unix)", (float)System.currentTimeMillis() / 1000L);
        newRow.setString("Time", hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year());
        autoSaveSensors();
      }
      hum_plot.addPoint((millis()/1000-time_start)/60, hu);
    } else if (selector.equals("TA")==true) {
      // TA is the temperature from DHT22, the value is not accurate
      String[] t_a = splitTokens(data, "[] ");
      float ta = Float.parseFloat(t_a[0]);
      savedata = "TE[" + ta +"]TI["+ hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year() +"]";
      String[] list = split(savedata, ' ');
      if (autoSaveOpt.isSelected()==true) {
        autoSaveSensors();
      }
    } else if (selector.equals("IR")==true) {
      String[] i_r = splitTokens(data, "[] ");
      float ir = Float.parseFloat(i_r[0]);
      // add to measurement file
      if (autoSaveOpt.isSelected()==false) {
        TableRow newRow = Meas_summary.addRow();
        newRow.setFloat("Irradiance(mW/cm2)", ir);
        newRow.setFloat("Time(Unix)", (float)System.currentTimeMillis() / 1000L);
        newRow.setString("Time", hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year());
      } else if (autoSaveOpt.isSelected()==true) {
        TableRow newRow = Meas_summarySens.addRow();
        newRow.setFloat("Irradiance(mW/cm2)", ir);
        newRow.setFloat("Time(Unix)", (float)System.currentTimeMillis() / 1000L);
        newRow.setString("Time", hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year());
      }
      irr_plot.addPoint((millis()/1000-time_start)/60, ir);
      alldata = alldata + "IR[" + ir +"]TI["+ hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year() +"]"+eol;
      savedata = "IR[" + ir +"]TI["+ hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year() +"]";
      String[] list = split(savedata, ' ');
      if (autoSaveOpt.isSelected()==true) {
        autoSaveSensors();
      }
    } else if (selector.equals("RE")==true) {  //Ready
      updateStatusBar("Ready to measure");
      ready=true;
      btnConnect.setLocalColorScheme(GCScheme.GREEN_SCHEME);
      btnStMeas.setLocalColorScheme(GCScheme.GREEN_SCHEME);
    }
    // a message is sent to be written
    else if (selector.equals("ME")==true) {  
      Serial_text_M.appendText(data.substring(0, data.length()-2));
      println(data.substring(0, data.length()-2));
      if (data.equals("Curve sweep complete.")==true) Serial_text_M.appendText(Integer.toString(count_data));  //write to know which iv to save
      if (data.substring(0, data.length()-2).equals("Settings saved to SD")==true)  handleMessageDialog("Settings saved to SD"); 
      ;
    } else if (selector.equals("US")==true) {  //update status from arduino
      data = data.substring(0, data.length()-2); // remove extra character from arduino
      if (data.equals("Ready to measure")==true) { 
        ready=true; 
        measuring=false;
      }  //status is ready to measure
      updateStatusBar(data);
    } else if (selector.equals("IP")==true) {  //single point IV
      String[] dataStrings = split(trim(data), ',');
      float[] dataPoint = float(dataStrings);

      float[] pV = {
        dataPoint[0]
      };
      float[] pI = {
        -dataPoint[1]
      };
      //multiple lines in plot
      GPointsArray pointsIV = new GPointsArray(pV, pI);
      GPoint pointIV = new GPoint(dataPoint[0], -dataPoint[1]);

      // Start iv is true only before the first iv point is inserted
      if (start_iv==true) {  
        start_iv = false;
        // limit max iv curve to display, this control can be done before the first iv point is inserted
        if (counter_iv>max_iv_curve_graph-1) {
          IV_plot.removeLayer(Integer.toString(counter_iv-max_iv_curve_graph+1));
          l1++;
        }
        // first iv curve is number 1

        counter_iv++; // counter_iv is reset only when the software is closed or when delete is pressed(it is used for plotting in different layers) or if the mode reset is on

        // add layer for new iv curve
        plot_iv.addLayer(Integer.toString(counter_iv), pointsIV);
      } else {
        // add point to existing layer
        plot_iv.addPoint(pointIV, Integer.toString(counter_iv));
      }

      // insert data to IV_data
      TableRow newRow = IV_data.addRow();
      newRow.setFloat("Voltage(V)", dataPoint[0]);
      newRow.setFloat("Current(mA)", -dataPoint[1]);
      newRow.setString("id", Integer.toString(counter_iv));
    } else {
      myPort.clear();
    }
  }
}



public float[] string2float(String iv) {
  String[] current_pts_str = splitTokens(iv, ",");
  float[] current_pts = new float[current_pts_str.length];
  for (int i=0; i<current_pts_str.length; i=i+1) {
    current_pts[i] = Float.parseFloat(current_pts_str[i]);
  }
  return current_pts;
}
