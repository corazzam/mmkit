import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import grafica.*; 
import g4p_controls.*; 
import processing.serial.*; 
import java.io.BufferedWriter; 
import java.io.FileWriter; 
import java.awt.Rectangle; 
import java.util.*; 
import java.math.BigDecimal; 
import java.math.BigInteger; 
import java.text.NumberFormat; 
import java.text.DecimalFormat; 
import java.awt.Color; 
import java.security.SecureRandom; 
import javax.swing.ImageIcon; 
import java.util.regex.Matcher; 
import java.util.regex.Pattern; 
import java.util.Properties; 
import javax.mail.Message; 
import javax.mail.MessagingException; 
import javax.mail.PasswordAuthentication; 
import javax.mail.Session; 
import javax.mail.Transport; 
import javax.mail.internet.InternetAddress; 
import javax.mail.internet.MimeMessage; 
import javax.mail.BodyPart; 
import javax.mail.internet.MimeBodyPart; 
import javax.mail.Multipart; 
import javax.mail.internet.MimeMultipart; 
import javax.activation.DataSource; 
import javax.activation.FileDataSource; 
import javax.activation.DataHandler; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class GUI_MMkit_BitBucket extends PApplet {

// v. 33 
// added debug mode
// changed if into else if for "IP" data selector in Serial_processing tab

boolean debug=false;







 










//Email
















// Settings default size of windows
int width_window = 1000;
int height_window = 810;

//  serial port:
Serial myPort;  
GDropList cboSerial;
String portName;

boolean eth = false;
boolean sql = false;

// Graphs creation
GPlot IV_plot = new GPlot(this);
GPlot env = new GPlot(this);
GPlot PCE_plot = new GPlot(this);
GPlot irr_plot = new GPlot(this);
GPlot temp_plot = new GPlot(this);
GPlot hum_plot = new GPlot(this);

float[] xLimAutoscale;
float[] yLimAutoscale;
int x_pos_cnt=20;
int y_pos_cnt=50;
int y_width_btn=30;
int x_width_btn=80;
int taskbars = 70; // max size taskabr in pixel is 40 plus 20 pixel titlebar plus 10 of tollerance
int logo_x=20;
int logo_y=40;

PImage img, bck;
//final static String ICON  = "MMkit_icon.png";
//final static String TITLE = "MMkit";
boolean compact_mode=false;
int heigh_reduction;

public void createSerialListBox() {
  String[] serialPortList = Serial.list(); // find all serial ports

  if (serialPortList!=null && serialPortList.length>0) {
    //System.out.println(serialPortList.length);
  } else {
    //System.out.println("Array is not initialized or empty");
    serialPortList = new String[] { 
      "No COMs"
    };
  }

  cboSerial = new GDropList(this, x_pos_cnt, y_pos_cnt-45, x_pos_cnt+50, y_pos_cnt+120, 5); // create serial list box
  cboSerial.setItems(serialPortList, 0);
  portName = cboSerial.getSelectedText(); // set global portName
}

boolean debug_meas=false;
int debug_i=0;

public void setup() {
  frameRate(20); // limit framerate to limit computer comsuption

  // Make sure the program fit different screen resolution (width<1000)
  float red_factor=2;
  if (displayHeight < 660+taskbars) {  // note that x-resolution must be at least 1000px
    red_factor = 1;  // height window (hw)=570
    compact_mode =true;
  } else if (displayHeight < height_window+taskbars) {
    red_factor=2;   // hw = 660
    compact_mode =true;
  } else if (displayHeight >= height_window+taskbars)
    compact_mode=false;
  else {
    red_factor=2;   // hw = 660
    compact_mode =true;
  }
  int displayHeightMax = 900;

  if (compact_mode==true) {
    heigh_reduction=PApplet.parseInt(PApplet.parseFloat((displayHeightMax-height_window))/red_factor+20);
    compact_mode=true;
    compact_mode_on();
    height_window=height_window-heigh_reduction*2-20;
    println("heigh_reduction: "+heigh_reduction);
    println("display_height: "+displayHeight);
    println("heigh_window: "+height_window);
  }

  // create window
  size(width_window, height_window);
  //frame.setResizable(true);
  //size(width_window, height_window,OPENGL);
  rects = new ArrayList<Rectangle> ();
  createGUI();

  // create data containers
  recreateTableSum(Meas_summary);
  recreateTableIV(IV_data);
  recreateTableSumS(Meas_summarySens);
  
  max_iv_curve_graph = Integer.parseInt(max_iv.getText().trim());
  savedataPartial = new String[max_iv_curve_graph];
  savedataPartial_ind = new Integer[max_iv_curve_graph]; 
  time_start=millis()/1000; //time when the software started
  updateStatusBar("Waiting for connection");
  //img = loadImage("mmkit-logo.png");
  //bck = loadImage("sun6.png");
  //changeAppIcon( loadImage("mmkit-icon.png") );
  createSerialListBox();  // for automatic update of port, you can move it to draw. But the program will use much more CPU.

  if (debug==true) {
    ready=true;
    ser_connected=true;
  }
}



public void draw() {
  background(255, 255, 255);
  //image(img, logo_x, logo_y, 270, 70);

  update_plots();

  for (Rectangle r : rects)
    showFrame(r);
  if (ser_connected==true && debug==false) {
    while (myPort.available () > 0) {
      if (calibrPh==false && calibrTi==false) {
        String inBuffer = myPort.readStringUntil(10);   //10 is the ASCII code for new line
        if (inBuffer != null) {
          println(inBuffer);
          update_data(inBuffer, IV_plot, temp_plot);
        }
      }
      if (calibrPh==true) {
        String inBuffer = myPort.readStringUntil(10);   //10 is the ASCII code for new line
        if (inBuffer != null) {
          Serial_text.appendText(inBuffer);
        }
      }
      if (calibrTi==true) {
        //String inBuffer = myPort.readStringUntil(10);   //10 is the ASCII code for new line
        String val = myPort.readStringUntil(10);
        Serial_text.appendText((val));
        if (val!=null && val.length()>1) {
          val = val.substring (0, 1);
          if (val.equals("T")==true) {
            long t = getTimeNow();
            sendTimeMessage(TIME_HEADER, t); 
            updateStatusBar("Time calibrated!");
          }
        }
      }
    }
  }
}


// if mouse is clicked in the logo go to the MMkit homepage
public void mouseClicked() {
  if (mouseX>logo_x+100&& mouseX<logo_x+200  &&mouseY>logo_y&& mouseY<logo_y+70 )
    link("http://plasticphotovoltaics.org/mmkit.html");
}


public void changeAppIcon(PImage img) {
  final PGraphics pg = createGraphics(48, 48, JAVA2D);

  pg.beginDraw();
  pg.image(img, 0, 0, 45, 45);
  pg.endDraw();

  frame.setIconImage(pg.image);
}

// resize window in compact mode
public void compact_mode_on()
{
  panelDim_IV = new float[] {
    380, (380-heigh_reduction*2-20)
    };
    
    /* Setting for summary*/
  firstPlotPos = new float[] {
    25, 25
  };
  panelDim = new float[] {
    200, 150-heigh_reduction
  };
  margins = new float[] {
    0, 30, 300, 0
  };
}
public double[] estrapolate_pce(float[] volt, float[] cur, float irr) {
  double M_power=0;
  double pce=0;

  // find Pmax
  for (int i=0; i< volt.length; i++) {
    double tmp_max;
    tmp_max = volt[i]/1000*cur[i];
    if (tmp_max>M_power) 
      M_power = tmp_max;
  }

  println("PM: "+M_power);

  double area_cell = Double.parseDouble(area.getText());
  pce = M_power/area_cell/irr*100;

  double[] volt_double = new double[volt.length];
  double[] cur_double = new double[volt.length];

  // keep voc positive
  double [] volt_2 =  new double[2];
  double [] cur_2 = new double[2];
  double[][] voc = new double[volt.length][1];  //max voc for every point
  double[] zero = {
    0
  };

  for (int i = 0; i < volt.length; i++)
  {
    volt_double[i] = (double) volt[i]/1000;
    cur_double[i] = (double) -cur[i];

    // calculate interception of isc=0 for all data points
    if (i>=1) {
      volt_2[1] = volt_double[i];
      volt_2[0] = volt_double[i-1];
      cur_2[1] = cur_double[i];
      cur_2[0] = cur_double[i-1];
      voc[i-1] = interpLinear(cur_2, volt_2, zero);
    }
  }

  double[] isc = interpLinear(volt_double, cur_double, zero);
  double voc_pos=voc[0][0]; // start from the initial value of interception
  int i =1;
  // take the voc as the first interception of isc=0 with a positive and monotic growing value
  while (! (voc_pos>=0) && i<volt.length) {
    //println("voc_tmp: "+voc[i][0]);
    voc_pos = voc[i][0];
    i++;
  }
  // if it was not possible to find a positive value
  if (!(voc_pos>=0) && i==volt.length) {
    voc_pos=Double.NaN;
  }


  double FF = -M_power/voc_pos/isc[0]*100;
  //println("voc: "+voc_pos+" FF: "+FF);

  double[] para = {
    pce, isc[0], voc_pos, FF, M_power
  };
  return para;
}



/**
 * <p>Static methods for doing useful math</p><hr>
 *
 * @author  : $Author: brian $
 * @version : $Revision: 1.1 $
 *
 * <hr><p><font size="-1" color="#336699"><a href="http://www.mbari.org">
 * The Monterey Bay Aquarium Research Institute (MBARI)</a> provides this
 * documentation and code &quot;as is&quot;, with no warranty, express or
 * implied, of its quality or consistency. It is provided without support and
 * without obligation on the part of MBARI to assist in its use, correction,
 * modification, or enhancement. This information should not be published or
 * distributed to third parties without specific written permission from
 * MBARI.</font></p><br>
 *
 * <font size="-1" color="#336699">Copyright 2002 MBARI.<br>
 * MBARI Proprietary Information. All rights reserved.</font><br><hr><br>
 *
 */

public   double[] interpLinear(double[] x, double[] y, double[] xi) throws IllegalArgumentException {

  if (x.length != y.length) {
    throw new IllegalArgumentException("X and Y must be the same length");
  }
  if (x.length == 1) {
    throw new IllegalArgumentException("X must contain more than one value");
  }
  double[] dx = new double[x.length - 1];
  double[] dy = new double[x.length - 1];
  double[] slope = new double[x.length - 1];
  double[] intercept = new double[x.length - 1];

  // Calculate the line equation (i.e. slope and intercept) between each point
  for (int i = 0; i < x.length - 1; i++) {
    dx[i] = x[i + 1] - x[i];
    if (dx[i] == 0) {
      println("X must be montotonic. A duplicate " + "x-value was found");
    }
    if (dx[i] < 0) {
      println("X must be sorted");
    }
    dy[i] = y[i + 1] - y[i];
    slope[i] = dy[i] / dx[i];
    intercept[i] = y[i] - x[i] * slope[i];
  }

  // Perform the interpolation here
  double[] yi = new double[xi.length];
  for (int i = 0; i < xi.length; i++) {
    if ((xi[i] > x[x.length - 1]) || (xi[i] < x[0])) {
      yi[i] = Double.NaN;
    } else {
      int loc = Arrays.binarySearch(x, xi[i]);
      if (loc < -1) {
        loc = -loc - 2;
        yi[i] = slope[loc] * xi[i] + intercept[loc];
      } else {
        yi[i] = y[loc];
      }
    }
  }

  return yi;
}

public static final BigDecimal[] interpLinear(BigDecimal[] x, BigDecimal[] y, BigDecimal[] xi) {
  if (x.length != y.length) {
    throw new IllegalArgumentException("X and Y must be the same length");
  }
  if (x.length == 1) {
    throw new IllegalArgumentException("X must contain more than one value");
  }
  BigDecimal[] dx = new BigDecimal[x.length - 1];
  BigDecimal[] dy = new BigDecimal[x.length - 1];
  BigDecimal[] slope = new BigDecimal[x.length - 1];
  BigDecimal[] intercept = new BigDecimal[x.length - 1];

  // Calculate the line equation (i.e. slope and intercept) between each point
  BigInteger zero = new BigInteger("0");
  BigDecimal minusOne = new BigDecimal(-1);

  for (int i = 0; i < x.length - 1; i++) {
    //dx[i] = x[i + 1] - x[i];
    dx[i] = x[i + 1].subtract(x[i]);
    if (dx[i].equals(new BigDecimal(zero, dx[i].scale()))) {
      throw new IllegalArgumentException("X must be montotonic. A duplicate " + "x-value was found");
    }
    if (dx[i].signum() < 0) {
      throw new IllegalArgumentException("X must be sorted");
    }
    //dy[i] = y[i + 1] - y[i];
    dy[i] = y[i + 1].subtract(y[i]);
    //slope[i] = dy[i] / dx[i];
    slope[i] = dy[i].divide(dx[i]);
    //intercept[i] = y[i] - x[i] * slope[i];
    intercept[i] = x[i].multiply(slope[i]).subtract(y[i]).multiply(minusOne);
    //intercept[i] = y[i].subtract(x[i]).multiply(slope[i]);
  }

  // Perform the interpolation here
  BigDecimal[] yi = new BigDecimal[xi.length];
  for (int i = 0; i < xi.length; i++) {
    //if ((xi[i] > x[x.length - 1]) || (xi[i] < x[0])) {
    if (xi[i].compareTo(x[x.length - 1]) > 0 || xi[i].compareTo(x[0]) < 0) {
      yi[i] = null; // same as NaN
    } else {
      int loc = Arrays.binarySearch(x, xi[i]);
      if (loc < -1) {
        loc = -loc - 2;
        //yi[i] = slope[loc] * xi[i] + intercept[loc];
        yi[i] = slope[loc].multiply(xi[i]).add(intercept[loc]);
      } else {
        yi[i] = y[loc];
      }
    }
  }

  return yi;
}

public   double[] interpLinear(long[] x, double[] y, long[] xi) throws IllegalArgumentException {

  double[] xd = new double[x.length];
  for (int i = 0; i < x.length; i++) {
    xd[i] = (double) x[i];
  }

  double[] xid = new double[xi.length];
  for (int i = 0; i < xi.length; i++) {
    xid[i] = (double) xi[i];
  }

  return interpLinear(xd, y, xid);
}



public void addTable(Table table, java.lang.String filename) {
  Table added_table;
  boolean sv_chk;
  File f;
  f = new File(filename);

  if (f.exists() && !f.isDirectory()) { 

    //tmp = loadTable(filename,"header");
    added_table = loadTable(filename);
    for (TableRow row : table.rows ()) {
      added_table.addRow(row);
    }
    sv_chk = saveTable(added_table, filename);
  } else {
    sv_chk = saveTable(table, filename); 
    if (sv_chk == false) {
      handleMessageDialog("Couldn't save! Probably file is open");
    }
  }
}

// Recreate datatable to be saved in csv file
public void recreateTableIV(Table table) {
  table.addColumn("id");
  table.addColumn("Time(Unix)");
  table.addColumn("Time");
  table.addColumn("Voltage(V)");
  table.addColumn("Current(mA)");
  table.addColumn("PCE(%)");
  table.addColumn("Isc(mA)");
  table.addColumn("Voc(V)");
  table.addColumn("FF(%)");
  table.addColumn("Humidity(R.H.)");
  table.addColumn("Temperature(C)");
  table.addColumn("Irradiance(mW/cm2)");
  table.addColumn("Pmax(mW)");
  table.addColumn("Area(cm2)");
  table.addColumn("Note");
}

public void recreateTableSum(Table table) {
  table.addColumn("id");
  table.addColumn("Time(Unix)");
  table.addColumn("Time");
  table.addColumn("PCE(%)");
  table.addColumn("Isc(mA)");
  table.addColumn("Voc(V)");
  table.addColumn("FF(%)");
  table.addColumn("Humidity(R.H.)");
  table.addColumn("Temperature(C)");
  table.addColumn("Irradiance(mW/cm2)");
  table.addColumn("Pmax(mW)");
  table.addColumn("Area(cm2)");
  table.addColumn("Note");
}

public void recreateTableSumS(Table table) {
  table.addColumn("Time(Unix)");
  table.addColumn("Time");
  table.addColumn("Humidity(R.H.)");
  table.addColumn("Temperature(C)");
  table.addColumn("Irradiance(mW/cm2)");
  table.addColumn("Note");
}

public void cleanData() {
  IV_data= new Table(); //Empty data
  Meas_summary= new Table(); //Empty data
  savedata="";
  recreateTableSum(Meas_summary);
  recreateTableIV(IV_data);

  count_data =0;
  counter_iv =0;
  //Erase plot too
  PCE_plot = new GPlot(this);
  irr_plot = new GPlot(this);
  temp_plot = new GPlot(this);
  hum_plot = new GPlot(this);
  IV_plot = new GPlot(this);
  l2=0;
  l1=0;
}

public void autoSave() {
  file= folder.getText()+fs+filename.getText()+"_"+note.getText().trim()+".txt";  // for database exportation
  file_sum= folder.getText()+"\\"+filename.getText()+"_summary.csv";
  file_iv= folder.getText()+"\\"+filename.getText()+"_iv_"+counter_iv +".csv";
  //save text file for DB
  appendTextToFile(file, savedata);
  try {
    addTable(Meas_summary, file_sum);
    addTable(IV_data, file_iv);
    // when the data is added, then we can clean the data array
    Meas_summary= new Table();
    IV_data= new Table();
    recreateTableIV(IV_data);
    recreateTableSum(Meas_summary);
    println("Autosave IV done");
  }
  catch(Exception ioe)
  {
    System.err.println("IOException: " + ioe.getMessage());
    handleMessageDialog("Couldn't save! Probably csv file is open");
  }
}

public void autoSaveSensors() {
  file= folder.getText()+fs+filename.getText()+"_"+note.getText().trim()+".txt";  // for database exportation
  file_sum= folder.getText()+"\\"+filename.getText()+"_summarySensors.csv";

  //save text file for DB
  appendTextToFile(file, savedata);
  try {
    //don't care about these columns for sensors
    addTable(Meas_summarySens, file_sum);
    // when the data is added, then we can clean the data array
    Meas_summarySens= new Table();
    recreateTableSumS(Meas_summarySens);
    println("Autosave sensors done");
  }
  catch(Exception ioe)
  {
    System.err.println("IOException: " + ioe.getMessage());
    handleMessageDialog("Couldn't save! Probably csv file is open");
  }
}


public int[] generate_colors(int size) {
  //change colour last iv curve
    Color color_t; //random color, but can be bright or dull
    
  //to get n=size different colors
  float hue_step=(float)1/size;
  int[] colors_array = new int[size];
  for (int i=0; i< size; i++) {
    float hue = (float) hue_step*(i);
    float saturation = 0.9f;//1.0 for brilliant, 0.0 for dull
    float luminance = 1.0f; //1.0 for brighter, 0.0 for black
    color_t = Color.getHSBColor(hue, saturation, luminance);
    colors_array[i] = color_t.getRGB();
  }
  return colors_array;
}


public static boolean isNumeric(String str)  
{  
  try  
  {  
    double d = Double.parseDouble(str);
  }  
  catch(NumberFormatException nfe)  
  {  
    return false;
  }  
  return true;
}
// set up your own email account to allow sending email with it from MMKit GUI

final String username = "whatever@gmail.com";
final String password = "something";
boolean email_sent = false;


public void send_email(String pce_meas, String email_to) {

  Serial_text_M.appendText("sending email");

  Properties props = new Properties();
  props.put("mail.smtp.auth", "true");
  props.put("mail.smtp.starttls.enable", "true");
  props.put("mail.smtp.host", "smtp.gmail.com");
  props.put("mail.smtp.port", "587");


  Session session = Session.getInstance(props, 
  new javax.mail.Authenticator() {
    protected PasswordAuthentication getPasswordAuthentication() {
      return new PasswordAuthentication(username, password);
    }
  }
  );

  try {
    // Create a default MimeMessage object.
    Message message = new MimeMessage(session);

    // Set From: header field of the header.
    message.setFrom(new InternetAddress(username));

    // Set To: header field of the header.
    message.setRecipients(Message.RecipientType.TO, 
    InternetAddress.parse(email.getText().trim()));
    // Set Subject: header field
    message.setSubject("MMkit stability report");

    // Create the message part
    BodyPart messageBodyPart = new MimeBodyPart();

    // Now set the actual message
    messageBodyPart.setText("Hello,"
      + "\n\n Your device under test has reached a pce of "+pce_meas+".");

    // Create a multipar message
    Multipart multipart = new MimeMultipart();

    // Set text message part
    multipart.addBodyPart(messageBodyPart);

    // attachment iv summary 
    messageBodyPart = new MimeBodyPart();
    DataSource source = new FileDataSource(file_sum);
    messageBodyPart.setDataHandler(new DataHandler(source));
    messageBodyPart.setFileName("summary_iv.csv");
    multipart.addBodyPart(messageBodyPart);        

    // attachment for database
    messageBodyPart = new MimeBodyPart();
    source = new FileDataSource(file);
    messageBodyPart.setDataHandler(new DataHandler(source));
    messageBodyPart.setFileName("database_file.txt");
    multipart.addBodyPart(messageBodyPart);

    // attachment screenshot 
    messageBodyPart = new MimeBodyPart();
    String file_screenshot = folder.getText()+fs+"screenshot"+hour()+"_"+minute()+"_"+second()+"_"+day()+"_"+month()+"_"+year() +".png";
    saveFrame(file_screenshot);
    source = new FileDataSource(file_screenshot);
    messageBodyPart.setDataHandler(new DataHandler(source));
    messageBodyPart.setFileName("screenshot.png");
    multipart.addBodyPart(messageBodyPart);

    // Send the complete message parts
    message.setContent(multipart);

    // Send message
    Transport.send(message);

    System.out.println("Sent message successfully....");

    Serial_text_M.appendText("Email sent");
  } 
  catch (MessagingException e) {
    throw new RuntimeException(e);
  }

  email_sent = true;
}

// Group 1 controls
GToggleGroup togG1Options; 
GOption grp1_a, grp1_b, grp1_c; 
GDropList grp1_d; 
GKnob grp1_e; 
GButton btnConnect, btnLdSett, btnSvSett, btnStMeas, btnSvData, btnStopMeas, btnChFolder, btnCalPhoto, btnCalTime, btnCalEnd, btnSvSet, btnDelData;
GCustomSlider grp1_g; 
GTextField startV, stopV, numPoints, numMeasurements, filename, folder, sett_irr, intervalIV, intervalEnv, intervalIrr, area, iv_save, note, max_iv, email, PCE_limit, cal_photo; 
GLabel lblTitle, lblStatus, startV_lbl, stopV_lbl, numPoints_lbl, numMeasurements_lbl, filename_lbl, folder_lbl, sett_irr_lbl, intervalIV_lbl, intervalEnv_lbl, intervalIrr_lbl, area_lbl, iv_save_lbl, note_lbl, max_iv_lbl, static_graph_lbl; 
GPanel panelIV, panelOpt, panelTime, panelCal, panelExSet; 
GCheckbox Debug, DHT22, DS18, Irr, autoSaveOpt, database_MySQL, con_ethernet, refresh_graph; 
GTextArea Serial_text, Serial_text_M; 
GWindow Calibr_window;
int pYStatus = 780;
int pWidth = width_window;
boolean ser_connected = false;
int x_IV_values =250;
int y_IV_values = 30;
int widthx_IV_values =50;
int widthy_IV_values =20;
int distance_IV =40;
int distance_lbl_IV=280; // negative
int widthx_lbl_IV=250;
int widthy_lbl_IV=widthy_IV_values;
String path_save;
// to write to the same file
PrintWriter output;
boolean calibrPh=false;
boolean calibrTi=false;
public static final char TIME_HEADER = 'T';
public static final char TIME_REQUEST = 7;  // ASCII bell character 
boolean DEBUG = false;
int count_data=0;
boolean ready=false;
String so = "mac";
int[] colors_array;
boolean measuring=false;


public void createGUI() {
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(GCScheme.CYAN_SCHEME);
  if (frame != null)
    frame.setTitle("MMkit User Interface");
  createGroup1Controls();
  createTextContainers();
  createOptions();
  createTimeSettings();
  createCalibration();  
  extraSettings();
  extraInternet();

  rects.add(new Rectangle(3, 40, 988, 260));
  rects.add(new Rectangle(3, 300, 988, 478));

  if (compact_mode==false) {
    lblStatus = new GLabel(this, 0, pYStatus, pWidth, 30, "");
  } else {
    lblStatus = new GLabel(this, 0, pYStatus-heigh_reduction*2-20, pWidth, 30, "");
  }

  lblStatus.setOpaque(true);
  // initialize title label
  lblTitle = new GLabel(this, 0, 0, pWidth, 35, "MultiMeasurement kit");
  lblTitle.setOpaque(true);
  GLabel lblMico = new GLabel(this, 450, 0, pWidth, 35, "mico@dtu.dk");
  lblTitle.setOpaque(true);
}


public void createGroup1Controls() {
  Debug = new GCheckbox(this, x_pos_cnt-170, y_pos_cnt, x_width_btn, y_width_btn);
  Debug.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  Debug.setText("Debug");
  Debug.addEventHandler(this, "debug_clicked");
  Debug.setOpaque(false);
  filename = new GTextField(this, x_pos_cnt+390, y_pos_cnt+32, x_width_btn-10, y_width_btn-15, G4P.SCROLLBARS_NONE);
  filename.setOpaque(true);
  //filename.setText(String.valueOf(year())+String.valueOf(month())+String.valueOf(day()));
  filename.setPromptText("Filename");
  //filename_lbl = new GLabel(this, x_pos_cnt+465, y_pos_cnt+5, x_width_btn, y_width_btn-10);
  //filename_lbl.setText("Filename:");
  //filename_lbl.setTextAlign(GAlign.RIGHT, null);
  //filename_lbl.setOpaque(false);
  btnConnect = new GButton(this, x_pos_cnt+75, y_pos_cnt-46, x_width_btn, y_width_btn);
  btnConnect.setText("Connect");
  btnConnect.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  btnStMeas = new GButton(this, x_pos_cnt+700, y_pos_cnt, x_width_btn, y_width_btn);
  btnStMeas.setText("Start");
  btnStMeas.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  btnStopMeas = new GButton(this, x_pos_cnt+800, y_pos_cnt, x_width_btn, y_width_btn);
  btnStopMeas.setText("Stop");
  btnStopMeas.setLocalColorScheme(GCScheme.RED_SCHEME);
  folder = new GTextField(this, x_pos_cnt+300, y_pos_cnt+32, x_width_btn, y_width_btn-15, G4P.SCROLLBARS_NONE);
  folder.setOpaque(true);
  //folder.setPromptText("Folder path");
  folder_lbl = new GLabel(this, x_pos_cnt+20, y_pos_cnt+32, x_width_btn, y_width_btn-15);
  //folder_lbl.setText("Folder:");
  folder.setPromptText("Folder");
  folder_lbl.setTextAlign(GAlign.RIGHT, null);
  folder_lbl.setOpaque(false);
  btnChFolder = new GButton(this, x_pos_cnt+300, y_pos_cnt, x_width_btn, y_width_btn);
  btnChFolder.setText("Choose folder");
  btnDelData = new GButton(this, x_pos_cnt+500, y_pos_cnt, x_width_btn, y_width_btn);
  btnDelData.setText("Clean plots");
  iv_save = new GTextField(this, x_pos_cnt+460, y_pos_cnt+32, x_width_btn-50, y_width_btn-15, G4P.SCROLLBARS_NONE);
  iv_save.setOpaque(true);
  iv_save.setPromptText("n. IV");
  btnSvData = new GButton(this, x_pos_cnt+400, y_pos_cnt, x_width_btn, y_width_btn);
  btnSvData.setText("Save data");
}

// event handler for buttons
public void handleButtonEvents(GButton button, GEvent event) {
  // handle Connect button
  if (btnConnect == button && event == GEvent.CLICKED) {
    portName = cboSerial.getSelectedText();
    updateStatusBar("Connecting..."); // display connecting status 
    myPort = new Serial(this, portName, 115200);
    ser_connected= true;
    try {
      Thread.sleep(1000);                 //1000 milliseconds is one second.
    } 
    catch(InterruptedException ex) {
      Thread.currentThread().interrupt();
    }
    myPort.write("C");
  } else if (btnStMeas == button && event == GEvent.CLICKED ) {
    File f = new File(folder.getText()+fs+filename.getText()+".txt");
    //if (ser_connected == true && (lblStatus.getText().equals("Status: Ready to measure")==true ||  lblStatus.getText().equals("Status: Saved!")==true||  lblStatus.getText().equals("Status: Data erased")==true||  lblStatus.getText().equals("Stop pressed! Finishing current measurement")==true||  lblStatus.getText().equals("Status: Settings saved to SD!")==true||  lblStatus.getText().equals("Status: Settings updated (SD is not connected)")==true)) {
    if ((ser_connected == true && ready == true) || debug==true) {
      if (Integer.parseInt(numPoints.getText().trim())>50 ||Integer.parseInt(numPoints.getText().trim())<2) {
        handleMessageDialog("N. points can vary from 2 to 50");
      } else if (autoSaveOpt.isSelected() && f.exists() && !f.isDirectory()) {
        handleMessageDialog("Autosave mode: file where to save must be empty");
      }
      //start==stop voltage don't run
      else if (Float.parseFloat(startV.getText().trim())== Float.parseFloat(stopV.getText().trim())) {
        handleMessageDialog("Start and stop voltage cannot be the same");
      } else if (abs(Float.parseFloat(startV.getText().trim()))>12 || abs(Float.parseFloat(stopV.getText().trim()))>12 ) {
        handleMessageDialog("Exceeding voltage limit");
      } else if (Irr.isSelected()==true && cal_photo.getText().equals("")==true  ) {
        handleMessageDialog("You must insert the calibration value of the photodiode");
      } else {
        if (((Float.parseFloat(numMeasurements.getText().trim())>1 || Float.parseFloat(numMeasurements.getText().trim())==0)) && (autoSaveOpt.isSelected()==false )) {
          handleMessageDialog("You are performing a time-study without autosave on");
        }
        // clean graphs
        if (refresh_graph.isSelected()==true) {
          PCE_plot = new GPlot(this);
          irr_plot = new GPlot(this);
          temp_plot = new GPlot(this);
          hum_plot = new GPlot(this);
        }

        // autosave is on then delete clean data before measuring 
        // if autosave is selected and then stop and start is pressed
        if (autoSaveOpt.isSelected() ) {
          file= folder.getText()+fs+filename.getText().trim()+note.getText().trim()+".txt";  // for database exportation
          file_sum= folder.getText()+fs+filename.getText().trim()+"_summary.csv";  // summary of iv curve measurements
          file_iv= folder.getText()+fs+filename.getText().trim()+"_iv.csv";  // separate iv curves, used only in autosave mode
          file_sum_sensors= folder.getText()+fs+filename.getText().trim()+"_summarySensors.csv";  // summary of sensors measurements, used only in autosave mode
          println(file);
          cleanData();
          output = createWriter(file);
        }

        //Options measurements
        email_sent = false;
        counter=0;

        if (debug==true) {
          debug_meas=true;
        } else {
          if (Debug.isSelected()) myPort.write("D 1");
          if (!Debug.isSelected()) myPort.write("D 0");
          if (DHT22.isSelected()) myPort.write("H 1");
          if (!DHT22.isSelected()) myPort.write("H 0");
          if (DS18.isSelected()) myPort.write("T 1");
          if (!DS18.isSelected()) myPort.write("T 0");
          if (Irr.isSelected()) { 
            myPort.write("I 1");
            myPort.write("V 5 "+Integer.parseInt(cal_photo.getText().trim()));
          }
          if (!Irr.isSelected()) myPort.write("I 0");
          int DELAY=10;

          // Set up interval if number of measurement > 1
          if ( Integer.parseInt(numMeasurements.getText().trim()) > 1 || Integer.parseInt(numMeasurements.getText().trim())== 0) {
            myPort.write("M 1 "+intervalIV.getText());
            if (DHT22.isSelected()||DS18.isSelected() )
              myPort.write("M 2 "+intervalEnv.getText());
            if (Irr.isSelected())
              myPort.write("M 3 "+intervalIrr.getText());
          }
          updateStatusBar("Measuring..."); // display connecting status 
          String command = "S " + startV.getText().trim()+ " "+ stopV.getText().trim()+ " "+(Integer.parseInt(numPoints.getText().trim())-1)+ " "+Integer.parseInt(numMeasurements.getText().trim());
          myPort.write(command);

          updateStatusBar("Command: " + command);
        }
        updateStatusBar("Measuring...");
        if (refresh_graph.isSelected()==true)  time_start=millis()/1000;

        ready=false; // MMkit is measuring
        measuring=true;
      }
    } else if (ser_connected == false) {
      handleMessageDialog("You must connect first!");
    } else  if (ready ==false) {
      handleMessageDialog("MMkit is not ready to measure");
    } else {
      handleMessageDialog("Error on starting measurement. Try restart");
    }
  } else if (btnStopMeas == button && event == GEvent.CLICKED) {
    if (ser_connected == true && measuring==true) {
      updateStatusBar("Stop! Finishing current measurement"); // display connecting status 
      ready=true; //ready to measure again - this should come
      measuring =false; //not sure
      String command = "O";
      myPort.write(command);
    } else if (ser_connected == false) {
      handleMessageDialog("You must connect first!");
    } else if (measuring==false) {
      handleMessageDialog("You are not measuring!");
    }
  } else if (btnCalPhoto == button && event == GEvent.CLICKED) {
    if (ser_connected == true && ready==true && measuring==false) {
      ready=false;
      updateStatusBar("Calibrating photodiode"); // display connecting status 
      calibrPh=true; //Special mode - no update of graphs. Serial stream goes to new window
      Calibr_window = new GWindow(this, "Calibration", 0, 0, 240, 300, false, JAVA2D);
      Calibr_window.setActionOnClose(GWindow.KEEP_OPEN);   //  GWindow.KEEP_OPEN, GWindow.CLOSE_WINDOW  depending on the action you want to perform when attempting to close
      Calibr_window.addDrawHandler(this, "win_draw1");
      int x = 50;
      int y =200;
      btnCalEnd = new GButton(Calibr_window.papplet, x, y, x_width_btn, y_width_btn);
      btnCalEnd.setText("End calibration");
      btnCalEnd.setLocalColorScheme(GCScheme.RED_SCHEME);
      String command = "A 1";
      Serial_text = new GTextArea(Calibr_window.papplet, 40, 20, 160, 140, G4P.SCROLLBARS_VERTICAL_ONLY);
      Serial_text.setOpaque(true);
      //Serial_text.addEventHandler(this, "Serial_text_change");
      myPort.write(command);
    } else if (ser_connected == false)
      updateStatusBar("You must connect first!");
    else if (ready == false)
      updateStatusBar("MMkit is not correctly connected");
    else if (measuring == true)
      updateStatusBar("You can't calibrate while measuring");
  } else if (btnCalTime == button && event == GEvent.CLICKED) {
    if (ser_connected == true && ready==true && measuring==false) {
      ready =false;
      if (Debug.isSelected()) myPort.write("D 1");
      if (!Debug.isSelected()) myPort.write("D 0");
      updateStatusBar("Calibrating time"); // display connecting status 
      calibrTi=true; //Special mode - no update of graphs. Serial stream goes to new window
      Calibr_window = new GWindow(this, "Calibration", 0, 0, 240, 300, false, JAVA2D);
      Calibr_window.setActionOnClose(GWindow.KEEP_OPEN);   //  GWindow.KEEP_OPEN, GWindow.CLOSE_WINDOW  depending on the action you want to perform when attempting to close
      Calibr_window.addDrawHandler(this, "win_draw1");
      int x = 50;
      int y =200;
      btnCalEnd = new GButton(Calibr_window.papplet, x, y, x_width_btn, y_width_btn);
      btnCalEnd.setText("End calibration");
      btnCalEnd.setLocalColorScheme(GCScheme.RED_SCHEME);
      //Calibr_window.addDrawHandler(this, "Calibr_draw");
      String command = "A 2";
      Serial_text = new GTextArea(Calibr_window.papplet, 40, 20, 160, 140, G4P.SCROLLBARS_VERTICAL_ONLY);
      Serial_text.setOpaque(true);
      myPort.write(command);
      print("command sent");
    } else if (ser_connected == false)
      updateStatusBar("You must connect first!");
    else if (ready == false)
      updateStatusBar("MMkit is not correctly connected");
    else if (measuring == true)
      updateStatusBar("You can't calibrate while measuring");
  } else if (btnCalEnd == button && event == GEvent.CLICKED) {
    ready=true;
    Calibr_window.forceClose();
    calibrTi=false;
    calibrPh=false;
    String command = "1";
    myPort.write(command);
    updateStatusBar("Ready to measure");
  } else if (btnDelData == button && event == GEvent.CLICKED) {
    updateStatusBar("Data erased"); // display connecting status 
    alldata=""; // empty data when saved!
    cleanData();
  } else if (btnSvSet == button && event == GEvent.CLICKED) {
    if (ser_connected == true || DEBUG==true) {

      if (Integer.parseInt(numPoints.getText().trim())>50 ||Integer.parseInt(numPoints.getText().trim())<2) {
        handleMessageDialog("N. points can vary from 2 to 50");
      } else if (abs(Float.parseFloat(startV.getText().trim()))>12 || abs(Float.parseFloat(stopV.getText().trim()))>12 ) {
        handleMessageDialog("Exceeding voltage limit");
      } else {
        String command = "E";
        updateStatusBar("Save settings");
        myPort.write(command);
        if (Debug.isSelected()) myPort.write("D 1");
        if (!Debug.isSelected()) myPort.write("D 0");
        if (DHT22.isSelected()) myPort.write("H 1");
        if (!DHT22.isSelected()) myPort.write("H 0");
        if (DS18.isSelected()) myPort.write("T 1");
        if (!DS18.isSelected()) myPort.write("T 0");
        if (Irr.isSelected()) myPort.write("I 1");
        if (!Irr.isSelected()) myPort.write("I 0");
        myPort.write("M 1 "+intervalIV.getText());
        myPort.write("M 2 "+intervalEnv.getText());
        myPort.write("M 3 "+intervalIrr.getText());
        myPort.write("V 1 "+startV.getText().trim());
        println("here ok");
        int DELAY=500;
        delay(DELAY);
        myPort.write("V 2 "+stopV.getText().trim());
        delay(DELAY);
        myPort.write("V 3 "+(Integer.parseInt(numPoints.getText().trim())-1));
        delay(DELAY);
        myPort.write("V 4 "+numMeasurements.getText().trim());
        delay(DELAY);
        myPort.write("V 5 "+Integer.parseInt(cal_photo.getText().trim()));
        println("Send signal to write to SD");
        myPort.write("E 1");  // write settings to sd card
      }
    } else 
      handleMessageDialog("You are not connected!");
  }
  //////////////// SAVE data /////////////////////////// 
  else if (btnSvData == button && event == GEvent.CLICKED) {


    if ( folder.getText() =="") {
      handleMessageDialog("Chose a folder first!");
    } else if (autoSaveOpt.isSelected()==true) {
      // when autosave is on there is no need to click save
      handleMessageDialog("Auto save is enabled!");
    } else if (savedata=="") handleMessageDialog("Nothing to save");
    else if (filename.getText()=="") handleMessageDialog("Specify a filename");
    else {
      // there is something to save
      //updateStatusBar("Saving..."); // display connecting status 
      file= folder.getText()+fs+filename.getText()+note.getText().trim()+".txt";  // for database exportation
      file_sum= folder.getText()+fs+filename.getText()+"_summary.csv";  // summary of iv curve measurements
      file_iv= folder.getText()+fs+filename.getText()+"_iv.csv";  // separate iv curves, used only in autosave mode
      file_sum_sensors= folder.getText()+fs+filename.getText()+"_summarySensors.csv";  // summary of sensors measurements, used only in autosave mode

      //no option for savings --> standard save: last iv curve is saved
      if (iv_save.getText()=="") {
        //save last iv curve data
        println("Save last iv curve");

        //extract information from data saved in program and export to toSave_sum and toSave_IV
        Table toSave_sum= new Table();
        Table toSave_IV =new Table();
        recreateTableIV(toSave_IV);
        recreateTableSum(toSave_sum);
        //find last curve position:
        // counter_iv is a counter that is reset only by pressing "delete data". However only a number of curve equals to 
        // "IV to display is stored"
        // savedataPartial_ind contains all the ID of the iv curves, acquired from counter_iv
        // find the index in savedataPartial_ind containing counter_iv
        int ind_dataToSave = Arrays.asList(savedataPartial_ind).indexOf(counter_iv);
        String in = String.valueOf(savedataPartial_ind[ind_dataToSave]);
        println("ID curve to save is: "+ in);
        println("IV saved is: "+ savedataPartial[ind_dataToSave]);
        try {
          FileWriter fw = new FileWriter(file, true); //the true will append the new data
          fw.write(savedataPartial[ind_dataToSave]+eol);//appends the string to the file
          fw.close();
        }
        catch(Exception ioe)
        {
          System.err.println("IOException: " + ioe.getMessage());
          handleMessageDialog("Couldn't save" + file);
        }


        // all the row with id=counter_iv (i.e. last curve are saved)
        for (TableRow row_to_save : Meas_summary.findRows ( Integer.toString (counter_iv), "id")) {
          toSave_sum.addRow(row_to_save);
        }
        for (TableRow row_to_save : IV_data.findRows ( Integer.toString (counter_iv), "id")) {
          toSave_IV.addRow(row_to_save);
        }

        try {
          addTable(toSave_sum, file_sum);
        }
        catch(Exception ioe)
        {
          System.err.println("IOException: " + ioe.getMessage());
          handleMessageDialog("Couldn't save" + file_sum);
        }
        try {
          addTable(toSave_IV, file_iv);
        }
        catch(Exception ioe)
        {
          System.err.println("IOException: " + ioe.getMessage());
          handleMessageDialog("Couldn't save" + file_iv);
        }

        updateStatusBar("IV curve " +counter_iv+ " saved"); // display connecting status
      }
      //save all the iv curve  displayed
      else if (iv_save.getText().trim().equals("A")==true) {
        println("Complete saving");

        try {
          saveTable(Meas_summary, file_sum);
        }
        catch(Exception ioe)
        {
          System.err.println("IOException: " + ioe.getMessage());
          handleMessageDialog("Couldn't save" + file_sum);
        }

        try {
          saveTable(IV_data, file_iv);
        }
        catch(Exception ioe)
        {
          System.err.println("IOException: " + ioe.getMessage());
          handleMessageDialog("Couldn't save" + file_iv);
        }

        try {
          FileWriter fw = new FileWriter(file, true); //the true will append the new data
          int limit;
          if (counter_iv>=max_iv_curve_graph) limit = max_iv_curve_graph;
          else limit = counter_iv;
          for (int i=0; i<limit; i++) {
            fw.write(savedataPartial[i]);//appends the string to the file
          }
          fw.close();
        }
        catch(Exception ioe)
        {
          System.err.println("IOException: " + ioe.getMessage());
          handleMessageDialog("Couldn't save" + file);
        }

        updateStatusBar("All IV curve saved"); // display connecting status
      } else if ( isNumeric(iv_save.getText().trim())) {

        println("Partial saving");
        if (Integer.parseInt(iv_save.getText ().trim())> counter_iv || Integer.parseInt(iv_save.getText ().trim())<= counter_iv-max_iv_curve_graph|| Integer.parseInt(iv_save.getText ().trim())<= 0) {
          handleMessageDialog("IV curve " +iv_save.getText ().trim() + " is not in memory" );
        } else {
          // save only selected iv curve and do not remove any data
          // so far I want to add data to existing file until I press delete data
          Table toSave_sum= new Table();
          Table toSave_IV= new Table();
          recreateTableIV(toSave_IV);
          recreateTableSum(toSave_sum);
          int ind_dataToSave = Arrays.asList(savedataPartial_ind).indexOf(Integer.parseInt(iv_save.getText ().trim()));
          String in = String.valueOf(savedataPartial_ind[ind_dataToSave]);
          println("ID curve to save is: "+ in);
          println("IV saved is: "+ savedataPartial[ind_dataToSave]);
          try {
            FileWriter fw = new FileWriter(file, true); //the true will append the new data
            fw.write(savedataPartial[ind_dataToSave]+eol);//appends the string to the file
            fw.close();
          }
          catch(Exception ioe)
          {
            System.err.println("IOException: " + ioe.getMessage());
            handleMessageDialog("Couldn't save" + file);
          }


          for (TableRow row_to_save : Meas_summary.findRows ( (iv_save.getText ().trim()), "id")) {
            toSave_sum.addRow(row_to_save);
          }
          TableRow newRow = Meas_summary.addRow();
          for (TableRow row_to_save : IV_data.findRows ( (iv_save.getText ().trim()), "id")) {
            toSave_IV.addRow(row_to_save);
          }            

          try {
            addTable(toSave_sum, file_sum);
          }
          catch(Exception ioe)
          {
            System.err.println("IOException: " + ioe.getMessage());
            handleMessageDialog("Couldn't save" + file_sum);
          }
          try {
            addTable(toSave_IV, file_iv);
          }
          catch(Exception ioe)
          {
            System.err.println("IOException: " + ioe.getMessage());
            handleMessageDialog("Couldn't save" + file_iv);
          }
          updateStatusBar("IV curve " +(Integer.parseInt(iv_save.getText ().trim()))+ " save!d"); // display connecting status
        }
      } else {
        handleMessageDialog("N. IV can be numberic or \"A\" to save all data stored");
      }
    }
  } else if (btnChFolder == button && event == GEvent.CLICKED) {
    selectFolder("Select a folder to process:", "folderSelected");
  }
}

int posx_option = 10;
public void createOptions() {
  panelOpt = new GPanel(this, 407, 110, 265, widthy_IV_values*9, "IV settings");
  panelOpt.setCollapsible(false);
  panelOpt.setDraggable(false);
  panelOpt.setText("Options");
  panelOpt.setOpaque(true);
  DHT22 = new GCheckbox(this, posx_option, y_IV_values-4, x_width_btn, y_width_btn);
  DHT22.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  DHT22.setText("Humidity");
  DHT22.addEventHandler(this, "DHT22_clicked");
  DHT22.setOpaque(false);
  autoSaveOpt = new GCheckbox(this, posx_option, y_IV_values+distance_IV*3-4, x_width_btn, y_width_btn);
  autoSaveOpt.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  autoSaveOpt.setText("Auto save");
  autoSaveOpt.addEventHandler(this, "autosave_clicked");
  autoSaveOpt.setOpaque(false);
  //autoSaveOpt.setSelected(true);
  DS18 = new GCheckbox(this, posx_option, y_IV_values+distance_IV-4, x_width_btn+15, y_width_btn);
  DS18.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  DS18.setText("Temperature");
  DS18.addEventHandler(this, "DS18_clicked");
  DS18.setOpaque(false);
  Irr = new GCheckbox(this, posx_option, y_IV_values+distance_IV*2-4, x_width_btn+50, y_width_btn);
  Irr.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  Irr.setText("Fixed irradiance\n(set value mW/cm2)");
  Irr.addEventHandler(this, "irr_clicked");
  Irr.setOpaque(false);
  sett_irr = new GTextField(this, posx_option+150, y_IV_values+distance_IV*2+1, x_width_btn-50, y_width_btn-10, G4P.SCROLLBARS_NONE);
  sett_irr.setOpaque(true);
  sett_irr.setText("100");
  area = new GTextField(this, posx_option+200, y_IV_values+1, x_width_btn-40, y_width_btn-10, G4P.SCROLLBARS_NONE);
  area.setOpaque(true);
  area.setText("1");
  area_lbl = new GLabel(this, posx_option+140, y_IV_values-5, x_width_btn-30, y_width_btn);
  area_lbl.setText("Area(cm2)");
  area_lbl.setTextAlign(GAlign.RIGHT, null);
  area_lbl.setOpaque(false);
  note = new GTextField(this, posx_option+170, y_IV_values+distance_IV, x_width_btn-10, y_width_btn-10, G4P.SCROLLBARS_NONE);
  note.setOpaque(true);
  note.setText("");
  note_lbl = new GLabel(this, posx_option+110, y_IV_values-5+distance_IV, x_width_btn-30, y_width_btn);
  note_lbl.setText("Note");
  note_lbl.setTextAlign(GAlign.RIGHT, null);
  note_lbl.setOpaque(false);
  panelOpt.addControl(DHT22);
  panelOpt.addControl(DS18);
  panelOpt.addControl(Irr);
  panelOpt.addControl(autoSaveOpt);
  panelOpt.addControl(sett_irr);
  panelOpt.addControl(area);
  panelOpt.addControl(area_lbl);
  panelOpt.addControl(note);
  panelOpt.addControl(note_lbl);
}


public void createTimeSettings() {
  int x_time = 170;
  int y_IV_values =40;
  panelTime = new GPanel(this, 700, 110, 250, widthy_IV_values*9, "a");
  panelTime.setCollapsible(true);
  panelTime.setCollapsed(true);
  panelTime.setDraggable(false);
  panelTime.setText("Interval between measurements");
  panelTime.setOpaque(true);
  intervalIV = new GTextField(this, x_time, y_IV_values, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  intervalIV.setOpaque(true);
  intervalIV.setText("1");
  intervalIV_lbl = new GLabel(this, x_time-distance_lbl_IV, y_IV_values, widthx_lbl_IV, widthy_lbl_IV);
  intervalIV_lbl.setText("IV curve (m)");
  intervalIV_lbl.setOpaque(false);
  intervalIV_lbl.setTextAlign(GAlign.RIGHT, null);
  intervalEnv = new GTextField(this, x_time, y_IV_values+distance_IV, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  intervalEnv.setOpaque(true);
  intervalEnv.setText("1");
  intervalEnv_lbl = new GLabel(this, x_time-distance_lbl_IV, y_IV_values+distance_IV, widthx_lbl_IV, widthy_lbl_IV);
  intervalEnv_lbl.setText("Temp. and Humidity (m)");
  intervalEnv_lbl.setTextAlign(GAlign.RIGHT, null);
  intervalEnv_lbl.setOpaque(false);
  intervalIrr = new GTextField(this, x_time, y_IV_values+distance_IV*2, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  intervalIrr.setOpaque(true);
  intervalIrr.setText("1");
  intervalIrr_lbl = new GLabel(this, x_time-distance_lbl_IV, y_IV_values+distance_IV*2, widthx_lbl_IV, widthy_lbl_IV);
  intervalIrr_lbl.setText("Irradiance (m)");
  intervalIrr_lbl.setTextAlign(GAlign.RIGHT, null);
  intervalIrr_lbl.setOpaque(false);

  panelTime.addControl(intervalIV);
  panelTime.addControl(intervalIV_lbl);
  panelTime.addControl(intervalEnv);
  panelTime.addControl(intervalEnv_lbl);
  panelTime.addControl(intervalIrr);
  panelTime.addControl(intervalIrr_lbl);
}
public void extraInternet() {
  int x = 20;
  int y =40;
  int y_shift= 60;
  panelExSet = new GPanel(this, 780, 150, x_width_btn*2+20, widthy_IV_values*4, "a");
  panelExSet.setCollapsible(true);
  panelExSet.setCollapsed(true);
  panelExSet.setDraggable(false);
  panelExSet.setText("Email report");
  panelExSet.setOpaque(true);

  email = new GTextField(this, x+15, y-5, x_width_btn+50, y_width_btn-15, G4P.SCROLLBARS_NONE);
  email.setPromptText("email");

  PCE_limit = new GTextField(this, x+95, y+15, x_width_btn-30, y_width_btn-15, G4P.SCROLLBARS_NONE);
  PCE_limit.setPromptText("PCElim");

  panelExSet.addControl(email);
  panelExSet.addControl(PCE_limit);
}

public void extraSettings() {
  int x = 20;
  int y =40;
  int y_shift= 60;
  GLabel expand_tab = new GLabel(this, 670, 92, 300, 20);
  expand_tab.setText("Click the tabs below to expand/compact each menu:");
  panelExSet = new GPanel(this, 700, 130, 250, widthy_IV_values*7, "a");
  panelExSet.setCollapsible(true);
  panelExSet.setCollapsed(true);
  panelExSet.setDraggable(false);
  panelExSet.setText("Update settings and plot options");
  panelExSet.setOpaque(true);
  btnSvSet = new GButton(this, x, y-5, x_width_btn, y_width_btn);
  btnSvSet.setText("Update settings");
  max_iv = new GTextField(this, x+55, y_IV_values+y_shift, x_width_btn-50, 20, G4P.SCROLLBARS_NONE);
  max_iv.setOpaque(true);
  max_iv.setText("5");
  max_iv_lbl = new GLabel(this, x-40, y_IV_values+y_shift, x_width_btn, 30);
  max_iv_lbl.setText("N. IV to display");
  max_iv_lbl.setOpaque(false);
  max_iv_lbl.setTextAlign(GAlign.RIGHT, null);
  if (sql) {
    database_MySQL = new GCheckbox(this, x, y+40, x_width_btn, y_width_btn);
    database_MySQL.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
    database_MySQL.setText("Database");
    database_MySQL.setOpaque(false);
  }
  refresh_graph = new GCheckbox(this, x+100, y_IV_values+y_shift, x_width_btn+50, 20);
  refresh_graph.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  refresh_graph.setText("Refresh graphs");
  refresh_graph.setSelected(true);
  refresh_graph.setOpaque(false);

  if (eth) {
    con_ethernet = new GCheckbox(this, x+100, y+40, x_width_btn, y_width_btn);
    con_ethernet.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
    con_ethernet.setText("Ethernet");
    con_ethernet.setOpaque(false);
  }
  panelExSet.addControl(max_iv);
  panelExSet.addControl(max_iv_lbl);
  panelExSet.addControl(btnSvSet);

  if (sql)panelExSet.addControl(database_MySQL);
  if (eth)panelExSet.addControl(con_ethernet);
  panelExSet.addControl(refresh_graph);
}


public void createCalibration() {
  int x = 120;
  int y =30;
  panelCal = new GPanel(this, 700, 150, x_width_btn*2+20, widthy_IV_values*4, "a");
  panelCal.setCollapsible(true);
  panelCal.setCollapsed(true);
  panelCal.setDraggable(false);
  panelCal.setText("Calibration");
  panelCal.setOpaque(true);
  btnCalPhoto = new GButton(this, x-25, y+20, x_width_btn, y_width_btn-10);
  btnCalPhoto.setText("Photodiode");
  btnCalTime = new GButton(this, x-115, y+20, x_width_btn, y_width_btn-10);
  btnCalTime.setText("Time");
  cal_photo = new GTextField(this, x-25, y, x_width_btn-50, y_width_btn-10, G4P.SCROLLBARS_NONE);
  panelCal.addControl(btnCalPhoto);
  panelCal.addControl(btnCalTime);
  panelCal.addControl(cal_photo);
  cal_photo.setText("");
}


public void createTextContainers() {
  int redude_x= 25;
  x_IV_values=200;
  panelIV = new GPanel(this, 50+redude_x, 110, widthx_IV_values+widthx_lbl_IV+25-redude_x, widthy_IV_values*9, "IV settings");
  panelIV.setCollapsible(false);
  panelIV.setText("IV settings");
  panelIV.setDraggable(false);
  panelIV.setOpaque(true);
  startV = new GTextField(this, x_IV_values, y_IV_values, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  startV.setOpaque(true);
  startV.setText("-1");
  startV_lbl = new GLabel(this, x_IV_values-distance_lbl_IV, y_IV_values, widthx_lbl_IV, widthy_lbl_IV);
  startV_lbl.setText("Start (V)");
  startV_lbl.setOpaque(false);
  startV_lbl.setTextAlign(GAlign.RIGHT, null);
  stopV = new GTextField(this, x_IV_values, y_IV_values+distance_IV, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  stopV.setOpaque(true);
  stopV.setText("1");
  stopV_lbl = new GLabel(this, x_IV_values-distance_lbl_IV, y_IV_values+distance_IV, widthx_lbl_IV, widthy_lbl_IV);
  stopV_lbl.setText("Stop (V)");
  stopV_lbl.setTextAlign(GAlign.RIGHT, null);
  stopV_lbl.setOpaque(false);
  numPoints = new GTextField(this, x_IV_values, y_IV_values+distance_IV*2, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  numPoints.setOpaque(true);
  numPoints.setText("10");
  numPoints_lbl = new GLabel(this, x_IV_values-distance_lbl_IV, y_IV_values+distance_IV*2, widthx_lbl_IV, widthy_lbl_IV);
  numPoints_lbl.setText("N. points");
  numPoints_lbl.setTextAlign(GAlign.RIGHT, null);
  numPoints_lbl.setOpaque(false);
  numMeasurements = new GTextField(this, x_IV_values, y_IV_values+distance_IV*3, widthx_IV_values, widthy_IV_values, G4P.SCROLLBARS_NONE);
  numMeasurements.setOpaque(true);
  numMeasurements.setText("1");
  numMeasurements_lbl = new GLabel(this, x_IV_values-distance_lbl_IV, y_IV_values+distance_IV*3, widthx_lbl_IV, widthy_lbl_IV+10);
  numMeasurements_lbl.setText("N. of Measurements\n(0 for unlimited)");
  numMeasurements_lbl.setTextAlign(GAlign.RIGHT, null);
  numMeasurements_lbl.setOpaque(false);
  panelIV.addControl(startV); 
  panelIV.addControl(startV_lbl);
  Serial_text_M = new GTextArea(this, 700, 190, 200, 100, G4P.SCROLLBARS_VERTICAL_ONLY);
  Serial_text_M.setOpaque(true);
  ;
  panelIV.addControl(stopV); 
  panelIV.addControl(stopV_lbl);
  panelIV.addControl(numPoints); 
  panelIV.addControl(numPoints_lbl);
  panelIV.addControl(numMeasurements); 
  panelIV.addControl(numMeasurements_lbl);
}
public void updateStatusBar(String s) {
  lblStatus.setText("Status: " + s);
}
public void debug_clicked(GCheckbox source, GEvent event) { //_CODE_:Debug:212813:
} //_CODE_:Debug:212813:

public void autosave_clicked(GCheckbox source, GEvent event) { 
  File f = new File(folder.getText()+fs+filename.getText()+".txt");
  if ( autoSaveOpt.isSelected() && folder.getText() =="") {
    handleMessageDialog("Chose a folder first!");
    autoSaveOpt.setSelected(false);
  } else if  (autoSaveOpt.isSelected() && f.exists() && !f.isDirectory()) {
    // check that the file doesn't exist already

      handleMessageDialog("File already exist! Chose a different filename.");
    autoSaveOpt.setSelected(false);
  } else if  (autoSaveOpt.isSelected() && filename.getText()=="") {
    // check that the file doesn't exist already

    handleMessageDialog("Specify a filename");
    autoSaveOpt.setSelected(false);
  } else if  (measuring==true) {
    // check that the file doesn't exist already

    handleMessageDialog("Cannot change while measuring");
    autoSaveOpt.setSelected(false);
  } else {
  }
}

public void irr_clicked(GCheckbox source, GEvent event) { 
  if (ready==true) {
    if ( Irr.isSelected() == true) {
      Irr.setText("Auto irradiance");
      sett_irr.setText(cal_photo.getText().trim());
      sett_irr.setVisible(false);

      sett_irr.setText(cal_photo.getText().trim());
    } else {
      Irr.setText("Irradiance\n(set value mW/cm2)");
      sett_irr.setText("100");
      sett_irr.setVisible(true);
    }
  } else if (lblStatus.getText().equals("Status: Waiting for connection")==true) {
    if ( Irr.isSelected() == true) {
      Irr.setText("Auto irradiance");
      sett_irr.setText(cal_photo.getText().trim());
      sett_irr.setVisible(false);
    } else {
      Irr.setText("Irradiance\n(set value mW/cm2)");
      sett_irr.setText("100");
      sett_irr.setVisible(true);
    }
  } else {
    handleMessageDialog("Don't change while measuring. Stop and wait for \"Ready to measure\"!");
    if ( Irr.isSelected() == true) {
      Irr.setSelected(false);
    } else {
      Irr.setSelected(true);
    }
  }
}

public void DS18_clicked(GCheckbox source, GEvent event) { 
  if (ready==true) {
  } else if (lblStatus.getText().equals("Status: Waiting for connection")==true) {
  } else {
    handleMessageDialog("Don't change while measuring. Stop and wait for \"Ready to measure\"!");
    if ( DS18.isSelected() == true) {
      DS18.setSelected(false);
    } else {
      DS18.setSelected(true);
    }
  }
}

public void DHT22_clicked(GCheckbox source, GEvent event) { 
  if (ready==true) {
  } else if (lblStatus.getText().equals("Status: Waiting for connection")==true) {
  } else {
    handleMessageDialog("Don't change while measuring. Stop and wait for \"Ready to measure\"!");
    if ( DHT22.isSelected() == true) {
      DHT22.setSelected(false);
    } else {
      DHT22.setSelected(true);
    }
  }
}



public void folderSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    path_save = selection.getAbsolutePath();
    folder.setText(path_save);
  }
}

int md_mtype;
// G4P code for message dialogs
public void handleMessageDialog(String message) {
  // Determine message type
  int mtype;
  switch(md_mtype) {
  default:
  case 0: 
    mtype = G4P.PLAIN; 
    break;
  case 1: 
    mtype = G4P.ERROR; 
    break;
  case 2: 
    mtype = G4P.INFO; 
    break;
  case 3: 
    mtype = G4P.WARNING; 
    break;
  case 4: 
    mtype = G4P.QUERY; 
    break;
  }
  String title = "Message";
  G4P.showMessage(this, message, title, mtype);
}


/**
 * Appends text to the end of a text file located in the data directory, 
 * creates the file if it does not exist.
 * Can be used for big files with lots of rows, 
 * existing lines will not be rewritten
 */
public void appendTextToFile(String filename, String text) {
  File f = new File(dataPath(filename));
  if (!f.exists()) {
    createFile(f);
  }
  try {
    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(f, true)));
    out.println(text);
    out.close();
  }
  catch (IOException e) {
    e.printStackTrace();
    handleMessageDialog("File "+filename + "cannot be written");
  }
}


/**
 * Creates a new file including all subfolders
 */
public void createFile(File f) {
  File parentDir = f.getParentFile();
  try {
    parentDir.mkdirs(); 
    f.createNewFile();
  }
  catch(Exception e) {
    e.printStackTrace();
  }
}    

// Graphic frames used to group controls
ArrayList<Rectangle> rects ;

// Simple graphical frame to group controls
public void showFrame(Rectangle r) {
  noFill();
  strokeWeight(1);
  stroke(color(240, 240, 255));
  rect(r.x, r.y, r.width, r.height);
  stroke(color(0));
  rect(r.x+1, r.y+1, r.width, r.height);
}


synchronized public void win_draw1(GWinApplet appc, GWinData data) { //_CODE_:Calibr_window:917203:
  appc.background(230);
} //_CODE_:Calibr_window:917203:




// Time methods
public void sendTimeMessage(char header, long time) {  
  String timeStr = String.valueOf(time);  
  myPort.write(header);  // send header and time to arduino
  myPort.write(timeStr);
}

public long getTimeNow() {
  // java time is in ms, we want secs    
  GregorianCalendar cal = new GregorianCalendar();
  cal.setTime(new Date());
  int  tzo = cal.get(Calendar.ZONE_OFFSET);
  int  dst = cal.get(Calendar.DST_OFFSET);
  long now = (cal.getTimeInMillis() / 1000) ; 
  now = now + (tzo/1000) + (dst/1000); 
  return now;
}
/*
Update the plots with the new data received from Arduino
 */


/* Setting for IV*/


int posx_plots = 250;
int posy_plots = 300;
float time_start;
float[] panelDim_IV = new float[] {
  370, 380
};



/* Setting for summary*/
float[] firstPlotPos = new float[] {
  25, 25
};
float[] panelDim = new float[] {
  200, 150
};
float[] margins = new float[] {
  0, 30, 300, 0
};



public void update_plots() {
  
   
  //parameters for saving data
  colors_array = generate_colors(Integer.parseInt(max_iv.getText().trim()));
  int max_iv_curve_graph_new = Integer.parseInt(max_iv.getText().trim());
  if (max_iv_curve_graph_new!=max_iv_curve_graph) {
 String[] savedataPartial_tmp = new String[max_iv_curve_graph];
 Integer[] savedataPartial_ind_tmp = new Integer[max_iv_curve_graph]; 
 arrayCopy( savedataPartial,0, savedataPartial_tmp,0,savedataPartial.length);
 arrayCopy( savedataPartial_ind, savedataPartial_ind_tmp);
 savedataPartial=savedataPartial_tmp;
 savedataPartial_ind=savedataPartial_ind_tmp;
  }
  
  // Create a new plot and set its position on the screen
  
  //IV curves
  IV_plot.setPos(posx_plots, posy_plots);
  IV_plot.setDim(panelDim_IV);
  // Set the plot title and the axis labels
  IV_plot.setTitleText("IV plot");
  IV_plot.getXAxis().setAxisLabelText("Voltage [mV]");
  IV_plot.getYAxis().setAxisLabelText("Current [mA]");
  // Preserve initial XLim and YLim for autoscale
  xLimAutoscale = IV_plot.getXLim();
  yLimAutoscale = IV_plot.getYLim();
  // Draw it!
  
    IV_plot.beginDraw();
  IV_plot.drawBox();
  IV_plot.drawXAxis();
  IV_plot.drawYAxis();
  IV_plot.drawTopAxis();
  IV_plot.drawRightAxis();
  IV_plot.drawTitle();
  IV_plot.drawGridLines(GPlot.BOTH);
  IV_plot.activatePanning();
  IV_plot.activateZooming();
  IV_plot.drawPoints();
  IV_plot.drawLines();
  if (l1!=0 || l2!=0) {
    int size = (int)(l2-l1);
    //the +1 is to consider the main layer that cannot be removed
    String[] legend_values = new String[size+1];
    float[] x = new float[size+1];
    float[] y = new float[size+1];
    int j=0;
   for (long i= l1; i <l2+1; i++)
   {
       if (i==l1) legend_values[j]="Legend:"; 
       else legend_values[j]= Long.toString(i);
       
       x[j]= 0.1f;
       y[j]=0.95f-0.04f*j;
      j++; 
   } 
  IV_plot.drawLegend(legend_values,x,y);
  }
  
  
  IV_plot.endDraw();

  // Create four plots to represent the 4 panels

  PCE_plot.setPos(firstPlotPos);
  PCE_plot.setMar(0, margins[1], margins[2], 0);
  PCE_plot.setDim(panelDim);
  PCE_plot.setAxesOffset(0);
  PCE_plot.setTicksLength(-4);
  PCE_plot.getXAxis().setDrawTickLabels(true);
  if(compact_mode==true)
  PCE_plot.setVerticalAxesNTicks(2);
  else
  PCE_plot.setVerticalAxesNTicks(3);

  irr_plot.setPos(firstPlotPos[0] + margins[1] + panelDim[0]+515, firstPlotPos[1]);
  irr_plot.setMar(0, 0, margins[2], margins[3]);
  irr_plot.setDim(panelDim);
  irr_plot.setAxesOffset(0);
  irr_plot.setTicksLength(-4);
  irr_plot.getXAxis().setDrawTickLabels(true);
  irr_plot.activateZooming();
  if(compact_mode==true)
  irr_plot.setVerticalAxesNTicks(2);
  else
  irr_plot.setVerticalAxesNTicks(3);

  temp_plot.setPos(firstPlotPos[0], firstPlotPos[1] + margins[2] + panelDim[1]);
  temp_plot.setMar(margins[0], margins[1], 75, 0);
  temp_plot.setDim(panelDim);
  temp_plot.setAxesOffset(0);
  temp_plot.setTicksLength(-4);
  if(compact_mode==true)
  temp_plot.setVerticalAxesNTicks(2);
  else
  temp_plot.setVerticalAxesNTicks(3);
  

  hum_plot.setPos(firstPlotPos[0] + margins[1] + panelDim[0]+515, firstPlotPos[1] + margins[2] + panelDim[1]);
  hum_plot.setMar(margins[0], 0, 75, margins[3]);
  hum_plot.setDim(panelDim);
  hum_plot.setAxesOffset(0);
  hum_plot.setTicksLength(-4);
  if(compact_mode==true)
  hum_plot.setVerticalAxesNTicks(2);
  else
  hum_plot.setVerticalAxesNTicks(3);

  // Set the points, the title and the axis labels

  PCE_plot.getXAxis().setAxisLabelText("Time [minutes]");
  PCE_plot.getYAxis().setAxisLabelText("PCE [%]");
  PCE_plot.setPointColor(color(100, 100, 255));

  irr_plot.getXAxis().setAxisLabelText("Time [minutes]");
  irr_plot.getYAxis().setAxisLabelText("Irradiance (mW/cm2)");
  irr_plot.setPointColor(color(100, 100, 255));

  temp_plot.getXAxis().setAxisLabelText("Time [minutes]");
  temp_plot.getYAxis().setAxisLabelText("Temperature (oC)");
  temp_plot.setInvertedYScale(false);
  temp_plot.setPointColor(color(100, 100, 255));

  hum_plot.getXAxis().setAxisLabelText("Time [minutes]");
  hum_plot.getYAxis().setAxisLabelText("Humidity [% R.H.]");
  hum_plot.setInvertedYScale(false);
  hum_plot.setPointColor(color(100, 100, 255));

  // Draw the plots
  PCE_plot.beginDraw();
  PCE_plot.drawBox();
  PCE_plot.drawXAxis();
  PCE_plot.drawYAxis();
  PCE_plot.drawTopAxis();
  PCE_plot.drawRightAxis();
  PCE_plot.drawTitle();
  PCE_plot.drawPoints();
  PCE_plot.endDraw();

  irr_plot.beginDraw();
  irr_plot.drawBox();
  irr_plot.drawXAxis();
  irr_plot.drawYAxis();
  irr_plot.drawTopAxis();
  irr_plot.drawRightAxis();
  irr_plot.drawPoints();
  irr_plot.endDraw();

  temp_plot.beginDraw();
  temp_plot.drawBox();
  temp_plot.drawXAxis();
  temp_plot.drawYAxis();
  temp_plot.drawTopAxis();
  temp_plot.drawRightAxis();
  temp_plot.drawPoints();
  temp_plot.endDraw();

  hum_plot.beginDraw();
  hum_plot.drawBox();
  hum_plot.drawXAxis();
  hum_plot.drawYAxis();
  hum_plot.drawTopAxis();
  hum_plot.drawRightAxis();
  hum_plot.drawPoints();
  //hum_plot.drawLines();
  hum_plot.endDraw();
}

// double click on IV graph to autoscale
public void mousePressed() {
  if (IV_plot.isOverBox(mouseX, mouseY)) {
    if (mouseEvent.getClickCount()==2) {
      IV_plot.setXLim(xLimAutoscale);
      IV_plot.setYLim(yLimAutoscale);
    }
  }
}
/*
This method uses data from serial port. It updates the vectors containing the data
 to plot, distinguishing among the different types of data. 
 The initial part of the data trasmitted must begin with 2 letters which identify a
 specific data type. JSON is not compatible because of the special character"
 */
boolean serialPortCreated = false;
String savedata="";
String alldata="";
String eol = System.getProperty("line.separator");
String fs = System.getProperty("file.separator");
ArrayList<Float> dataV = new ArrayList(); // where the most recent voltage data is stored as floats
ArrayList<Float> dataI = new ArrayList(); // where the most recent current data is stored as floats
ArrayList<Float> dataVAll = new ArrayList(); // where all historical voltage data is stored as floats
ArrayList<Float> dataIAll = new ArrayList(); // where all historical current data is stored as floats
float dataVMin = 1.5f; // minimum voltage value in data
float dataVMax = -1.5f; // maximum voltage value in data
float dataIMax = -250; // maximum current value in data
float dataIMin = 250; // minimum current value in data
float maxAbsCurrent; // maximum magnitude of current
float maxAbsVoltage; // maximum magnitude of voltage
boolean IVpoints = false;
//table code
Table IV_data = new Table();
Table Meas_summary = new Table();
Table Meas_summarySens = new Table();
int counter;
int counter_iv=0;
boolean start_iv=true;
int max_iv_curve_graph;  //(the value is inserted must be reduced by one to have the correct result);
String file;
String file_sum;
String file_iv;
String file_sum_sensors;
boolean receiving_data;
//legend in IV graph
static long l1;
static long l2;
int part_save = 0;
String[] savedataPartial;
Integer[] savedataPartial_ind;
int max_memory = 100;
//color
Random rand = new Random();


public void update_data(String serial, GPlot plot_iv, GPlot temp_plot) {
  if (serial.length()>=2) {
    receiving_data = true;
    String selector = serial.substring (0, 2); // extract the first 2 character from sting
    String data = serial.substring(2); // extract the data
    // Arduino is sending an IV curve

    //////////////////////////////////////// complete IV ///////////////////////////////////////////
    if (selector.equals("IV")==true) {

      String[] i_v = splitTokens(data, "[] ");
      println(i_v[1]);
      String curr = i_v[1];  //first part of the token sent are the current points
      String volt = i_v[3];  //third part of the data sent are the voltage points
      // IV data to save
      savedata = "IV{\"current\":[" + curr +"],\"voltage\":" + "[" + volt + "]}" +"TI["+ hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year() + "]";
      float[] c_pts = string2float(curr);
      float[] v_pts = string2float(volt);


      // initialize sensor variables
      float hu=0; 
      float te=0; 
      float ir=0;
      // add to save data and to table other sensors information, if connected
      TableRow newRow = Meas_summary.addRow();
      try {
        if (DHT22.isSelected()==true && DS18.isSelected()==true && Irr.isSelected()==true) {

          hu = Float.parseFloat(i_v[5]); 
          float ta = Float.parseFloat(i_v[7]); 
          newRow.setFloat("Humidity(R.H.)", hu); 
          savedata=savedata+"HU["+hu+"]";
          // println("iv9: "+ i_v[9]);
          te = Float.parseFloat(i_v[9]); 
          newRow.setFloat("Temperature(C)", te);
          savedata=savedata+"DS["+te+"]";
          //println("te: "+ te);
          ir = Float.parseFloat(i_v[11]); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        } else if (DHT22.isSelected()==true && DS18.isSelected()==true && Irr.isSelected()==false) {
          hu = Float.parseFloat(i_v[5]); 
          float ta = Float.parseFloat(i_v[7]); 
          newRow.setFloat("Humidity(R.H.)", hu); 
          savedata=savedata+"HU["+hu+"]";
          te = Float.parseFloat(i_v[9]); 
          newRow.setFloat("Temperature(C)", te);
          savedata=savedata+"DS["+te+"]";
          ir = Float.parseFloat(sett_irr.getText().trim()); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        } else if (DHT22.isSelected()==true && DS18.isSelected()==false && Irr.isSelected()==true) {
          hu = Float.parseFloat(i_v[5]); 
          float ta = Float.parseFloat(i_v[7]); 
          newRow.setFloat("Humidity(R.H.)", hu); 
          savedata=savedata+"HU["+hu+"]";
          ir = Float.parseFloat(i_v[9]); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        } else if (DHT22.isSelected()==true && DS18.isSelected()==false && Irr.isSelected()==false) {
          hu = Float.parseFloat(i_v[5]); 
          float ta = Float.parseFloat(i_v[7]); 
          newRow.setFloat("Humidity(R.H.)", hu); 
          savedata=savedata+"HU["+hu+"]";
          ir = Float.parseFloat(sett_irr.getText().trim()); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        } else if (DHT22.isSelected()==false && DS18.isSelected()==true && Irr.isSelected()==true) {
          te = Float.parseFloat(i_v[5]); 
          newRow.setFloat("Temperature(C)", te);
          savedata=savedata+"DS["+te+"]";
          ir = Float.parseFloat(i_v[7]); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        } else if (DHT22.isSelected()==false && DS18.isSelected()==true && Irr.isSelected()==false) {
          te = Float.parseFloat(i_v[5]); 
          newRow.setFloat("Temperature(C)", te);
          savedata=savedata+"DS["+te+"]";
          ir = Float.parseFloat(sett_irr.getText().trim()); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        } else if (DHT22.isSelected()==false && DS18.isSelected()==false && Irr.isSelected()==true) {
          ir = Float.parseFloat(i_v[5]); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        }
        // no sensors are selected
        else { 
          ir = Float.parseFloat(sett_irr.getText().trim()); 
          newRow.setFloat("Irradiance(mW/cm2)", ir);
          savedata=savedata+"IR["+ir+"]";
        }
      }
      catch (NumberFormatException   a) {
        handleMessageDialog("Error some sensors are probably not connected!");
        updateStatusBar("Stop! Finishing current measurement"); // display connecting status 
        ready=true; //ready to measure again - this should come
        measuring =false; //not sure
        String command = "O";
        myPort.write(command);
      }
      catch (ArrayIndexOutOfBoundsException  a) {
        handleMessageDialog("Do not change sensor option while measuring!");
        updateStatusBar("Stop! Finishing current measurement"); // display connecting status 
        ready=true; //ready to measure again - this should come
        measuring =false; //not sure
        String command = "O";
        myPort.write(command);
      }
      //savedata=savedata+eol;

      double[] para = new double[5];
      float pce;

      // extract photovoltaic parameters
      try {
        para = estrapolate_pce(v_pts, c_pts, ir);
        pce = (float)para[0];
      }
      catch (Exception e)
      {
        pce=0;
        para[0]=0;
        para[1]=0;
        para[2]=0;
      }

      //add pce to plot
      PCE_plot.addPoint((millis()/1000-time_start)/60, pce);
      println(para[0], " ", para[1], " ", para[2], " ", para[3], " ", para[4]);
      NumberFormat formatter = new DecimalFormat("#0.0");   
      // print to GUI result of IV curve
      Serial_text_M.appendText("ID: "+Integer.toString(counter_iv)+" PCE: "+ formatter.format(para[0])+" Isc(mA): " + formatter.format(para[1]) + " Voc(V): " + formatter.format(para[2])+" FF(%): " + formatter.format(para[3]) );


      // add data to "Meas_summary" table
      newRow.setString("id", Integer.toString(counter_iv));
      newRow.setString("Time", hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year());
      newRow.setFloat("PCE(%)", (float)para[0]);
      newRow.setFloat("Isc(mA)", (float)para[1]);
      newRow.setFloat("Voc(V)", (float)para[2]);
      newRow.setFloat("FF(%)", (float)para[3]);
      newRow.setFloat("Pmax(mW)", (float)para[4]);
      newRow.setFloat("Time(Unix)", (float)getTimeNow());
      newRow.setString("Note", note.getText());  //note is only for iv curve
      newRow.setFloat("Area(cm2)", Float.parseFloat(area.getText().trim()));

      // add data to "IV_data" table: this data is inserted after every IV point
      newRow = IV_data.addRow();
      newRow.setFloat("PCE(%)", (float)para[0]);
      newRow.setFloat("Isc(mA)", (float)para[1]);
      newRow.setFloat("Voc(V)", (float)para[2]);
      newRow.setFloat("FF(%)", (float)para[3]);
      newRow.setFloat("Pmax(mW)", (float)para[4]);
      newRow.setString("id", Integer.toString(counter_iv));
      newRow.setFloat("Time(Unix)", (float)System.currentTimeMillis() / 1000L);
      newRow.setString("Time", hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year());
      newRow.setString("Note", note.getText());  //note is only for iv curve
      newRow.setFloat("Area(cm2)", Float.parseFloat(area.getText().trim()));
      try {
        if (DHT22.isSelected()==true) { 
          newRow.setFloat("Humidity(R.H.)", hu);
        }
        if (DS18.isSelected()==true) { 
          newRow.setFloat("Temperature(C)", te);
        }
        // irr is always inserted: from sensor or value specified
        newRow.setFloat("Irradiance(mW/cm2)", ir);
      }
      catch (Exception e) {
        handleMessageDialog("Couldn't save to IV_data table");
      }



      // save iv curve in memory for .txt file
      savedataPartial_ind[part_save] = counter_iv;
      savedataPartial[part_save] = savedata;
      println("part "+part_save);
      // colors
      IV_plot.getLayer(Integer.toString(counter_iv)).setPointColor(colors_array[part_save]);
      IV_plot.getLayer(Integer.toString(counter_iv)).setLineColor(colors_array[part_save]);
      //legend
      l2++;
      part_save++;
      
      if (autoSaveOpt.isSelected()==true) {
        // AUTOSAVE ON
        // read filename everytime. iv curve are saved in separate iv file
        autoSave();
        Serial_text_M.setText("");   // when the data is saved, there is no need to show it on the message area
      }


      if (part_save==max_iv_curve_graph) {
        part_save=0;
      }
      // meas_summary has one line for every iv curve
      // autosave: meas_summary is clean every time a data is saved
      if (Meas_summary.getRowCount()==max_iv_curve_graph+1) {
        Meas_summary.removeRow(0);
        Table toSave_IV =new Table();
        recreateTableIV(toSave_IV);
        for (int i=counter_iv-max_iv_curve_graph+1; i<=counter_iv; i++) {
          println("id= "+i);
          int j=0;
          for (TableRow row_to_keep : IV_data.findRows ( Integer.toString (i), "id")) {
            toSave_IV.addRow(row_to_keep);
            println("id rows of measure to keep= "+toSave_IV.getString(j, "id"));
            j++;
          }
        }
        for (TableRow row : IV_data.rows ()) {
          println(row.getString("id"));
        }
        for (TableRow row : toSave_IV.rows ()) {
          println(row.getString("id"));
        }
        IV_data = toSave_IV;
      }

      // keep only curve shown in the legend in memory:
      // part_save is the index of the array savedataPartial: when the index reaches the savedataPartial_ind.length=(max_iv_curve_graph-1)
      // then overwrite initial values

      // if something was inserted in PCE limit then it is possible to send an email (only in stability studies)
      if ( isNumeric(PCE_limit.getText().trim()) && email.getText().trim().equals("")==false &&  (autoSaveOpt.isSelected()==true) && (Float.parseFloat(numMeasurements.getText().trim())>1 || Float.parseFloat(numMeasurements.getText().trim())==0) ) {
        println("send email");
        // the  email is sent only once
        if (pce<Float.parseFloat(PCE_limit.getText().trim()) && email_sent==false) {
          send_email(String.valueOf(pce), email.getText().trim());
        }
      } else if (isNumeric(PCE_limit.getText().trim()) && email.getText().trim().equals("")==false &&  (autoSaveOpt.isSelected()==false))
        Serial_text_M.appendText("Can't use email function without autosave");
      else if (isNumeric(PCE_limit.getText().trim()) && email.getText().trim().equals("")==false &&  Float.parseFloat(numMeasurements.getText().trim())==1)
        Serial_text_M.appendText("Can't use email function with single iv measurement");
      count_data++;
      start_iv=true; //iv curve done
    }

    // these data doesn't have any id information --> it cannot be saved with single iv saving
    else if (selector.equals("DS")==true) {
      String[] t_e = splitTokens(data, "[] ");
      float te = Float.parseFloat(t_e[0]); 
      temp_plot.addPoint((millis()/1000-time_start)/60, te);
      // data is not saved if the autosave mode is not active
      if (autoSaveOpt.isSelected()==false) {
      } else if (autoSaveOpt.isSelected()==true) {
        savedata = "DS[" + te +"]TI["+ hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year() +"]";
        TableRow newRow = Meas_summarySens.addRow();
        newRow.setFloat("Temperature(C)", te);
        newRow.setFloat("Time(Unix)", (float)System.currentTimeMillis() / 1000L);
        newRow.setString("Time", hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year());
        autoSaveSensors();
      }
    } else if (selector.equals("HU")==true) {
      float hu=0;
      try {
        String[] h_u = splitTokens(data, "[] ");
        hu = Float.parseFloat(h_u[0]);
      }
      catch (NumberFormatException a) {
        handleMessageDialog("DHT sensor is probably not connected!");
      }
      if (autoSaveOpt.isSelected()==false) {
      } else if (autoSaveOpt.isSelected()==true) {
        savedata = "HU[" + hu +"]TI["+ hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year() +"]";
        TableRow newRow = Meas_summarySens.addRow();
        newRow.setFloat("Humidity(R.H.)", hu);
        newRow.setFloat("Time(Unix)", (float)System.currentTimeMillis() / 1000L);
        newRow.setString("Time", hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year());
        autoSaveSensors();
      }
      hum_plot.addPoint((millis()/1000-time_start)/60, hu);
    } else if (selector.equals("TA")==true) {
      // TA is the temperature from DHT22, the value is not accurate
      String[] t_a = splitTokens(data, "[] ");
      float ta = Float.parseFloat(t_a[0]);
      savedata = "TE[" + ta +"]TI["+ hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year() +"]";
      String[] list = split(savedata, ' ');
      if (autoSaveOpt.isSelected()==true) {
        autoSaveSensors();
      }
    } else if (selector.equals("IR")==true) {
      String[] i_r = splitTokens(data, "[] ");
      float ir = Float.parseFloat(i_r[0]);
      // add to measurement file
      if (autoSaveOpt.isSelected()==false) {
        TableRow newRow = Meas_summary.addRow();
        newRow.setFloat("Irradiance(mW/cm2)", ir);
        newRow.setFloat("Time(Unix)", (float)System.currentTimeMillis() / 1000L);
        newRow.setString("Time", hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year());
      } else if (autoSaveOpt.isSelected()==true) {
        TableRow newRow = Meas_summarySens.addRow();
        newRow.setFloat("Irradiance(mW/cm2)", ir);
        newRow.setFloat("Time(Unix)", (float)System.currentTimeMillis() / 1000L);
        newRow.setString("Time", hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year());
      }
      irr_plot.addPoint((millis()/1000-time_start)/60, ir);
      alldata = alldata + "IR[" + ir +"]TI["+ hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year() +"]"+eol;
      savedata = "IR[" + ir +"]TI["+ hour()+":"+minute()+":"+second()+" "+day()+" "+month()+" "+year() +"]";
      String[] list = split(savedata, ' ');
      if (autoSaveOpt.isSelected()==true) {
        autoSaveSensors();
      }
    } else if (selector.equals("RE")==true) {  //Ready
      updateStatusBar("Ready to measure");
      ready=true;
      btnConnect.setLocalColorScheme(GCScheme.GREEN_SCHEME);
      btnStMeas.setLocalColorScheme(GCScheme.GREEN_SCHEME);
    }
    // a message is sent to be written
    else if (selector.equals("ME")==true) {  
      Serial_text_M.appendText(data.substring(0, data.length()-2));
      println(data.substring(0, data.length()-2));
      if (data.equals("Curve sweep complete.")==true) Serial_text_M.appendText(Integer.toString(count_data));  //write to know which iv to save
      if (data.substring(0, data.length()-2).equals("Settings saved to SD")==true)  handleMessageDialog("Settings saved to SD"); 
      ;
    } else if (selector.equals("US")==true) {  //update status from arduino
      data = data.substring(0, data.length()-2); // remove extra character from arduino
      if (data.equals("Ready to measure")==true) { 
        ready=true; 
        measuring=false;
      }  //status is ready to measure
      updateStatusBar(data);
    } else if (selector.equals("IP")==true) {  //single point IV
      String[] dataStrings = split(trim(data), ',');
      float[] dataPoint = PApplet.parseFloat(dataStrings);

      float[] pV = {
        dataPoint[0]
      };
      float[] pI = {
        -dataPoint[1]
      };
      //multiple lines in plot
      GPointsArray pointsIV = new GPointsArray(pV, pI);
      GPoint pointIV = new GPoint(dataPoint[0], -dataPoint[1]);

      // Start iv is true only before the first iv point is inserted
      if (start_iv==true) {  
        start_iv = false;
        // limit max iv curve to display, this control can be done before the first iv point is inserted
        if (counter_iv>max_iv_curve_graph-1) {
          IV_plot.removeLayer(Integer.toString(counter_iv-max_iv_curve_graph+1));
          l1++;
        }
        // first iv curve is number 1

        counter_iv++; // counter_iv is reset only when the software is closed or when delete is pressed(it is used for plotting in different layers) or if the mode reset is on

        // add layer for new iv curve
        plot_iv.addLayer(Integer.toString(counter_iv), pointsIV);
      } else {
        // add point to existing layer
        plot_iv.addPoint(pointIV, Integer.toString(counter_iv));
      }

      // insert data to IV_data
      TableRow newRow = IV_data.addRow();
      newRow.setFloat("Voltage(V)", dataPoint[0]);
      newRow.setFloat("Current(mA)", -dataPoint[1]);
      newRow.setString("id", Integer.toString(counter_iv));
    } else {
      myPort.clear();
    }
  }
}



public float[] string2float(String iv) {
  String[] current_pts_str = splitTokens(iv, ",");
  float[] current_pts = new float[current_pts_str.length];
  for (int i=0; i<current_pts_str.length; i=i+1) {
    current_pts[i] = Float.parseFloat(current_pts_str[i]);
  }
  return current_pts;
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "GUI_MMkit_BitBucket" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
