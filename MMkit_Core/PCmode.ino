////////////////////////////// Methods used to interface with MMkit GUI (PC mode) ///////////////////////////////

void connectPC(){
    if (Serial.available() > 0){
        int firstByte = Serial.read();
        
        if (firstByte == 'C'){  // the char 'C' from the Processing app (MMkit GUI) indicates it is trying to reconnect - PC mode
            if (DEBUG) Serial.println(F("You are connected to MMkit"));  // signal to applet that it's connected
            settings.pcmode = 1;  // entering PC mode when C is receaved (versus Free mode, see plasticphotovoltaics.org/MMkit for more information)
            // autostart mode allows MMkit to start automatically if an accidental power black out occurs
            settings.autostart=0;
            setAutoStart(false); // PC mode disable auto start
            delay(DELAY_SHOW_SERIAL);
            Serial.println(F("RE"));  //Tell the GUI that MMkit is ready
        }
        else if(firstByte == 'S'){  // 'S' indicates starting a measurement
            count = 0; // reset number of measurement done so far
            go =true; // you can start a measurement
            delay(400);
            // setting up the parameters of measurement
            settings.startV = Serial.parseFloat();
            settings.stopV = Serial.parseFloat();
            settings.numPoints = (int)Serial.parseFloat();
            settings.nmeas = (int)Serial.parseFloat();
            
            // autostart on PC mode is not activated because user might not save the settings to the SD card before starting the measurement
            // during an autostart in fact, the settings are loaded from the SD card, which can lead to unwanted measurements
            if (settings.nmeas==0) { // if 0 is chosen as number of measurements then it will be infinite (automode)
                setAutoStart(false);
                settings.automode=true;
            }
            else if (settings.nmeas>1)
            {
              setAutoStart(false);
              settings.automode=false;
            }
            else {  // disable automode
                setAutoStart(false);
                settings.automode=false;
            }
        }
        else if(firstByte == 'D'){  // 'D' managing debug mode
            int tmp = (int)Serial.parseFloat();
            if (tmp ==1)
                DEBUG=true;
            else if (tmp==0)
                DEBUG = false;
        }
        else if(firstByte == 'V'){  // 'V' indicates iV settings
            int tmp = (int)Serial.parseFloat();
            if (tmp ==1) {  // set start voltage
                settings.startV = Serial.parseFloat();
                delay(DELAY_SHOW_SERIAL);
                Serial.print(F("StartV: "));
                Serial.println(settings.startV);
            }
            else if (tmp==2) { //set stop voltage
                settings.stopV = Serial.parseFloat();
                delay(DELAY_SHOW_SERIAL);
                Serial.print(F("StopV: "));
                Serial.println(settings.stopV);
            }
            else if (tmp==3) {  //set number of points to acquire during measurement
                settings.numPoints = Serial.parseFloat();
                delay(DELAY_SHOW_SERIAL);
                Serial.print(F("numPoints: "));
                Serial.println(settings.numPoints);
            }
            else if (tmp==4) {  //set number of measurements to perform
                settings.nmeas = Serial.parseFloat();
                delay(DELAY_SHOW_SERIAL);
                Serial.print(F("nmeas: "));
                Serial.println(settings.nmeas);
            }
            else if (tmp==5) {  //set calibration of photodiode
                delay(DELAY_SHOW_SERIAL);
                settings.calibrationPh = Serial.parseFloat();
            }
            else if (tmp==6) {  //possibility to include ethernet connection
                delay(DELAY_SHOW_SERIAL);
                settings.eth = (int)Serial.parseFloat();
            }
            else if (tmp==7) {  //activate IV curve measuring (set to true as default)
                delay(DELAY_SHOW_SERIAL);
                settings.sIV = (int)Serial.parseFloat();
            }
            
        }
        else if(firstByte == 'H'){  // 'H' activate/deactivate humidity measurement
            int tmp = (int)Serial.parseFloat();
            if (tmp ==1)
                settings.sDHT22=1;
            else if (tmp==0)
                settings.sDHT22=0;
        }
        else if(firstByte == 'T'){  // 'T' indicates activate/deactivate temperature measurement
            int tmp = (int)Serial.parseFloat();
            if (tmp ==1)
                settings.sDS18B20=1;
            else if (tmp==0)
                settings.sDS18B20=0;
        }
        else if(firstByte == 'I'){  // 'I' activate/deactivate irradiance measurement
            int tmp = (int)Serial.parseFloat();
            if (tmp ==1)
                settings.sPHOTO=1;
            else if (tmp==0)
                settings.sPHOTO=0;
        }
        else if(firstByte == 'O'){  // 'O' indicates stopping a measurement
            settings.automode=false;  // stop automode and autostart when accidental rebooting
            go=false; // stop measuring
            setAutoStart(false);
            if (DEBUG) Serial.println(F("Stop measurement"));
        }
        // Calibration mode
        else if(firstByte == 'A'){  //
            int tmp = (int)Serial.parseFloat();
            // calibrate photodiode
            if (tmp == 1) {
                while ( (int)Serial.parseFloat() != 1 ) {
                    delay(DELAY_SHOW_SERIAL/2);
                    ir_IV_index = 1;
                    while (ir_IV_index<= CNT) {
                        irr.push(analogRead(analogPhotodiodePin));
                        ir_IV_index+=1;
                    }
                    Serial.println(irr.mean()-correct_photo);
                    delay(DELAY_SHOW_SERIAL);
                }
                if (DEBUG) Serial.println(F("Stop photodiode calibration"));
            }
            // calibrate time
            if (tmp == 2) {
                delay(2*DELAY_SHOW_SERIAL);
                Serial.println(F("Start time calibration"));
                delay(2*DELAY_SHOW_SERIAL);
                Serial.print("T\n");
                delay(DELAY_SHOW_SERIAL);
                if(Serial.available() )
                {
                    if (DEBUG) Serial.println(F("Serial available"));
                    delay(DELAY_SHOW_SERIAL);
                    processSyncMessage();
                }
                delay(1000);
                Serial.println(digitalClockDisplay());
            }
        }
        // Receaving the string M allows choosing the interval of each measurements
        else if(firstByte == 'M'){
            int tmp = (int)Serial.parseFloat();
            // time between IV curve measurements
            if (tmp == 1)
                settings.intervalIV = Serial.parseFloat()*60; //calculating seconds from minutes
            // time between humidity+temperature measurements
            if (tmp == 2)
                settings.intervalEnv = Serial.parseFloat()*60;
            // time between irradiance measurements
            if (tmp == 3)
                settings.intervalPh = Serial.parseFloat()*60;
            
        }
        // Write setting to SD card
        else if(firstByte == 'E'){  //
            delay(400);
            int firstByte = (int)Serial.parseFloat();
            if(firstByte == 1){  // attemps writing to SD card
                delay(DELAY_SHOW_SERIAL);
                Serial.println(F("Attemp writing to SD card"));
                if (settings.SDuse) {
                    setAutoStart(false);
                    setSettingsFromPC();
                }
                else if (!settings.SDuse) {
                    delay(DELAY_SHOW_SERIAL);
                    Serial.print(MESSAGE);
                    Serial.println(F("Settings updated (SD error)"));
                }
            }
        }
        else if(firstByte == 'R'){  // print settings to serial port
            printSettings();
        }
        else{
            Serial.flush();  // in case of garbage serial data, flush the buffer
        }
    }
}




