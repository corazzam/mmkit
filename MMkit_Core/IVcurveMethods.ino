
////////////////////////////// Measuring of IV curves related methods ///////////////////////////////

void runVoltageSweep() {
    
    #define size_array 50  //Jsonarray. max 50 points for current and voltage
    //Json array having both current and voltage goes to buffer. Each point 10 digits, 100 points for each current and voltage
    // --> 10*50*2=1000  NOTE: 2000 runs out of memory
    char buffer[1000];
    
    boolean currentOverload = false; // indicates currentOverload during sweep
    
    // create variable for start and stop voltage DAC level:
    int startVLevel = settings.startV*250;
    int stopVLevel = settings.stopV*250;
    
    // create increment to obtain numPoints number of data points
    int stepLevel = (stopVLevel - startVLevel) / (settings.numPoints);
    
    Serial.println(F("**** Starting new measurement ****"));
    Serial.print(F("Starting Voltage = "));   Serial.print(startVLevel);  Serial.println(" mV");
    Serial.print(F("Stop Voltage \t =  "));   Serial.print(stopVLevel);   Serial.println(" mV");
    Serial.print(F("Voltage Step \t =    "));   Serial.print(stepLevel);   Serial.println(" mV");
    
    // define current and voltage variables:
    float current;
    float deviceVoltage;
    int sweep; //voltage sweep function
    
    //Offset on the DAC to lift the Virtual GND - One terminal at offset instead of zero voltage.
    // lifted 1V; for voltages above 3 or below -3 offset is zero
    
    int offset = 0;
    
    Serial.print(F("Voltage Offset \t =    "));
    Serial.print(offset);
    Serial.println(" mV");
    Serial.println(F("Enabling OpAmp"));
    digitalWrite(OpAMPpin,HIGH);
    
    delay(500);
    
    JsonArray<size_array> I;
    JsonArray<size_array> V;
    double mA=0;
    
    int level = startVLevel;
    for (int i = 0; i <= settings.numPoints; i += 1) {
        
        // check if the start/stop button has been pressed or stop from the MMkit GUI has been pressed
        stop_check();
        connectPC();
        
        if (level<0){
            int Vpos = dac.analogWrite(0,0+offset);
            int Vneg = dac.analogWrite(1,-level+offset);
        }
        else{
            int Vpos = dac.analogWrite(0,level+offset);
            int Vneg = dac.analogWrite(1,0+offset);
        }
        
        //measure the voltage
        double mV = measure_mV()*6.0172+6.2083;
        double mV_wanted;
        if (abs(mA) > current_limit) {  // check if the current limit has been reached and interrupt the measurement
            delay(DELAY_SHOW_SERIAL);
            Serial.println(("MEYou have reached current limit"));
            break;
        }
        else {
            mV_wanted = startVLevel*4+i*stepLevel*4; //considering gain=4
        }
        
        // start a loop to converge the value of voltage to the wanted one, in case of error
        int resol_iv=30; // difference from value measured and value wanted (mV) in order to exit the loop
        int n_max = 15; // for speed reason limit the number of iterations 
        int ii=0; // initialize a counter for the conversion
        while ((abs(abs(mV)-abs(mV_wanted)))>resol_iv && (ii < n_max)) {
            // change the values that determines a voltage in order to improve the conversion to the wanted value
            if ((mV-mV_wanted)<0) {
                level = level+10;
            }
            else if ((mV-mV_wanted)>0) {
                level = level-10;
            }
            
            if ((mV_wanted)<0)
            {
                int Vpos = dac.analogWrite(0,0+offset);
                int Vneg = dac.analogWrite(1,-level+offset);
            }
            else{
                int Vpos = dac.analogWrite(0,level+offset);
                int Vneg = dac.analogWrite(1,0+offset);
            }
            mV = measure_mV()*6.0172+6.2083;
            ii+=1;
        }
        
        // measure current
        mA = measure_mA()*2.5*0.5+correct_error;  // Added calibration values: Gain of 0.5 (oamp)x2.5(new Shunt 2.5 times smaller) and offset at 0V(correct_error)
        level += stepLevel;
        
        
        Serial.print("IP");  //code to tell processing of new IV data points
        Serial.print(mV,4);
        Serial.print(",");
        Serial.println(mA,4);
        
        int mV_i = mV;            // Get the integer part (678).
        float mV_f = mV - mV_i;     // Get fractional part (678.0123 - 678 = 0.0123).
        int mV_d = trunc(abs(mV_f) * 10000);   // Turn into integer (123).
        //int d2 = (int)(f2 * 10000); // Or this one: Turn into integer.
        int mA_i = mA;            // Get the integer part (678).
        float mA_f = mA - mA_i;     // Get fractional part (678.0123 - 678 = 0.0123).
        int mA_d = trunc(abs(mA_f) * 10000);   // Turn into integer (123).
        
        // Update screen with new data point
        sprintf(display_data_l2,"%d.%04d mV",mV_i,mV_d);
        sprintf(display_data_l3,"%d.%04d mA",mA_i,mA_d);
        data_screen_line(FILE_EXP,display_data_l2,display_data_l3,display_data_l4,display_data_l5,display_data_l6,display_data_l7);
        
        if (DEBUG) Serial.println(F("Add to array"));
        
        // add data point to vector
        I.add<4>(mA);
        V.add<4>(mV);
    }
    
    delay(DELAY_SHOW_SERIAL);
    Serial.println(F("MECurve sweep complete.")); // tell to MMkit GUI successful sweep.
    delay(DELAY_SHOW_SERIAL);
    
    JsonObject<2> IV_curve;
    IV_curve["current"] = I;
    IV_curve["voltage"] = V;
    
    resetVOpAmp();
    
    // Print full IV curve to serial and to SD card
    IV_curve.printTo(buffer,sizeof(buffer));
    delay(300);
    Serial.print("IV");
    Serial.print(buffer);
    
    
    if (settings.SDuse) {
        if (!file.open(FILE_EXP, O_WRITE | O_CREAT | O_AT_END)) {
            sd.errorHalt("opening filename for write failed");
            }
        file.print("IV");
        file.print(buffer);
        delay(1000);
        
    }
    
    // When measuring an IV curve, temperature, humidity and irradiance are also measured
    double temp_ds18;
    char temp_ds18_char[10];
    char temp_hum[10];
    char temp_temp[10];
    
    if (settings.sDHT22==1) {
        int chk = DHT.read22(DHT22PIN);
        double hum_DHT_tmp = DHT.humidity;
        double tem_DHT_tmp = DHT.temperature;
        dtostrf(DHT.humidity,2,1,temp_hum);
        dtostrf(DHT.temperature,2,1,temp_temp);
        Serial.print("HU[");
        Serial.print(hum_DHT_tmp);
        Serial.print("]");
        if (settings.SDuse) {file.print("HU[");file.print(hum_DHT_tmp);file.print("]") ; }
        Serial.print("TE[");
        Serial.print(tem_DHT_tmp);
        Serial.print("]");
        if (settings.SDuse) {file.print("TE[");file.print(tem_DHT_tmp);file.print("]") ; }
    }
    if (settings.sDS18B20==1) {
        temp_ds18 = ds18b20();
        dtostrf(temp_ds18,2,2,temp_ds18_char);
        Serial.print("DS[");
        Serial.print(ds18b20());
        Serial.print("]");
        if (settings.SDuse) {file.print("DS[");file.print(temp_ds18);file.print("]");  }
    }
    if (settings.sPHOTO==1) {
        Serial.print("IR[");
        ir_IV_index = 1;
        while (ir_IV_index<= CNT) {
            irr.push(analogRead(analogPhotodiodePin));
            ir_IV_index+=1;
        }
        float ir_tmp = (irr.mean()-correct_photo)/settings.calibrationPh*100;
        char photo_char[6];
        dtostrf(ir_tmp,6,2,photo_char);
        Serial.print(photo_char);
        Serial.print("]");
        if (settings.SDuse) {file.print("IR[");file.print((int)ir_tmp);file.print("]") ; }
    }
    char time[30];
    
    file.close();
    
    Serial.print("\n");
    
    if ( settings.mysql==1 ) { // mysql export not implemented
        
    }
}


//Rounds down (via intermediary integer conversion truncation)
String doubleToString(double input,int decimalPlaces){
    if(decimalPlaces!=0){
        String string = String((int)(input*pow(10,decimalPlaces)));
        if(abs(input)<1){
            if(input>0)
            string = "0"+string;
            else if(input<0)
            string = string.substring(0,1)+"0"+string.substring(1);
        }
        return string.substring(0,string.length()-decimalPlaces)+"."+string.substring(string.length()-decimalPlaces);
    }
    else {
        return String((int)input);
    }
}


double measure_mV() {
    chan = 0; // Channel (Voltage)
    res = 2; // set resolution
    gain = 0; // x1
    mvDivisor = 1 << (gain + 2*res);
    adcConfig = 0;  // if adcConfig not reset, chan gets stuck at 3
    adcConfig |= chan << 5 | res << 2 | gain | MCP342X_START;
    mcp342xWrite(adcConfig);
    int32_t data;
    if (!mcp342xRead(data)) halt();
    
    return (double)data/mvDivisor;  // in milivolts
    
}


double measure_mA() {
    chan = 1; // Channel (Current)
    res = 2; // set resolution
    gain = 3; // x8
    mvDivisor = 1 << (gain + 2*res);
    adcConfig = 0;  // if adcConfig not reset, chan gets stuck at 3
    adcConfig |= chan << 5 | res << 2 | gain | MCP342X_START;
    mcp342xWrite(adcConfig);
    int32_t data;
    if (!mcp342xRead(data)) halt();
    
    return (double)data/mvDivisor;  // in milivolts
    
}

void resetVOpAmp() {
    //Reset DAC - Setting Vref and Gain is needed for correct conversion of bits to voltages.
    dac.analogWrite(0,0,0,0);
    dac.setVref(1,1,1,1);
    dac.setGain(1,1,1,1);
    dac.setPowerDown(0, 0, 3, 3); //( 0 - On; 3 - 500k resistance to GND)
    digitalWrite(OpAMPpin,LOW);
    digitalWrite(relaypin,LOW);  // light off
}



void softReset(){
    asm volatile ("  jmp 0");
}

//------------------------------------------------------------------------------
void halt(){ // only used for mcp3424 read function
    Serial.println(F("*ERROR* - Halted - check address and wiring of MCP3424 ADC"));
    Serial.println(F("Press 'r' to reset the program"));
    while(1){
        if( Serial.available()>0){ if( Serial.read() == 'r' ){ Serial.println(F("resetting program \n")); delay(100); softReset(); } }
    }
}

// Functions for using mcp3424 ADC (have not been able to make a functional library)

//------------------------------------------------------------------------------
// read mcp342x data - updated 10mar11/wbp
uint8_t mcp342xRead(int32_t &data)
{
    uint8_t *p = (uint8_t *)&data;   // pointer used to form int32 data
    uint32_t start = millis();            // timeout - not really needed?
    if ((adcConfig & MCP342X_RES_FIELD) == MCP342X_18_BIT)  // in 18 bit mode?
    {
        Serial.println(F("*ERROR* - MCP3424 ADC 18bit read not implemented"));
    }
    else
    {
        do {  // 12-bit to 16-bit mode
            Wire.requestFrom(MCP342X_ADDRESS, 3);
            if (Wire.available() != 3) {
                Serial.println(F("*ERROR* - MCP3424 ADC 12-16bit read failed"));
                return false;
            }
            p[1] = Wire.read();
            p[0] = Wire.read();
            p[2] = p[1] & 0X80 ? 0XFF : 0;         // extend sign bits
            p[3] = p[2];
            uint8_t s = Wire.read();          // read config/status byte
            if ((s & MCP342X_BUSY) == 0) return true;  // or escape here
        }
        while (millis() - start < 500);   // allows rollover of millis()
    }
    Serial.println(F("*ERROR* - ADC read timeout"));      // dang it
    return false;
}

//------------------------------------------------------------------------------
// write mcp342x configuration byte
uint8_t mcp342xWrite(uint8_t config)
{
    Wire.beginTransmission(MCP342X_ADDRESS);
    Wire.write(config);
    Wire.endTransmission();
}
