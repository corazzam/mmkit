////////////////////////////// Methods used to interface with SD card ///////////////////////////////

// the following variables are long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 50;    // the debounce time; increase if the output flickers


void setSettings() { // set standard values of settings
    settings.intervalIV=1; //1 second
    settings.intervalEnv=1;
    settings.intervalPh=1;
    settings.calibrationPh=455;
    settings.sDHT22=0;
    settings.sDS18B20=0;
    settings.sPHOTO=0;
    settings.sIV=1;
    settings.startV=-1;
    settings.stopV=1;
    settings.numPoints=5;
    settings.nmeas=3;
    settings.pcmode=0;  //only if PC sends "C" pcmode can be true
    settings.automode=0;
    settings.SDuse=1;
    //
    settings.localmysql=1;// not implemented
    settings.domainip1=192;// not implemented
    settings.domainip2=168;// not implemented
    settings.domainip3=1;// not implemented
    settings.arduinoip=1;// not implemented
    settings.mysqlip=1;// not implemented
    settings.dynIP=0;// not implemented
    settings.eth=0;// not implemented
    settings.wifi=0;// not implemented
    settings.mysql=0; // not implemented
    
    Serial.println(F("Default settings set from Arduino!"));
}


void setSettingsFromPC() {
    // Write in the SD card the current settings - use in Free Mode and requested from MMkit GUI:
    Serial.println(F("Attempt set settings..."));
    //Rewrite settings file
    if (!myFile.open("settings.txt", O_WRITE | O_CREAT | O_AT_END )) {
        sd.errorHalt("opening filename for write failed");
            delay(DELAY_SHOW_SERIAL);
        Serial.print(MESSAGE);
        Serial.println(F("SD error"));
    }
    if (!myFile.remove()) Serial.println("Error file.remove");
    if (!myFile.open("settings.txt", O_WRITE | O_CREAT | O_AT_END ))
        myFile.println(F("//Settings file"));
    myFile.print(F("intervalIV = "));
    myFile.println(settings.intervalIV);
    myFile.print(F("intervalEnv = "));
    myFile.println(settings.intervalEnv);
    myFile.print(F("intervalPh = "));
    myFile.println(settings.intervalPh);
    myFile.print(F("sDHT22 = "));
    myFile.println(settings.sDHT22);
    myFile.print(F("sDS18B20 = "));
    myFile.println(settings.sDS18B20);
    myFile.print(F("sPHOTO = "));
    myFile.println(settings.sPHOTO);
    myFile.print(F("startV = "));
    myFile.println(settings.startV );
    myFile.print(F("stopV = "));
    myFile.println(settings.stopV );
    myFile.print(F("numPoints = "));
    myFile.println(settings.numPoints);
    myFile.print(F("nmeas = "));
    myFile.println(settings.nmeas);
    myFile.print(F("calibrationPh = "));
    myFile.println(settings.calibrationPh);
    myFile.print(F("eth = "));
    myFile.println(settings.eth );
    myFile.print(F("autostart = "));
    myFile.println(settings.autostart );
    myFile.close();
    delay(500);
    delay(DELAY_SHOW_SERIAL);
    Serial.print(MESSAGE);
    Serial.println(F("Settings saved to SD"));
}


// check if the program should stop
void stop_check() {
    int pin_status = digitalRead(pin_BUTTON);
    // if the button state has changed:
    if (pin_status != pin_prev_status) {
        if (pin_status == HIGH) {
            Serial.println(F("SD safe mode"));
            go =false;
            setAutoStart(false);
            display.print("Stop pressed");
            display.display();
            display.clearDisplay();
        }
    }
    delay(50);
    pin_prev_status = pin_status;
}


// check if the program should start measuring in Free Mode or accidental restart
void start_check() {
    // in Free mode the SD card must be working
    if (settings.SDuse==true) {
        if (!card.init(SPI_HALF_SPEED, chipSelect)) {  }
        if (!volume.init(&card)){}
    }
    
    // the program can start if we are in the unlimited mode or if we have not performed all the measurements - "waiting" is displayed
    if (count< settings.nmeas  || settings.automode==true) {
        display.println("Waiting");
        if (!settings.SDuse && !settings.pcmode) {
            // attempt solving problems to SD card by restarting if in Free mode
            display.println("SD error");
            delay(1000);
            display.println("Attempt restarting");
            delay(1000);
            resetFunc();
        }
        // attempt solving problems to RTC card by restarting if in Free mode
        if (!RTC_conn && !settings.pcmode) {
            display.println("RTC error");
            delay(1000);
            display.println("Attempt restarting");
            delay(1000);
            resetFunc();
        }
        display.display();
        display.clearDisplay();
        delay(300);
    }
    // the program is done when the end of the programmed amount of measurements are taken - "done" is displayed
    else {
        setAutoStart(false);  // do not start measurement automatically when an accidental restart occur
        display.println("Done");
        display.display();
        display.clearDisplay();
        delay(300);
    }
    
    // check the start/stop button has been pressed
    int reading = digitalRead(pin_BUTTON);
    if (reading != pin_prev_status) {
        // reset the debouncing timer
        lastDebounceTime = millis();
    }
    
    if ((millis() - lastDebounceTime) > debounceDelay) {
        // whatever the reading is at, it's been there for longer
        // than the debounce delay, so take it as the actual current state:
        
        // if the button state has changed, start measuring
        if (reading != pin_status) {
            pin_status = reading;
            
            if (pin_status == HIGH) {
                setAutoStart(true);  // if the MMkit restarts before "done" is displayed (or stopped is pressed) it will restart automatically measuring
                Serial.println(F("Starting"));
                display.println("Starting");
                display.print("StartV: ");
                display.println(settings.startV);
                display.print("StopV: ");
                display.println(settings.stopV);
                display.print("N.meas: ");
                if(settings.nmeas==0) {
                    display.println("unlim.");
                    
                }
                else {
                    display.print(count+1);
                    display.print("/");
                    display.println(settings.nmeas);
                }
                display.display();
                display.clearDisplay();
                delay(1000); //the delay is to allow reading information on display
                go =true; // allow starting
            }
        }
    }
    // save the reading.  Next time through the loop,
    // it'll be the lastButtonState:
    pin_prev_status = reading;
    
    
}


int countSDfiles() {
    int counter = 0;
    sd.vwd()->rewind();
    while (file.openNext(sd.vwd(), O_READ)) {
        file.close();
        counter+=1;
        if (DEBUG) { Serial.println(F("Count SD files:"));Serial.print(counter);  Serial.print(" ");}
    }
    return counter;
}

// read settings from SD card
void getSettings()   // don't use special character in descriptor!!
{
    // Open the settings file for reading:
    Serial.println(F("Attempt getting settings..."));
    if (!myFile.open("settings.txt", O_RDWR )) {
        Serial.println("opening filename for write failed");
        }
    else {
        Serial.println(F("Loading settings"));
        char character;
        String description = "";
        String value = "";
        char value_c[20];
        byte value_b[4];
        boolean valid = true;
        // read from the file until there's nothing else in it:
        while (myFile.available()) {
            character = myFile.read();
            if(character == '/')         {
                // Comment - ignore this line
                while(character != '\n'){
                    character = myFile.read();
                };
            } else if(isalnum(character))      {  // Add a character to the description
                description.concat(character);
            } else if(character =='=')         {  // start checking the value for possible results
                // First going to trim out all trailing white spaces
                do {
                    character = myFile.read();
                } while(character == ' ');
                if(description == "intervalIV") {
                    valid = true;
                    Serial.print(F("intervalIV set from SD"));
                    value = "";
                    while(character != '\n') {
                        value.concat(character);
                        character = myFile.read();
                    };
                    if (valid) {
                        // Convert string to array of chars
                        char charBuf[value.length()+1];
                        value.toCharArray(charBuf,value.length()+1);
                        // Convert chars to integer
                        settings.intervalIV = atoi(charBuf);
                    } else {
                        Serial.print(F(" standard "));
                        // revert to default value for invalid entry in settings
                        settings.intervalIV = 10;
                    }
                    Serial.print(F(" to "));
                    Serial.println(value);
                }
                else if(description == "intervalPh") {
                    valid = true;
                    Serial.println(F("intervalPh set from SD"));
                    value = "";
                    while(character != '\n') {
                        value.concat(character);
                        character = myFile.read();
                    };
                    if (valid) {
                        // Convert string to array of chars
                        char charBuf[value.length()+1];
                        value.toCharArray(charBuf,value.length()+1);
                        // Convert chars to integer
                        settings.intervalPh = atoi(charBuf);
                    } else {
                        // revert to default value for invalid entry in settings
                        settings.intervalPh = 10;
                    }
                }
                else if(description == "intervalEnv") {
                    valid = true;
                    Serial.println(F("intervalEnv set from SD"));
                    value = "";
                    while(character != '\n') {
                        value.concat(character);
                        character = myFile.read();
                    };
                    if (valid) {
                        // Convert string to array of chars
                        char charBuf[value.length()+1];
                        value.toCharArray(charBuf,value.length()+1);
                        // Convert chars to integer
                        settings.intervalEnv = atoi(charBuf);
                    } else {
                        // revert to default value for invalid entry in settings
                        settings.intervalEnv = 10;
                    }
                }else if(description == "numPoints") {
                    valid = true;
                    Serial.println(F("numPoints set from SD"));
                    value = "";
                    while(character != '\n') {
                        value.concat(character);
                        character = myFile.read();
                    };
                    if (valid) {
                        // Convert string to array of chars
                        char charBuf[value.length()+1];
                        value.toCharArray(charBuf,value.length()+1);
                        // Convert chars to integer
                        settings.numPoints = atoi(charBuf);
                    } else {
                        // revert to default value for invalid entry in settings
                        settings.numPoints = 5;
                    }
                }
                else if(description == "stopV") {
                    valid = true;
                    Serial.println(F("stopV set from SD"));
                    value = "";
                    while(character != '\n') {
                        value.concat(character);
                        character = myFile.read();
                    };
                    if (valid) {
                        // Convert string to array of chars
                        char charBuf[value.length()+1];
                        value.toCharArray(charBuf,value.length()+1);
                        // Convert chars to integer
                        settings.stopV = atof(charBuf);
                    } else {
                        // revert to default value for invalid entry in settings
                        settings.stopV = 1;
                    }
                }
                else if(description == "calibrationPh") {
                    valid = true;
                    Serial.println(F("calibrationPh set from SD"));
                    value = "";
                    while(character != '\n') {
                        value.concat(character);
                        character = myFile.read();
                    };
                    if (valid) {
                        // Convert string to array of chars
                        char charBuf[value.length()+1];
                        value.toCharArray(charBuf,value.length()+1);
                        // Convert chars to integer
                        settings.calibrationPh = atof(charBuf);
                    } else {
                        // revert to default value for invalid entry in settings
                        settings.calibrationPh = 615.75;
                    }
                }
                else if(description == "startV") {
                    valid = true;
                    Serial.println(F("startV set from SD"));
                    value = "";
                    while(character != '\n') {
                        value.concat(character);
                        character = myFile.read();
                    };
                    if (valid) {
                        // Convert string to array of chars
                        char charBuf[value.length()+1];
                        value.toCharArray(charBuf,value.length()+1);
                        // Convert chars to integer
                        settings.startV = atof(charBuf);
                    } else {
                        // revert to default value for invalid entry in settings
                        settings.startV = -1;
                    }
                }
                else if(description == "dynIP") {
                    valid = true;
                    Serial.println(F("dynIP set from SD"));
                    if (character=='1') {
                        settings.dynIP = true;
                    } else {
                        settings.dynIP = false;
                    }
                } else if(description == "sDHT22") {
                    valid = true;
                    Serial.println(F("sDHT22 set from SD"));
                    if (character=='1') {
                        settings.sDHT22 = true;
                    } else {
                        settings.sDHT22 = false;
                    }
                } else if(description == "sDS18B20") {
                    valid = true;
                    Serial.println(F("sDS18B20 set from SD"));
                    if (character=='1') {
                        settings.sDS18B20 = true;
                    } else {
                        settings.sDS18B20 = false;
                    }
                } else if(description == "sPHOTO") {
                    valid = true;
                    Serial.println(F("sPHOTO set from SD"));
                    if (character=='1') {
                        settings.sPHOTO = true;
                    } else {
                        settings.sPHOTO = false;
                    }
                }else if(description == "autostart") {
                    valid = true;
                    Serial.println(F("autostart set from SD"));
                    if (character=='1') {
                        settings.autostart = true;
                    } else {
                        settings.autostart = false;
                    }
                }
                else if(description == "sIV") {
                    valid = true;
                    Serial.println(F("sIV set from SD"));
                    if (character=='1') {
                        settings.sIV = true;
                    } else {
                        settings.sIV = false;
                    }
                }
                else if(description == "pcmode") {
                    valid = true;
                    Serial.println(F("pcmode set from SD"));
                    if (character=='1') {
                        settings.pcmode = true;
                    } else {
                        settings.pcmode = false;
                    }
                }
                else if(description == "nmeas") {
                    valid = true;
                    Serial.println(F("nmeas set from SD"));
                    value = "";
                    while(character != '\n') {
                        value.concat(character);
                        character = myFile.read();
                    };
                    if (valid) {
                        // Convert string to array of chars
                        char charBuf[value.length()+1];
                        value.toCharArray(charBuf,value.length()+1);
                        // Convert chars to integer
                        settings.nmeas = atoi(charBuf);
                    } else {
                        // revert to default value for invalid entry in settings
                        settings.nmeas = 1;
                    }
                }
                else if(description == "arduinoID") {
                    valid = true;
                    Serial.println(F("arduinoID set from SD"));
                    value = "";
                    do {
                        value.concat(character);
                        character = myFile.read();
                    } while(character != '\n');
                    value.toCharArray(value_c,value.length());
                    for(int i=0; i<10; i++)
                    {
                        settings.arduinoID[i] = value_c[i];
                    }
                }
                else { // unknown parameter
                    while(character != '\n')
                    character = myFile.read();
                }
                description = "";
            } else {
            }
        }
        // close the file:
        myFile.close();
    }
}

// set autostart in case of accidental reboot
void setAutoStart(boolean control_auto) {
    if (settings.SDuse==true && settings.autostart!=control_auto) {
        settings.autostart=control_auto;
        digitalWrite(SS_ETHERNET, HIGH);  // SD Card ACTIVE
        Serial.println(F("Attempt set autostart..."));
        //Rewrite autostart file
        if (!myFile.open("autost.txt", O_WRITE | O_CREAT | O_AT_END )) {
            sd.errorHalt("opening filename for write failed");
            }
        if (!myFile.remove()) Serial.println("Error file.remove");
        if (!myFile.open("autost.txt", O_WRITE | O_CREAT | O_AT_END ))
            myFile.println(F("//autostart"));
        myFile.print(F("autostart = "));
        myFile.println(settings.autostart );
        myFile.close();
        delay(300);
        Serial.println(F("autostart saved to SD"));
    }
    else if (settings.autostart!=control_auto && settings.SDuse==false){
        settings.autostart=control_auto;
    }
}


void error_P(const char* str) {
    settings.SDuse=0; // if entering in the error, the SD does't work
    PgmPrint("error: ");
    SerialPrintln_P(str);
    if (card.errorCode()) {
        PgmPrint("SD error: ");
        Serial.print(card.errorCode(), HEX);
        Serial.print(',');
        Serial.println(card.errorData(), HEX);
    }
    
}


void getAutoStart() {
    Serial.println(F("Attempt getting autostart..."));
    if (!myFile.open("autost.txt", O_RDWR )) {
        Serial.println("opening filename for write failed");
        }
    else {
        Serial.println(F("Loading autostart"));
        char character;
        String description = "";
        String value = "";
        boolean valid = true;
        while (myFile.available()) {
            character = myFile.read();
            if(character == '/')         {
                // Comment - ignore this line
                while(character != '\n'){
                    character = myFile.read();
                };
            } else if(isalnum(character))      {  // Add a character to the description
                description.concat(character);
            } else if(character =='=')         {  // start checking the value for possible results
                // First going to trim out all trailing white spaces
                do {
                    character = myFile.read();
                } while(character == ' ');
                
                if(description == "autostart") {
                    valid = true;
                    Serial.println(F("autostart set from SD"));
                    if (character=='1') {
                        settings.autostart = true;
                    } else {
                        settings.autostart = false;
                    }
                }
                else { // unknown parameter
                    while(character != '\n')
                    character = myFile.read();
                }
                description = "";
            } else {
            }
        }
        myFile.close();
    }
}


// print to serial port all settings
void printSettings() {
    delay(DELAY_SHOW_SERIAL);
    int DELAY_SHOW_SERIAL2=0;
    Serial.println(F("//Settings file"));
    delay(DELAY_SHOW_SERIAL2);
    Serial.print(F("intervalIV = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.intervalIV);
    delay(DELAY_SHOW_SERIAL2);
    Serial.print(F("intervalEnv = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.intervalEnv);
    delay(DELAY_SHOW_SERIAL2);
    Serial.print(F("intervalPh = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.intervalPh);
    delay(DELAY_SHOW_SERIAL2);
    Serial.print(F("sDHT22 = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.sDHT22);
    delay(DELAY_SHOW_SERIAL2);
    Serial.print(F("sDS18B20 = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.sDS18B20);
    delay(DELAY_SHOW_SERIAL2);
    Serial.print(F("sPHOTO = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.sPHOTO);
    delay(DELAY_SHOW_SERIAL2);
    Serial.print(F("startV = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.startV );
    delay(DELAY_SHOW_SERIAL2);
    Serial.print(F("stopV = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.stopV );
    delay(DELAY_SHOW_SERIAL2);
    Serial.print(F("numPoints = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.numPoints);
    delay(DELAY_SHOW_SERIAL2);
    Serial.print(F("nmeas = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.nmeas);
    delay(DELAY_SHOW_SERIAL2);
    Serial.print(F("calibrationPh = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.calibrationPh);
    delay(DELAY_SHOW_SERIAL2);
    Serial.print(F("eth = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.eth );
    Serial.print(F("pcmode = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.pcmode );
    Serial.print(F("automode = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.automode );
    Serial.print(F("SDuse = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.SDuse );
    Serial.print(F("Time = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(digitalClockDisplay() );
    Serial.print(F("FILE_EXP = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(FILE_EXP);
    Serial.print(F("autostart = "));
    delay(DELAY_SHOW_SERIAL2);
    Serial.println(settings.autostart);
    delay(500);
}
