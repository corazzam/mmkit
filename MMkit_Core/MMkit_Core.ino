
/*************** LIBRARIES ********************************/
#include <SdFat.h>
#include <SdBaseFile.h>
#include <SdFatUtil.h>
#include <SPI.h>
#include <Wire.h>
#include <Time.h>
#include <DS3232RTC.h>
#include <dht.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include <mcp4728.h> //DAC chip
#include <OneWire.h>
#include <sha1.h>
#include <JsonGenerator.h>
#include <avr/pgmspace.h>
#include <ctype.h>
using namespace ArduinoJson::Generator;
#include <Average.h>

/*************** INTERFACE with MMkit GUI ********************************/
boolean DEBUG = false;
#define TIME_REQUEST  7    // ASCII bell character requests a time sync message
#define MESSAGE  "ME"    // message code for processing app
#define UPDSTATUS  "US"    // update status for processing app
#define DELAY_SHOW_SERIAL  200

/*************** Correct errors ********************************/
// IV curve
float correct_error= 0;  // substract any current offset at 0 V

//photodiode
float correct_photo= 0;  // don't consider dark offset of photodiode

/*************** Screen ********************************/
Adafruit_PCD8544 display = Adafruit_PCD8544(7, 6, 5, 4, 3);  //Connection to the pin controlling the screen (SPI emulation)
#define DISP_CONTRAST 50
#define DISP_TEXTSIZE 0.5
char display_data_l1[14];
char display_data_l2[14];
char display_data_l3[14];
char display_data_l4[14];
char display_data_l5[14];
char display_data_l6[14];
char display_data_l7[14];

/******* Temperature and Humidity ********/
#define DHT22PIN 28 //Data is collected
dht DHT;
OneWire  ds(26);  // ds18B20 temperarature sensor
double hum_DHT;
double tem_DHT;
double temp_DS18;

/******* Photodiode ********/
const int analogPhotodiodePin = A0;
int photodiodeValue = 0;
#define CNT 100
Average<float> irr(CNT);
int ir_IV_index;
double irr_photo;

/******* Timing ********/
unsigned long timerDHT = 0;
boolean timerDHT_RST = true;
unsigned long timerphoto = 0;
boolean timerphoto_RST = true;
unsigned long timerIV = 0;
boolean timerIV_RST = true;
String Time; //
boolean RTC_conn=true;
unsigned long millis_limit = 3888000000;  //3888000000=45(days)*24(hours)*60(minutes)*60(seconds)*1000(convert to milliseconds)

/******* SD card ********/
const int chipSelect = 4;  // 8 for Sparkfun shield - 4 for Arduino ethernet
#define pin_BUTTON 24
int pin_status;
int pin_prev_status;
// definition
Sd2Card card;
SdVolume volume;
SdFile root;
SdFile file;
SdFat sd;
#define error(s) error_P(PSTR(s))
#define BUFSIZ 100
#define SS_ETHERNET 53
// settings to be saved in SD card
SdFile myFile;
struct parameters {
    double intervalIV;
    double intervalEnv;
    double intervalPh;
    float calibrationPh;
    boolean dynIP;
    boolean pcmode;
    boolean eth;
    boolean wifi;
    boolean mysql;
    boolean sDHT22;
    boolean sDS18B20;
    boolean sPHOTO;
    boolean automode;
    int nmeas;
    boolean sIV;
    boolean SDuse;
    float startV;
    float stopV;
    char arduinoID[10];
    int numPoints;
    boolean localmysql;
    byte domainip1;
    byte domainip2;
    byte domainip3;
    int arduinoip;
    byte mysqlip;
    boolean autostart;
} settings;

/******* Control of experiment ********/
boolean waiting=true;
boolean go = false;
// limits to length of exp_id and of filename on the SD card (max 8.3)
char EXP_ID[15];
char FILE_EXP[15];

/******* IV courve measurement ********/
double current_limit = 70; // limit is 70mA
int minNumPoints = 0;
int maxNumPoints = 50;  //due to memory issue
int count=0;  // Reset counter

char inData[4]; // Allocate some space for the string
char inChar=-1; // Where to store the character read
byte index = 0; // Index into array; where to store the character

#define OpAMPpin 8 // shutdown pin for the (optional) operational amplifier
#define relaypin 5 //relay for turning on/off lamp

// I2C address for MCP3422 - base address for MCP3424 = 0x68
#define MCP342X_ADDRESS 0X6E    // should be changed if using RTC - DS1307 and DS3231 are on 0x68    // soldered Adr1-Adr0 to Vdd 0x6E

// fields in ADC configuration register
#define MCP342X_GAIN_FIELD 0X03 // PGA field
#define MCP342X_GAIN_X1    0X00 // PGA gain X1
#define MCP342X_GAIN_X2    0X01 // PGA gain X2
#define MCP342X_GAIN_X4    0X02 // PGA gain X4
#define MCP342X_GAIN_X8    0X03 // PGA gain X8

#define MCP342X_RES_FIELD  0X0C // resolution/rate field
#define MCP342X_RES_SHIFT  0X02 // shift to low bits
#define MCP342X_12_BIT     0X00 // 12-bit 240 SPS
#define MCP342X_14_BIT     0X04 // 14-bit 60 SPS
#define MCP342X_16_BIT     0X08 // 16-bit 15 SPS
#define MCP342X_18_BIT     0X0C // 18-bit 3.75 SPS

#define MCP342X_CONTINUOUS 0X10 // 1 = continuous, 0 = one-shot

#define MCP342X_CHAN_FIELD 0X60 // channel field
#define MCP342X_CHANNEL_1  0X00 // select MUX channel 1
#define MCP342X_CHANNEL_2  0X20 // select MUX channel 2
#define MCP342X_CHANNEL_3  0X40 // select MUX channel 3
#define MCP342X_CHANNEL_4  0X60 // select MUX channel 4

#define MCP342X_START      0X80 // write: start a conversion
#define MCP342X_BUSY       0X80 // read: output not ready
uint8_t chan = 0XFF, gain = 0XFF, res = 0XFF;

// default adc configuration register - resolution and gain added in setup()
uint8_t adcConfig = MCP342X_START | MCP342X_CHANNEL_1 | MCP342X_CONTINUOUS;
// divisor to convert ADC reading to milivolts
uint16_t mvDivisor;

// Initiate DAC
mcp4728 dac = mcp4728(0); // instantiate mcp4728 object, Device ID = 0


/*************** SETUP ********************************/
void setup()
{
    Serial.begin(115200);
    setSettings();
    
    /////////////////// Display ///////////////////
    display.begin();
    display.setContrast(DISP_CONTRAST);
    display.setTextSize(DISP_TEXTSIZE);
    display.setTextColor(BLACK);
    
    /////////////////// Time ///////////////////
    delay(1500);
    setSyncProvider(RTC.get);   // the function to get the time from the RTC
    delay(100);
    if(timeStatus()!= timeSet) {
        Serial.println(F("Unable to sync with the RTC"));
        delay(DELAY_SHOW_SERIAL);
        Serial.print(MESSAGE);
        Serial.println(F("RTC error"));
        RTC_conn=false;
    }
    else
        Serial.println(F("RTC has set the system time"));
    
    /////////////////// SD card ///////////////////
    // SD card is mandatory for the correct function
    //Safe mode
    pinMode(pin_BUTTON,OUTPUT);
    pin_status = digitalRead(pin_BUTTON);
    pin_prev_status = pin_status; // Button will change status and determine an event for SD safe mode
    // Debug SD: if the SD fails in any of the following step the program won't run through and show "SD error on screen"
  
    if (settings.SDuse) {
        Serial.println(F("Init SD card"));
        if (!card.init(SPI_HALF_SPEED, chipSelect)) { error("card.init failed!"); }
        if (!volume.init(&card)) error("vol.init failed!");
        if (!root.openRoot(&volume)) error("openRoot failed");
        //Start SD card
        if(settings.SDuse )if (!sd.begin(chipSelect, SPI_HALF_SPEED)) sd.initErrorHalt();
        // Load settings if SD is loaded
        if (settings.SDuse){ getSettings(); getAutoStart();} else { Serial.println(F("Cannot load settings!")); }
    }
    
    else if (settings.SDuse==0) {
        delay(DELAY_SHOW_SERIAL);
        Serial.print(MESSAGE);
        Serial.println(F("SD error"));
    }
    
    // Filename 8.3 format limit = MMDD_XXX
    char buffer[5];
    String res= (String)day();
    res+= month();
    res.toCharArray(buffer,sizeof(buffer));
    sprintf(FILE_EXP, "%s_%03i.txt", buffer ,countSDfiles());
    sprintf(EXP_ID, "%s_%s", FILE_EXP ,settings.arduinoID);
    Serial.println(FILE_EXP);
    
    /////////////////// DHT22 ///////////////////
    
    if (settings.sDHT22==1) {
        Serial.println(F("DHT22 ON"));
    }
    
    /////////////////// Setup IV measurements ///////////////////
    if ((settings.sIV==1)) {
        Serial.println(F("MeasIV ON"));
        // Open I2C bus
        Wire.begin();
        
        // Initialize the DAC
        dac.begin();  // initialize i2c interface
        dac.vdd(5000); // set VDD(mV) of MCP4728 for correct conversion between LSB and Vout
        
        // Initialize the OPamp shutdown pins (if using tlv4113)
        pinMode (OpAMPpin, OUTPUT); // initiate opamp
        digitalWrite(OpAMPpin,LOW);  // set shutdown
    }
    
    if (settings.nmeas==0) { // unlimited measurements
        settings.automode=1;
    }
} //end setup


void(* resetFunc) (void) = 0;//declare reset function at address 0 - restart of MMkit to try fixing RTC or SD card problems


void loop()
{
    // check MMkit GUI connection (PC mode)
    connectPC();
    
    //tell MMkit GUI that it's waiting to measure
    if (!waiting) {
        delay(DELAY_SHOW_SERIAL);
        Serial.print(UPDSTATUS);
        Serial.println(F("Ready to measure"));
        waiting=true;
    }
    
    // check start measuring by start/stop button pressed (Free Mode) 
    start_check();
    
    ////////////////////////////// Start measuring ///////////////////////////////
    
    // If no SD or RTC are working Arduino will not run in Free mode
    while((settings.automode==true && settings.pcmode==true&& (go==true ||settings.autostart==true)) || (settings.automode==true && settings.SDuse==true && RTC_conn==true && (go==true ||settings.autostart==true)&& settings.pcmode==false)  || ((count < settings.nmeas) && (settings.pcmode==true)&& (go==true ||settings.autostart==true)) || ((count < settings.nmeas) && (settings.SDuse==true)&& RTC_conn==true &&(settings.pcmode==false)&&(go==true ||settings.autostart==true)) ) {
        waiting=false;
        
        if (millis()>millis_limit)resetFunc(); // reboot MMkit after roughly 45 days to avoid overflow of millis variable
        
        // to stop measurements
        int pin_prev_status = LOW;
        
        connectPC(); // verify if stop is pressed
        stop_check();
           
        /************ Temperature and humidity ************/
        if (((((millis() - timerDHT)/1000 > (settings.intervalEnv)) && (timerDHT_RST==false)) || count==0 ) && (settings.sDHT22==1 || settings.sDS18B20==1))  // the first measurement doesn't wait
        {
            char serial_env[10] PROGMEM;
            
            if (settings.SDuse) {
                if (!file.open(FILE_EXP, O_WRITE | O_CREAT | O_AT_END)) {
                    sd.errorHalt("opening filename for write failed");
                    }
            }
            
            // DHT
            char hum_dht[5];
            char temp_dht[5];
            if (settings.sDHT22==1) {
                int chk = DHT.read22(DHT22PIN);
                
                hum_DHT = DHT.humidity;
                tem_DHT = DHT.temperature;
                
                dtostrf(hum_DHT,2,1,hum_dht);
                dtostrf(tem_DHT,2,1,temp_dht);
                sprintf(serial_env,"HU[%s]",hum_dht);
                delay(DELAY_SHOW_SERIAL);
                Serial.println(serial_env);
                delay(DELAY_SHOW_SERIAL);
                sprintf(serial_env,"TA[%s]",temp_dht);
                Serial.println(serial_env);
                delay(DELAY_SHOW_SERIAL);
                if (settings.SDuse) {
                    file.print("TE[");
                    file.print(temp_dht);
                    file.print("]");
                    file.print("HU[");
                    file.print(hum_dht);
                    file.print("]");
                }
            }
            
            // DS18
            char temp_ds18_char[5];
            if (settings.sDS18B20==1) {
                double temp_ds18 = ds18b20();
                
                dtostrf(temp_ds18,2,2,temp_ds18_char);
                sprintf(serial_env,"DS[%s]",temp_ds18_char);
                Serial.println(serial_env);
                delay(DELAY_SHOW_SERIAL);
                if (settings.SDuse) {
                    file.print("DS[");
                    file.print(temp_ds18);
                    file.print("]");
                }
            }
            
            if (settings.sDHT22==1) {
                sprintf(display_data_l4,"TE:%sHU:%s",temp_dht,hum_dht); // if NAN this doesn't work
                data_screen_line(FILE_EXP,display_data_l2,display_data_l3,display_data_l4,display_data_l5,display_data_l6,display_data_l7);
            }
            if (settings.sDS18B20==1) {
                sprintf(display_data_l6,"DS:%s",temp_ds18_char); // if NAN this doesn't work
                data_screen_line(FILE_EXP,display_data_l2,display_data_l3,display_data_l4,display_data_l5,display_data_l6,display_data_l7);
            }
            
            if (settings.SDuse) {
                file.print("TI[");
                delay(200);
                file.print(digitalClockDisplay());
                file.println("]");
                file.close();
            }
            
            // Reset timer
            timerDHT_RST = true;
            connectPC();

            stop_check();
            
        }
        // Reset DHT
        if ((timerDHT_RST == true) && (settings.sDHT22==1||settings.sDS18B20==1))
        {
            if (DEBUG) Serial.println(F("ResetEnv"));
            timerDHT = millis();
            timerDHT_RST = false;
            stop_check();
            connectPC();
        }
        
        /************ Irradiance ************/
        if (((((millis() - timerphoto)/1000 > (settings.intervalPh)) && (timerphoto_RST==false))|| count==0) && (settings.sPHOTO==1))
        {
            // Read from photodiode in mW/cm2
            int irr_index=1;
            while (irr_index<= CNT) {
                irr.push(analogRead(analogPhotodiodePin));
                irr_index+=1;
            }
            float sensorValue = (irr.mean()-correct_photo)/settings.calibrationPh*100;
            int photodiodeValue = (int)sensorValue;
            char photo_char[6];
            dtostrf(sensorValue,6,2,photo_char);

            if (DEBUG) {Serial.println(F("Debug for photodiode:")); settings.calibrationPh=615; Serial.println(analogRead(analogPhotodiodePin)); Serial.println(settings.calibrationPh); Serial.println(photodiodeValue);}
            // write to SD
            String o_p = "[";
            String c_p = "]";
            String sd_irr = "IR"+o_p;
            sd_irr += photo_char;
            sd_irr += c_p;
            delay(DELAY_SHOW_SERIAL);
            Serial.println(sd_irr);
            if (settings.SDuse) {
                if (!file.open(FILE_EXP, O_WRITE | O_CREAT | O_AT_END)) {
                    sd.errorHalt("opening filename for write failed");
                    }
                file.print("IR[");
                file.print(photo_char);
                file.print("]");
                file.print("TI[");
                file.print(digitalClockDisplay());
                file.println("]");
                file.close();
            }
            // Screen
            char buf[10];
            sprintf(buf, "%d", (int)photodiodeValue);
            sprintf(display_data_l5,"IR:%s",buf);
            // display update
            data_screen_line(FILE_EXP,display_data_l2,display_data_l3,display_data_l4,display_data_l5,display_data_l6,display_data_l7);
            
            // reset timer photodiode
            timerphoto_RST = true;
  
            stop_check();
            connectPC();
        }
        //Reset Photo
        if ((timerphoto_RST == true) && (settings.sPHOTO==1))
        {
            if (DEBUG) Serial.println(F("ResetPhoto"));
            timerphoto = millis();
            timerphoto_RST = false;
            stop_check();
            connectPC();
        }
        
        /************ IV curve measurement ************/
        if (((((millis() - timerIV)/1000 > (settings.intervalIV)) && (timerIV_RST==false))|| count==0)&& (settings.sIV==1))
        {
            runVoltageSweep();
            
            if (settings.SDuse) {
                if (!file.open(FILE_EXP, O_WRITE | O_CREAT | O_AT_END)) {
                    sd.errorHalt("opening filename for write failed");
                    }
                file.print("TI[");
                file.print(digitalClockDisplay());
                file.println("]");
                file.close();
            }
            
            timerIV_RST = true;
            // counter number of IV measurement
            count+=1;
            if (DEBUG) { Serial.print(F("Measurement IV n.: ")); Serial.print(count); }
            if (DEBUG) { Serial.print(F(" of total: ")); Serial.println(settings.nmeas); }
        }
        //Reset IV
        if ((timerIV_RST == true) && (settings.sIV==1))
        {
            if (DEBUG) Serial.println(F("ResetIV"));
            timerIV = millis();
            timerIV_RST = false;
            stop_check();
            connectPC();
        }
    }
}






