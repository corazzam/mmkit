////////////////////////////// Methods needed to read from sensors ///////////////////////////////


float ds18b20() { 
// DS18B20
    byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];
  float celsius, fahrenheit;
  
  if ( !ds.search(addr)) {
    ds.reset_search();
    delay(250);
  }
  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on at the end
  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();

  }

  
  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = (float)raw / 16.0;
  //Serial.print(F("Temp. DS18B20 = "));
  //Serial.print(celsius);
  //Serial.println(F(" Celsius"));
  //fahrenheit = celsius * 1.8 + 32.0;
  return celsius;
}

//-------------------------------- Time METHODS ---------------------------------

String digitalClockDisplay(){
  // digital clock display of the time
  String res = (String)hour();
  res+= printDigits(minute());
  res+= printDigits(second());
  res += " ";
  res+= day();
  res += " ";
  res+= month();
  res += " ";
  res+= year();
  return res;
}

String printDigits(int digits){
  // utility function for digital clock display: prints preceding colon and leading 0
  String res = ":";
  if(digits < 10)
    res+="0";
  res= res+ (String) digits;
   return res;  
}

String digitalClockDisplay_fn(){  //fn=filenmae
  // digital clock display of the time
  char buffer[8];
  String res= (String)day();
  res+= month();
  Serial.println(res);
  //res += "_";
  res.toCharArray(buffer,sizeof(buffer));
  Serial.println(buffer);
  return res;
}


/*  code to process time sync messages from the serial port   */
#define TIME_MSG_LEN  11   // time sync to PC is HEADER followed by unix time_t as ten ascii digits
#define TIME_HEADER  'T'   // Header tag for serial time sync message


time_t processSyncMessage() {
  // return the time if a valid sync message is received on the serial port.
  while(Serial.available() >=  TIME_MSG_LEN ){  // time message consists of a header and ten ascii digits
    char c = Serial.read() ; 
    if (DEBUG)Serial.print(c);  
    if( c == TIME_HEADER ) {       
      time_t pctime = 0;
      for(int i=0; i < TIME_MSG_LEN -1; i++){   
        c = Serial.read();  
        if (DEBUG) delay(500);  
        if (DEBUG) Serial.print(c);        
        if( c >= '0' && c <= '9'){   
          pctime = (10 * pctime) + (c - '0') ; // convert digits to a number    
        }
      }
      delay(1000);
     Serial.println(F("Time calibrated"));   
     RTC.set(pctime);
     setTime(pctime);   // Sync Arduino clock to the time received on the serial port
      return pctime; 
    }  
  }
  return 0;
}

//-------------------------------- Screen METHODS ---------------------------------

//specify every line to plot
void data_screen_line(char* l1,char* l2,char* l3,char* l4,char* l5,char* l6,char* l7) {
 
    display.println(l1);
    display.println(l2);
    display.println(l3);
    display.print(l4);
    display.println(l5);
    display.println(l6);
    display.display();
    display.clearDisplay();
    delay(100); 
}
